<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.1.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Parts" color="7" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Hidden" color="7" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="micro-stm">
<description>&lt;b&gt;ARM 32-bit Cortex™ MCUs&lt;/b&gt;&lt;p&gt;

&lt;author&gt;Created by jb@jenszuhause.de&lt;/author&gt;&lt;p&gt;
&lt;b&gt;You are using this libary by your own risk.&lt;/b&gt;&lt;br&gt;
&lt;b&gt;Make sure to check everything well due to this libary before using it.&lt;/b&gt;</description>
<packages>
<package name="LQFP64">
<description>&lt;b&gt;LQFP64&lt;/b&gt;&lt;p&gt;
10 x 10 mm, 64-pin low-profile quad flat package</description>
<wire x1="-5" y1="-5" x2="-5" y2="5" width="0.127" layer="21"/>
<wire x1="-5" y1="5" x2="5" y2="5" width="0.127" layer="21"/>
<wire x1="5" y1="5" x2="5" y2="-5" width="0.127" layer="21"/>
<wire x1="5" y1="-5" x2="-5" y2="-5" width="0.127" layer="21"/>
<circle x="-4.42" y="-4.43" radius="0.306103125" width="0.127" layer="21"/>
<rectangle x1="-3.86" y1="-6" x2="-3.64" y2="-5" layer="51"/>
<rectangle x1="-3.36" y1="-6" x2="-3.14" y2="-5" layer="51"/>
<rectangle x1="-2.86" y1="-6" x2="-2.64" y2="-5" layer="51"/>
<rectangle x1="-2.36" y1="-6" x2="-2.14" y2="-5" layer="51"/>
<rectangle x1="-1.86" y1="-6" x2="-1.64" y2="-5" layer="51"/>
<rectangle x1="-1.36" y1="-6" x2="-1.14" y2="-5" layer="51"/>
<rectangle x1="-0.86" y1="-6" x2="-0.64" y2="-5" layer="51"/>
<smd name="1" x="-3.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<rectangle x1="-0.36" y1="-6" x2="-0.14" y2="-5" layer="51"/>
<rectangle x1="0.14" y1="-6" x2="0.36" y2="-5" layer="51"/>
<rectangle x1="0.64" y1="-6" x2="0.86" y2="-5" layer="51"/>
<rectangle x1="1.14" y1="-6" x2="1.36" y2="-5" layer="51"/>
<rectangle x1="1.64" y1="-6" x2="1.86" y2="-5" layer="51"/>
<rectangle x1="2.14" y1="-6" x2="2.36" y2="-5" layer="51"/>
<rectangle x1="2.64" y1="-6" x2="2.86" y2="-5" layer="51"/>
<rectangle x1="3.14" y1="-6" x2="3.36" y2="-5" layer="51"/>
<rectangle x1="3.64" y1="-6" x2="3.86" y2="-5" layer="51"/>
<rectangle x1="5.39" y1="-4.25" x2="5.61" y2="-3.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-3.75" x2="5.61" y2="-2.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-3.25" x2="5.61" y2="-2.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-2.75" x2="5.61" y2="-1.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-2.25" x2="5.61" y2="-1.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-1.75" x2="5.61" y2="-0.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-1.25" x2="5.61" y2="-0.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-0.75" x2="5.61" y2="0.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="-0.25" x2="5.61" y2="0.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="0.25" x2="5.61" y2="1.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="0.75" x2="5.61" y2="1.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="1.25" x2="5.61" y2="2.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="1.75" x2="5.61" y2="2.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="2.25" x2="5.61" y2="3.25" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="2.75" x2="5.61" y2="3.75" layer="51" rot="R90"/>
<rectangle x1="5.39" y1="3.25" x2="5.61" y2="4.25" layer="51" rot="R90"/>
<rectangle x1="3.64" y1="5" x2="3.86" y2="6" layer="51" rot="R180"/>
<rectangle x1="3.14" y1="5" x2="3.36" y2="6" layer="51" rot="R180"/>
<rectangle x1="2.64" y1="5" x2="2.86" y2="6" layer="51" rot="R180"/>
<rectangle x1="2.14" y1="5" x2="2.36" y2="6" layer="51" rot="R180"/>
<rectangle x1="1.64" y1="5" x2="1.86" y2="6" layer="51" rot="R180"/>
<rectangle x1="1.14" y1="5" x2="1.36" y2="6" layer="51" rot="R180"/>
<rectangle x1="0.64" y1="5" x2="0.86" y2="6" layer="51" rot="R180"/>
<rectangle x1="0.14" y1="5" x2="0.36" y2="6" layer="51" rot="R180"/>
<rectangle x1="-0.36" y1="5" x2="-0.14" y2="6" layer="51" rot="R180"/>
<rectangle x1="-0.86" y1="5" x2="-0.64" y2="6" layer="51" rot="R180"/>
<rectangle x1="-1.36" y1="5" x2="-1.14" y2="6" layer="51" rot="R180"/>
<rectangle x1="-1.86" y1="5" x2="-1.64" y2="6" layer="51" rot="R180"/>
<rectangle x1="-2.36" y1="5" x2="-2.14" y2="6" layer="51" rot="R180"/>
<rectangle x1="-2.86" y1="5" x2="-2.64" y2="6" layer="51" rot="R180"/>
<rectangle x1="-3.36" y1="5" x2="-3.14" y2="6" layer="51" rot="R180"/>
<rectangle x1="-3.86" y1="5" x2="-3.64" y2="6" layer="51" rot="R180"/>
<rectangle x1="-5.61" y1="3.25" x2="-5.39" y2="4.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="2.75" x2="-5.39" y2="3.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="2.25" x2="-5.39" y2="3.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="1.75" x2="-5.39" y2="2.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="1.25" x2="-5.39" y2="2.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="0.75" x2="-5.39" y2="1.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="0.25" x2="-5.39" y2="1.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-0.25" x2="-5.39" y2="0.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-0.75" x2="-5.39" y2="0.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-1.25" x2="-5.39" y2="-0.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-1.75" x2="-5.39" y2="-0.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-2.25" x2="-5.39" y2="-1.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-2.75" x2="-5.39" y2="-1.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-3.25" x2="-5.39" y2="-2.25" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-3.75" x2="-5.39" y2="-2.75" layer="51" rot="R90"/>
<rectangle x1="-5.61" y1="-4.25" x2="-5.39" y2="-3.25" layer="51" rot="R90"/>
<smd name="2" x="-3.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="3" x="-2.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="4" x="-2.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="5" x="-1.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="6" x="-1.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="7" x="-0.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="8" x="-0.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="9" x="0.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="10" x="0.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="11" x="1.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="12" x="1.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="13" x="2.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="14" x="2.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="15" x="3.25" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="16" x="3.75" y="-5.75" dx="1.2" dy="0.3" layer="1" rot="R90"/>
<smd name="17" x="5.75" y="-3.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="18" x="5.75" y="-3.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="19" x="5.75" y="-2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="20" x="5.75" y="-2.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="21" x="5.75" y="-1.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="22" x="5.75" y="-1.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="23" x="5.75" y="-0.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="24" x="5.75" y="-0.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="25" x="5.75" y="0.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="26" x="5.75" y="0.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="27" x="5.75" y="1.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="28" x="5.75" y="1.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="29" x="5.75" y="2.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="30" x="5.75" y="2.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="31" x="5.75" y="3.25" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="32" x="5.75" y="3.75" dx="1.2" dy="0.3" layer="1" rot="R180"/>
<smd name="33" x="3.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="34" x="3.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="35" x="2.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="36" x="2.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="37" x="1.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="38" x="1.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="39" x="0.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="40" x="0.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="41" x="-0.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="42" x="-0.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="43" x="-1.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="44" x="-1.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="45" x="-2.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="46" x="-2.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="47" x="-3.25" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="48" x="-3.75" y="5.75" dx="1.2" dy="0.3" layer="1" rot="R270"/>
<smd name="49" x="-5.75" y="3.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="50" x="-5.75" y="3.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="51" x="-5.75" y="2.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="52" x="-5.75" y="2.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="53" x="-5.75" y="1.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="54" x="-5.75" y="1.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="55" x="-5.75" y="0.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="56" x="-5.75" y="0.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="57" x="-5.75" y="-0.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="58" x="-5.75" y="-0.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="59" x="-5.75" y="-1.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="60" x="-5.75" y="-1.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="61" x="-5.75" y="-2.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="62" x="-5.75" y="-2.75" dx="1.2" dy="0.3" layer="1"/>
<smd name="63" x="-5.75" y="-3.25" dx="1.2" dy="0.3" layer="1"/>
<smd name="64" x="-5.75" y="-3.75" dx="1.2" dy="0.3" layer="1"/>
<text x="-2.98" y="2.05" size="1.27" layer="25">&gt;Name</text>
<text x="-2.96" y="-2.18" size="1.27" layer="27">&gt;Value</text>
</package>
</packages>
<symbols>
<symbol name="STM32_64PIN_TYP10">
<pin name="PA0-WKUP" x="22.86" y="-40.64" length="middle" rot="R180"/>
<pin name="PA1" x="22.86" y="-38.1" length="middle" rot="R180"/>
<pin name="PA2" x="22.86" y="-35.56" length="middle" rot="R180"/>
<pin name="PA3" x="22.86" y="-33.02" length="middle" rot="R180"/>
<pin name="PA4" x="22.86" y="-30.48" length="middle" rot="R180"/>
<pin name="PA5" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PA6" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PA7" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PA9" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PA10" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PA13" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PA14" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PB0" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="VDDA" x="-22.86" y="-22.86" length="middle" direction="pwr"/>
<pin name="BOOT0" x="-22.86" y="-35.56" length="middle" direction="in"/>
<pin name="NRST" x="-22.86" y="-33.02" length="middle" function="dot"/>
<pin name="OSC_IN" x="-22.86" y="-12.7" length="middle" function="clk"/>
<pin name="OSC_OUT" x="-22.86" y="-15.24" length="middle" function="clk"/>
<wire x1="-17.78" y1="-43.18" x2="-17.78" y2="40.64" width="0.254" layer="94"/>
<wire x1="-17.78" y1="40.64" x2="17.78" y2="40.64" width="0.254" layer="94"/>
<wire x1="17.78" y1="40.64" x2="17.78" y2="-43.18" width="0.254" layer="94"/>
<wire x1="17.78" y1="-43.18" x2="-17.78" y2="-43.18" width="0.254" layer="94"/>
<text x="-5.08" y="43.18" size="1.778" layer="95">&gt;Name</text>
<text x="-5.08" y="-45.72" size="1.778" layer="96">&gt;Value</text>
<pin name="PA8" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PA15" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PB1" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PB2" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="PB3" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="PB4" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="PB5" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PB6" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PB7" x="22.86" y="20.32" length="middle" rot="R180"/>
<pin name="PA11" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="PA12" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PB8" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PB9" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PB10" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PB12" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PB13" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="PB14" x="22.86" y="35.56" length="middle" rot="R180"/>
<pin name="PB15" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="PC13" x="-22.86" y="33.02" length="middle"/>
<pin name="PC14" x="-22.86" y="35.56" length="middle"/>
<pin name="PC15" x="-22.86" y="38.1" length="middle"/>
<pin name="VSSA" x="-22.86" y="-25.4" length="middle" direction="pwr"/>
<pin name="PC0" x="-22.86" y="0" length="middle"/>
<pin name="PC1" x="-22.86" y="2.54" length="middle"/>
<pin name="PC2" x="-22.86" y="5.08" length="middle"/>
<pin name="PC3" x="-22.86" y="7.62" length="middle"/>
<pin name="PC4" x="-22.86" y="10.16" length="middle"/>
<pin name="PC5" x="-22.86" y="12.7" length="middle"/>
<pin name="PC6" x="-22.86" y="15.24" length="middle"/>
<pin name="PC7" x="-22.86" y="17.78" length="middle"/>
<pin name="PC8" x="-22.86" y="20.32" length="middle"/>
<pin name="PC9" x="-22.86" y="22.86" length="middle"/>
<pin name="PC10" x="-22.86" y="25.4" length="middle"/>
<pin name="PC11" x="-22.86" y="27.94" length="middle"/>
<pin name="PC12" x="-22.86" y="30.48" length="middle"/>
<pin name="PD2" x="-22.86" y="-5.08" length="middle"/>
<pin name="VBAT" x="-22.86" y="-20.32" length="middle" direction="pwr"/>
<pin name="VCAP1" x="-22.86" y="-40.64" length="middle" direction="pwr"/>
</symbol>
<symbol name="PWR_VDD4_VSS4">
<pin name="VDD@1" x="-7.62" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="VSS@1" x="-7.62" y="-17.78" length="middle" direction="pwr" rot="R90"/>
<pin name="VDD@2" x="-2.54" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="VSS@2" x="-2.54" y="-17.78" length="middle" direction="pwr" rot="R90"/>
<pin name="VDD@3" x="2.54" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="VDD@4" x="7.62" y="17.78" length="middle" direction="pwr" rot="R270"/>
<pin name="VSS@3" x="2.54" y="-17.78" length="middle" direction="pwr" rot="R90"/>
<pin name="VSS@4" x="7.62" y="-17.78" length="middle" direction="pwr" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="STM32F401R*T" prefix="IC">
<description>&lt;b&gt;ARM 32-bit Cortex™-M3 CPU&lt;/b&gt;&lt;p&gt;
84 MHz maximum frequency&lt;br&gt;
128 to 256 Kbytes of Flash memory&lt;br&gt;
64 kbytes of SRAM&lt;br&gt;
50 GPIOs&lt;br&gt;
1.7 to 3.6 V application supply&lt;br&gt;</description>
<gates>
<gate name="MCU" symbol="STM32_64PIN_TYP10" x="0" y="0"/>
<gate name="PWR" symbol="PWR_VDD4_VSS4" x="50.8" y="0" addlevel="request"/>
</gates>
<devices>
<device name="" package="LQFP64">
<connects>
<connect gate="MCU" pin="BOOT0" pad="60"/>
<connect gate="MCU" pin="NRST" pad="7"/>
<connect gate="MCU" pin="OSC_IN" pad="5"/>
<connect gate="MCU" pin="OSC_OUT" pad="6"/>
<connect gate="MCU" pin="PA0-WKUP" pad="14"/>
<connect gate="MCU" pin="PA1" pad="15"/>
<connect gate="MCU" pin="PA10" pad="43"/>
<connect gate="MCU" pin="PA11" pad="44"/>
<connect gate="MCU" pin="PA12" pad="45"/>
<connect gate="MCU" pin="PA13" pad="46"/>
<connect gate="MCU" pin="PA14" pad="49"/>
<connect gate="MCU" pin="PA15" pad="50"/>
<connect gate="MCU" pin="PA2" pad="16"/>
<connect gate="MCU" pin="PA3" pad="17"/>
<connect gate="MCU" pin="PA4" pad="20"/>
<connect gate="MCU" pin="PA5" pad="21"/>
<connect gate="MCU" pin="PA6" pad="22"/>
<connect gate="MCU" pin="PA7" pad="23"/>
<connect gate="MCU" pin="PA8" pad="41"/>
<connect gate="MCU" pin="PA9" pad="42"/>
<connect gate="MCU" pin="PB0" pad="26"/>
<connect gate="MCU" pin="PB1" pad="27"/>
<connect gate="MCU" pin="PB10" pad="29"/>
<connect gate="MCU" pin="PB12" pad="33"/>
<connect gate="MCU" pin="PB13" pad="34"/>
<connect gate="MCU" pin="PB14" pad="35"/>
<connect gate="MCU" pin="PB15" pad="36"/>
<connect gate="MCU" pin="PB2" pad="28"/>
<connect gate="MCU" pin="PB3" pad="55"/>
<connect gate="MCU" pin="PB4" pad="56"/>
<connect gate="MCU" pin="PB5" pad="57"/>
<connect gate="MCU" pin="PB6" pad="58"/>
<connect gate="MCU" pin="PB7" pad="59"/>
<connect gate="MCU" pin="PB8" pad="61"/>
<connect gate="MCU" pin="PB9" pad="62"/>
<connect gate="MCU" pin="PC0" pad="8"/>
<connect gate="MCU" pin="PC1" pad="9"/>
<connect gate="MCU" pin="PC10" pad="51"/>
<connect gate="MCU" pin="PC11" pad="52"/>
<connect gate="MCU" pin="PC12" pad="53"/>
<connect gate="MCU" pin="PC13" pad="2"/>
<connect gate="MCU" pin="PC14" pad="3"/>
<connect gate="MCU" pin="PC15" pad="4"/>
<connect gate="MCU" pin="PC2" pad="10"/>
<connect gate="MCU" pin="PC3" pad="11"/>
<connect gate="MCU" pin="PC4" pad="24"/>
<connect gate="MCU" pin="PC5" pad="25"/>
<connect gate="MCU" pin="PC6" pad="37"/>
<connect gate="MCU" pin="PC7" pad="38"/>
<connect gate="MCU" pin="PC8" pad="39"/>
<connect gate="MCU" pin="PC9" pad="40"/>
<connect gate="MCU" pin="PD2" pad="54"/>
<connect gate="MCU" pin="VBAT" pad="1"/>
<connect gate="MCU" pin="VCAP1" pad="30"/>
<connect gate="MCU" pin="VDDA" pad="13"/>
<connect gate="MCU" pin="VSSA" pad="12"/>
<connect gate="PWR" pin="VDD@1" pad="64"/>
<connect gate="PWR" pin="VDD@2" pad="48"/>
<connect gate="PWR" pin="VDD@3" pad="32"/>
<connect gate="PWR" pin="VDD@4" pad="19"/>
<connect gate="PWR" pin="VSS@1" pad="63"/>
<connect gate="PWR" pin="VSS@2" pad="47"/>
<connect gate="PWR" pin="VSS@3" pad="31"/>
<connect gate="PWR" pin="VSS@4" pad="18"/>
</connects>
<technologies>
<technology name="B"/>
<technology name="C"/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="silabs">
<description>Silicon Laboratories&lt;p&gt;
C8051Fxxx family of mixed-signal microcontrollers integrates world-class analog,&lt;br&gt;
a high-speed pipelined 8051 CPU, ISP Flash Memory,&lt;br&gt;
and on-chip JTAG based debug in each device.&lt;br&gt;
The combination of configurable high-performance analog,&lt;br&gt;
100 MIPS 8051 core and in-system field programmability provides the user with complete design flexibility,&lt;br&gt;
improved time-to-market, superior system performance and greater end product differentiation.&lt;p&gt;

Source: http://www.silabs.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="MSOP10">
<wire x1="-1.5" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<smd name="6" x="1" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="7" x="0.5" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="8" x="0" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="9" x="-0.5" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<smd name="10" x="-1" y="2.3" dx="0.27" dy="1.5" layer="1"/>
<rectangle x1="0.365" y1="1.57" x2="0.635" y2="2.51" layer="51"/>
<rectangle x1="-0.135" y1="1.57" x2="0.135" y2="2.51" layer="51"/>
<rectangle x1="-0.635" y1="1.57" x2="-0.365" y2="2.51" layer="51"/>
<rectangle x1="-1.135" y1="1.57" x2="-0.865" y2="2.51" layer="51"/>
<rectangle x1="0.865" y1="1.57" x2="1.135" y2="2.51" layer="51"/>
<rectangle x1="0.865" y1="-2.5" x2="1.135" y2="-1.56" layer="51"/>
<smd name="5" x="1" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="4" x="0.5" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="3" x="0" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="2" x="-0.5" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<smd name="1" x="-1" y="-2.31" dx="0.27" dy="1.5" layer="1"/>
<rectangle x1="0.365" y1="-2.5" x2="0.635" y2="-1.56" layer="51"/>
<rectangle x1="-0.135" y1="-2.5" x2="0.135" y2="-1.56" layer="51"/>
<rectangle x1="-0.635" y1="-2.5" x2="-0.365" y2="-1.56" layer="51"/>
<rectangle x1="-1.135" y1="-2.5" x2="-0.865" y2="-1.56" layer="51"/>
<text x="-2.31" y="3.31" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.5" y="-5" size="1.27" layer="27">&gt;VALUE</text>
<circle x="-0.9" y="-0.9" radius="0.2" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="SI5351A">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<pin name="VDD" x="-12.7" y="5.08" length="middle" direction="pwr"/>
<pin name="XA" x="-12.7" y="2.54" length="middle" direction="pas"/>
<pin name="XB" x="-12.7" y="0" length="middle" direction="pas"/>
<pin name="SCL" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="SDA" x="-12.7" y="-5.08" length="middle"/>
<pin name="CLK2" x="12.7" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="VDDO" x="12.7" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="CLK1" x="12.7" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="CLK0" x="12.7" y="5.08" length="middle" direction="out" rot="R180"/>
<text x="-2.54" y="12.7" size="1.27" layer="95">&gt;NAME</text>
<text x="-2.54" y="10.16" size="1.27" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="SI5351" prefix="IC">
<gates>
<gate name="G$1" symbol="SI5351A" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MSOP10">
<connects>
<connect gate="G$1" pin="CLK0" pad="10"/>
<connect gate="G$1" pin="CLK1" pad="9"/>
<connect gate="G$1" pin="CLK2" pad="6"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="5"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VDDO" pad="7"/>
<connect gate="G$1" pin="XA" pad="2"/>
<connect gate="G$1" pin="XB" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IC_Robotech">
<description>&lt;Very-Low-Power Stereo Audio CODEC With PowerTune&amp;#153; Technology&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="QFN50P500X500X100-33N">
<description>&lt;b&gt;QFN50P500X500X100-33N&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="-2.4892" y="1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="2" x="-2.4892" y="1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="3" x="-2.4892" y="0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="4" x="-2.4892" y="0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="5" x="-2.4892" y="-0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="6" x="-2.4892" y="-0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="7" x="-2.4892" y="-1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="8" x="-2.4892" y="-1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="9" x="-1.7526" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="10" x="-1.2446" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="11" x="-0.762" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="12" x="-0.254" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="13" x="0.254" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="14" x="0.762" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="15" x="1.2446" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="16" x="1.7526" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="17" x="2.4892" y="-1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="18" x="2.4892" y="-1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="19" x="2.4892" y="-0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="20" x="2.4892" y="-0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="21" x="2.4892" y="0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="22" x="2.4892" y="0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="23" x="2.4892" y="1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="24" x="2.4892" y="1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="25" x="1.7526" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="26" x="1.2446" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="27" x="0.762" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="28" x="0.254" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="29" x="-0.254" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="30" x="-0.762" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="31" x="-1.2446" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="32" x="-1.7526" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="33" x="0" y="-0.0508" dx="3.3528" dy="3.3528" layer="1"/>
<text x="-3.4544" y="5.08" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-3.4544" y="-6.985" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.2446" y1="-4.191" x2="-1.2446" y2="-3.1496" width="0.1524" layer="21"/>
<wire x1="-0.7366" y1="3.2004" x2="-0.7366" y2="4.2164" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.254" x2="4.2164" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="2.2352" x2="2.5654" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.5654" x2="2.5654" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.5654" x2="-2.5654" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="-2.5654" x2="-2.2352" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="-2.5654" x2="2.5654" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="2.5654" x2="2.2352" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="2.5654" x2="-2.5654" y2="2.2352" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="-2.2352" x2="-2.5654" y2="-2.5654" width="0.1524" layer="21"/>
</package>
<package name="SOP65P490X110-8N">
<description>&lt;b&gt;SOP65P490X110-8N&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="-2.1844" y="0.9652" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="2" x="-2.1844" y="0.3302" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="3" x="-2.1844" y="-0.3302" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="4" x="-2.1844" y="-0.9652" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="5" x="2.1844" y="-0.9652" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="6" x="2.1844" y="-0.3302" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="7" x="2.1844" y="0.3302" dx="1.4986" dy="0.4318" layer="1"/>
<smd name="8" x="2.1844" y="0.9652" dx="1.4986" dy="0.4318" layer="1"/>
<text x="-2.8702" y="3.175" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-3.4544" y="-4.445" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.6002" y1="0.7874" x2="-1.6002" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="1.1684" x2="-2.5654" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="1.1684" x2="-2.5654" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="0.7874" x2="-1.6002" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="0.127" x2="-1.6002" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="0.508" x2="-2.5654" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="0.508" x2="-2.5654" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="0.127" x2="-1.6002" y2="0.127" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.508" x2="-1.6002" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.127" x2="-2.5654" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="-0.127" x2="-2.5654" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="-0.508" x2="-1.6002" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-1.1684" x2="-1.6002" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-0.7874" x2="-2.5654" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="-0.7874" x2="-2.5654" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="-2.5654" y1="-1.1684" x2="-1.6002" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-0.7874" x2="1.6002" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-1.1684" x2="2.5654" y2="-1.1684" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="-1.1684" x2="2.5654" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="-0.7874" x2="1.6002" y2="-0.7874" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-0.127" x2="1.6002" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-0.508" x2="2.5654" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="-0.508" x2="2.5654" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="-0.127" x2="1.6002" y2="-0.127" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="0.508" x2="1.6002" y2="0.127" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="0.127" x2="2.5654" y2="0.127" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="0.127" x2="2.5654" y2="0.508" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="0.508" x2="1.6002" y2="0.508" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="1.1684" x2="1.6002" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="0.7874" x2="2.5654" y2="0.7874" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="0.7874" x2="2.5654" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.5654" y1="1.1684" x2="1.6002" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-1.6002" x2="1.6002" y2="-1.6002" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="-1.6002" x2="1.6002" y2="1.6002" width="0.1524" layer="51"/>
<wire x1="1.6002" y1="1.6002" x2="0.3048" y2="1.6002" width="0.1524" layer="51"/>
<wire x1="0.3048" y1="1.6002" x2="-0.3048" y2="1.6002" width="0.1524" layer="51"/>
<wire x1="-0.3048" y1="1.6002" x2="-1.6002" y2="1.6002" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="1.6002" x2="-1.6002" y2="-1.6002" width="0.1524" layer="51"/>
<wire x1="-1.6002" y1="-1.6002" x2="1.6002" y2="-1.6002" width="0.1524" layer="21"/>
<wire x1="1.6002" y1="1.6002" x2="0.3048" y2="1.6002" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.6002" x2="-0.3048" y2="1.6002" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="1.6002" x2="-1.6002" y2="1.6002" width="0.1524" layer="21"/>
</package>
<package name="SOT65P210X110-6N">
<description>&lt;b&gt;SOT - 363&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="-1.1" y="0.65" dx="0.95" dy="0.4" layer="1"/>
<smd name="2" x="-1.1" y="0" dx="0.95" dy="0.4" layer="1"/>
<smd name="3" x="-1.1" y="-0.65" dx="0.95" dy="0.4" layer="1"/>
<smd name="4" x="1.1" y="-0.65" dx="0.95" dy="0.4" layer="1"/>
<smd name="5" x="1.1" y="0" dx="0.95" dy="0.4" layer="1"/>
<smd name="6" x="1.1" y="0.65" dx="0.95" dy="0.4" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" rot="R90" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-0.7" y1="-1.075" x2="0.7" y2="-1.075" width="0.001" layer="51"/>
<wire x1="0.7" y1="-1.075" x2="0.7" y2="1.075" width="0.001" layer="51"/>
<wire x1="0.7" y1="1.075" x2="-0.7" y2="1.075" width="0.001" layer="51"/>
<wire x1="-0.7" y1="1.075" x2="-0.7" y2="-1.075" width="0.001" layer="51"/>
<wire x1="-1.85" y1="-1.35" x2="1.85" y2="-1.35" width="0.05" layer="51"/>
<wire x1="1.85" y1="-1.35" x2="1.85" y2="1.35" width="0.05" layer="51"/>
<wire x1="1.85" y1="1.35" x2="-1.85" y2="1.35" width="0.05" layer="51"/>
<wire x1="-1.85" y1="1.35" x2="-1.85" y2="-1.35" width="0.05" layer="51"/>
<circle x="0" y="0" radius="0.35" width="0.05" layer="21"/>
<wire x1="0" y1="-0.5" x2="0" y2="0.5" width="0.05" layer="51"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.05" layer="51"/>
<wire x1="-0.525" y1="1" x2="-0.625" y2="0.9" width="0.1" layer="51"/>
<wire x1="-0.425" y1="1" x2="-0.625" y2="0.8" width="0.1" layer="51"/>
<wire x1="-0.325" y1="1" x2="-0.625" y2="0.7" width="0.1" layer="51"/>
<wire x1="-0.225" y1="1" x2="-0.625" y2="0.6" width="0.1" layer="51"/>
<wire x1="-0.125" y1="1" x2="-0.625" y2="0.5" width="0.1" layer="51"/>
<wire x1="-0.025" y1="1" x2="-0.625" y2="0.4" width="0.1" layer="51"/>
<wire x1="0.075" y1="1" x2="-0.625" y2="0.3" width="0.1" layer="51"/>
<wire x1="0.175" y1="1" x2="-0.625" y2="0.2" width="0.1" layer="51"/>
<wire x1="0.275" y1="1" x2="-0.625" y2="0.1" width="0.1" layer="51"/>
<wire x1="0.375" y1="1" x2="-0.625" y2="0" width="0.1" layer="51"/>
<wire x1="-0.625" y1="-1" x2="0.625" y2="-1" width="0.1" layer="51"/>
<wire x1="0.625" y1="-1" x2="0.625" y2="1" width="0.1" layer="51"/>
<wire x1="0.625" y1="1" x2="-0.625" y2="1" width="0.1" layer="51"/>
<wire x1="-0.625" y1="1" x2="-0.625" y2="-1" width="0.1" layer="51"/>
<wire x1="-0.3" y1="-1" x2="0.3" y2="-1" width="0.2" layer="21"/>
<wire x1="0.3" y1="-1" x2="0.3" y2="1" width="0.2" layer="21"/>
<wire x1="0.3" y1="1" x2="-0.3" y2="1" width="0.2" layer="21"/>
<wire x1="-0.3" y1="1" x2="-0.3" y2="-1" width="0.2" layer="21"/>
<circle x="-1.3" y="1.35" radius="0.125" width="0.25" layer="25"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;Small Outline Transistor 223&lt;/b&gt;&lt;p&gt;
PLASTIC PACKAGE CASE 318E-04&lt;br&gt;
Source: http://www.onsemi.co.jp .. LM137M-D.PDF</description>
<smd name="GND" x="-2.3" y="-3.15" dx="1.5" dy="2" layer="1"/>
<smd name="OUT" x="0" y="-3.15" dx="1.5" dy="2" layer="1"/>
<smd name="IN" x="2.3" y="-3.15" dx="1.5" dy="2" layer="1"/>
<rectangle x1="-0.9271" y1="1.1303" x2="0.9271" y2="4.3307" layer="51" rot="R270"/>
<rectangle x1="-0.9271" y1="-3.1623" x2="0.9271" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="-3.2385" y1="-3.1623" x2="-1.3843" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="1.3843" y1="-3.1623" x2="3.2385" y2="-2.2987" layer="51" rot="R270"/>
<wire x1="3.277" y1="1.778" x2="3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="3.277" y1="-1.778" x2="-3.277" y2="-1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="-1.778" x2="-3.277" y2="1.778" width="0.2032" layer="21"/>
<wire x1="-3.277" y1="1.778" x2="3.277" y2="1.778" width="0.2032" layer="21"/>
<rectangle x1="-0.9271" y1="1.1303" x2="0.9271" y2="4.3307" layer="51" rot="R270"/>
<rectangle x1="-0.9271" y1="-3.1623" x2="0.9271" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="-3.2385" y1="-3.1623" x2="-1.3843" y2="-2.2987" layer="51" rot="R270"/>
<rectangle x1="1.3843" y1="-3.1623" x2="3.2385" y2="-2.2987" layer="51" rot="R270"/>
<text x="-2.54" y="0.0508" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.54" y="-1.3208" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<smd name="VOUT" x="0" y="3.15" dx="3.8" dy="2" layer="1"/>
<text x="0.4" y="0.4" size="0.254" layer="48" font="vector">direction of pcb</text>
<text x="0.4" y="-0.05" size="0.254" layer="48" font="vector">transportation for</text>
<text x="0.4" y="-0.5" size="0.254" layer="48" font="vector">wavesoldering</text>
<wire x1="0" y1="-0.7" x2="0" y2="0.6" width="0.127" layer="48"/>
<wire x1="0" y1="0.6" x2="-0.2" y2="0.2" width="0.127" layer="48"/>
<wire x1="-0.2" y1="0.2" x2="0.2" y2="0.2" width="0.127" layer="48"/>
<wire x1="0.2" y1="0.2" x2="0" y2="0.6" width="0.127" layer="48"/>
<wire x1="0" y1="-0.7" x2="0.2" y2="-0.3" width="0.127" layer="48"/>
<wire x1="0.2" y1="-0.3" x2="-0.2" y2="-0.3" width="0.127" layer="48"/>
<wire x1="-0.2" y1="-0.3" x2="0" y2="-0.7" width="0.127" layer="48"/>
</package>
<package name="QFN50P500X500X100-33H">
<description>&lt;b&gt;QFN50P500X500X100-33N&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="-2.4892" y="1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="2" x="-2.4892" y="1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="3" x="-2.4892" y="0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="4" x="-2.4892" y="0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="5" x="-2.4892" y="-0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="6" x="-2.4892" y="-0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="7" x="-2.4892" y="-1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="8" x="-2.4892" y="-1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="9" x="-1.7526" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="10" x="-1.2446" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="11" x="-0.762" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="12" x="-0.254" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="13" x="0.254" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="14" x="0.762" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="15" x="1.2446" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="16" x="1.7526" y="-2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="17" x="2.4892" y="-1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="18" x="2.4892" y="-1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="19" x="2.4892" y="-0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="20" x="2.4892" y="-0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="21" x="2.4892" y="0.254" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="22" x="2.4892" y="0.762" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="23" x="2.4892" y="1.2446" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="24" x="2.4892" y="1.7526" dx="0.8128" dy="0.3048" layer="1"/>
<smd name="25" x="1.7526" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="26" x="1.2446" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="27" x="0.762" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="28" x="0.254" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="29" x="-0.254" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="30" x="-0.762" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="31" x="-1.2446" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<smd name="32" x="-1.7526" y="2.4892" dx="0.8128" dy="0.3048" layer="1" rot="R90"/>
<text x="-3.4544" y="5.08" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-3.4544" y="-6.985" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-1.2446" y1="-4.191" x2="-1.2446" y2="-3.1496" width="0.1524" layer="21"/>
<wire x1="-0.7366" y1="3.2004" x2="-0.7366" y2="4.2164" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.254" x2="4.2164" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="2.2352" x2="2.5654" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="2.2352" y1="-2.5654" x2="2.5654" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="2.5654" x2="-2.5654" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="-2.5654" x2="-2.2352" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="-2.5654" x2="2.5654" y2="-2.2352" width="0.1524" layer="21"/>
<wire x1="2.5654" y1="2.5654" x2="2.2352" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="2.5654" x2="-2.5654" y2="2.2352" width="0.1524" layer="21"/>
<wire x1="-2.5654" y1="-2.2352" x2="-2.5654" y2="-2.5654" width="0.1524" layer="21"/>
<pad name="33" x="0" y="0" drill="2.5" diameter="3.3528" shape="square"/>
</package>
<package name="TSSOP14">
<smd name="1" x="-2.54" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="2" x="-1.89" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="3" x="-1.24" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="4" x="-0.59" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="5" x="0.06" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="6" x="0.71" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="7" x="1.36" y="-2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="8" x="1.36" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="9" x="0.71" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="10" x="0.06" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="11" x="-0.59" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<smd name="12" x="-1.24" y="2.54" dx="0.35" dy="1.5" layer="1" rot="R180"/>
<wire x1="-3.09" y1="-2.19" x2="2.052" y2="-2.19" width="0.127" layer="21"/>
<wire x1="2.052" y1="-2.19" x2="2.052" y2="2.21" width="0.127" layer="21"/>
<wire x1="2.052" y1="2.21" x2="-3.09" y2="2.21" width="0.127" layer="21"/>
<wire x1="-3.09" y1="2.21" x2="-3.09" y2="0.485" width="0.127" layer="21"/>
<circle x="-2.2225" y="-1.27" radius="0.449" width="0.127" layer="21"/>
<wire x1="-3.09" y1="0.485" x2="-3.09" y2="-0.615" width="0.127" layer="21"/>
<wire x1="-3.09" y1="-0.615" x2="-3.09" y2="-2.19" width="0.127" layer="21"/>
<wire x1="-3.09" y1="0.485" x2="-3.09" y2="-0.615" width="0.127" layer="21" curve="-180"/>
<wire x1="-3.4925" y1="-3.4925" x2="2.6185" y2="-3.4925" width="0.127" layer="51"/>
<wire x1="2.6185" y1="-3.4925" x2="2.6185" y2="3.4925" width="0.127" layer="51"/>
<wire x1="2.6185" y1="3.4925" x2="-3.4925" y2="3.4925" width="0.127" layer="51"/>
<wire x1="-3.4925" y1="3.4925" x2="-3.4925" y2="-3.4925" width="0.127" layer="51"/>
<text x="-3.175" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.4925" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<text x="-4.445" y="-3.4925" size="1.27" layer="51">1</text>
<smd name="13" x="-1.89" y="2.54" dx="0.35" dy="1.5" layer="1"/>
<smd name="14" x="-2.54" y="2.54" dx="0.35" dy="1.5" layer="1"/>
</package>
<package name="SOP65P488X118-8N">
<description>&lt;b&gt;8 Lead MSOP&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="-2.212" y="0.975" dx="1.425" dy="0.45" layer="1"/>
<smd name="2" x="-2.212" y="0.325" dx="1.425" dy="0.45" layer="1"/>
<smd name="3" x="-2.212" y="-0.325" dx="1.425" dy="0.45" layer="1"/>
<smd name="4" x="-2.212" y="-0.975" dx="1.425" dy="0.45" layer="1"/>
<smd name="5" x="2.212" y="-0.975" dx="1.425" dy="0.45" layer="1"/>
<smd name="6" x="2.212" y="-0.325" dx="1.425" dy="0.45" layer="1"/>
<smd name="7" x="2.212" y="0.325" dx="1.425" dy="0.45" layer="1"/>
<smd name="8" x="2.212" y="0.975" dx="1.425" dy="0.45" layer="1"/>
<text x="0" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="0" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-3.175" y1="1.8" x2="3.175" y2="1.8" width="0.05" layer="51"/>
<wire x1="3.175" y1="1.8" x2="3.175" y2="-1.8" width="0.05" layer="51"/>
<wire x1="3.175" y1="-1.8" x2="-3.175" y2="-1.8" width="0.05" layer="51"/>
<wire x1="-3.175" y1="-1.8" x2="-3.175" y2="1.8" width="0.05" layer="51"/>
<wire x1="-1.5" y1="1.5" x2="-0.85" y2="1.5" width="0.1" layer="51"/>
<wire x1="-0.85" y1="1.5" x2="1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="1.5" y1="-1.5" x2="-1.5" y2="-1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="-1.5" x2="-1.5" y2="0.85" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0.85" x2="-1.5" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.5" y1="0.85" x2="-0.85" y2="1.5" width="0.1" layer="51"/>
<wire x1="-1.15" y1="1.5" x2="1.15" y2="1.5" width="0.2" layer="21"/>
<wire x1="1.15" y1="1.5" x2="1.15" y2="-1.5" width="0.2" layer="21"/>
<wire x1="1.15" y1="-1.5" x2="-1.15" y2="-1.5" width="0.2" layer="21"/>
<wire x1="-1.15" y1="-1.5" x2="-1.15" y2="1.5" width="0.2" layer="21"/>
<wire x1="-2.925" y1="1.55" x2="-1.5" y2="1.55" width="0.2" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="TLV320AIC3204IRHBR">
<wire x1="5.08" y1="2.54" x2="43.18" y2="2.54" width="0.254" layer="94"/>
<wire x1="43.18" y1="-43.18" x2="43.18" y2="2.54" width="0.254" layer="94"/>
<wire x1="43.18" y1="-43.18" x2="5.08" y2="-43.18" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-43.18" width="0.254" layer="94"/>
<text x="44.45" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="44.45" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="MCLK_(1)" x="0" y="0" length="middle"/>
<pin name="BCLK" x="0" y="-2.54" length="middle"/>
<pin name="WCLK" x="0" y="-5.08" length="middle"/>
<pin name="DIN/MFP1" x="0" y="-7.62" length="middle"/>
<pin name="DOUT/MFP2" x="0" y="-10.16" length="middle"/>
<pin name="OVIDD" x="0" y="-12.7" length="middle"/>
<pin name="IOVSS" x="0" y="-15.24" length="middle"/>
<pin name="SCLK/MFP3" x="0" y="-17.78" length="middle"/>
<pin name="SCL/SSZ" x="0" y="-20.32" length="middle"/>
<pin name="SDA/MOSI" x="0" y="-22.86" length="middle"/>
<pin name="MISO/MFP4" x="0" y="-25.4" length="middle"/>
<pin name="SPI_SELECT" x="0" y="-27.94" length="middle"/>
<pin name="IN1_L" x="0" y="-30.48" length="middle"/>
<pin name="IN1_R" x="0" y="-33.02" length="middle"/>
<pin name="IN2_L" x="0" y="-35.56" length="middle"/>
<pin name="IN2_R" x="0" y="-38.1" length="middle"/>
<pin name="AVSS" x="0" y="-40.64" length="middle"/>
<pin name="EP" x="48.26" y="0" length="middle" rot="R180"/>
<pin name="GPIO/MFP5_(32)" x="48.26" y="-2.54" length="middle" rot="R180"/>
<pin name="~RESET" x="48.26" y="-5.08" length="middle" rot="R180"/>
<pin name="LDO_SELECT" x="48.26" y="-7.62" length="middle" rot="R180"/>
<pin name="DVDD" x="48.26" y="-10.16" length="middle" rot="R180"/>
<pin name="DVSS" x="48.26" y="-12.7" length="middle" rot="R180"/>
<pin name="HPR" x="48.26" y="-15.24" length="middle" rot="R180"/>
<pin name="LDOIN" x="48.26" y="-17.78" length="middle" rot="R180"/>
<pin name="HPL" x="48.26" y="-20.32" length="middle" rot="R180"/>
<pin name="AVDD" x="48.26" y="-22.86" length="middle" rot="R180"/>
<pin name="LOR" x="48.26" y="-25.4" length="middle" rot="R180"/>
<pin name="LOL" x="48.26" y="-27.94" length="middle" rot="R180"/>
<pin name="IN3_R" x="48.26" y="-30.48" length="middle" rot="R180"/>
<pin name="IN3_L" x="48.26" y="-33.02" length="middle" rot="R180"/>
<pin name="MICBIAS" x="48.26" y="-35.56" length="middle" rot="R180"/>
<pin name="REF" x="48.26" y="-38.1" length="middle" rot="R180"/>
</symbol>
<symbol name="SSM2305RMZ-REEL7">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="SD" x="0" y="0" length="middle"/>
<pin name="NC" x="0" y="-2.54" length="middle" direction="nc"/>
<pin name="IN+" x="0" y="-5.08" length="middle"/>
<pin name="IN-" x="0" y="-7.62" length="middle"/>
<pin name="OUT-" x="27.94" y="0" length="middle" rot="R180"/>
<pin name="GND" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="VDD" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="OUT+" x="27.94" y="-7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="MASWSS0115TR-3000">
<wire x1="5.08" y1="2.54" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="20.32" y2="2.54" width="0.254" layer="94"/>
<wire x1="20.32" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="21.59" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="21.59" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="RF1" x="0" y="0" length="middle"/>
<pin name="GND" x="0" y="-2.54" length="middle" direction="pwr"/>
<pin name="RF2" x="0" y="-5.08" length="middle"/>
<pin name="V1" x="25.4" y="0" length="middle" rot="R180"/>
<pin name="RFC" x="25.4" y="-2.54" length="middle" rot="R180"/>
<pin name="V2" x="25.4" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="LINEAR-REGULATOR-OUT2">
<pin name="IN" x="-12.7" y="2.54" length="middle" direction="pwr"/>
<pin name="OUT@1" x="12.7" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="GND" x="0" y="-7.62" length="middle" direction="pwr" rot="R90"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.254" layer="94"/>
<pin name="OUT@2" x="12.7" y="0" length="middle" direction="sup" rot="R180"/>
</symbol>
<symbol name="OPAMP">
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<text x="2.54" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="PWR+-">
<pin name="V+" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<pin name="V-" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<text x="1.27" y="3.175" size="0.8128" layer="93" rot="R90">V+</text>
<text x="1.27" y="-4.445" size="0.8128" layer="93" rot="R90">V-</text>
<text x="-2.54" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
</symbol>
<symbol name="MCP4901T-E_MS">
<wire x1="5.08" y1="2.54" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="22.86" y2="2.54" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.16" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-10.16" width="0.254" layer="94"/>
<text x="24.13" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="24.13" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="VDD" x="0" y="0" length="middle" direction="pwr"/>
<pin name="!CS" x="0" y="-2.54" length="middle" direction="in"/>
<pin name="SCK" x="0" y="-5.08" length="middle" direction="in"/>
<pin name="SDI" x="0" y="-7.62" length="middle" direction="in"/>
<pin name="VOUT" x="27.94" y="0" length="middle" direction="out" rot="R180"/>
<pin name="VSS" x="27.94" y="-2.54" length="middle" direction="pwr" rot="R180"/>
<pin name="VREF" x="27.94" y="-5.08" length="middle" direction="in" rot="R180"/>
<pin name="!LDAC" x="27.94" y="-7.62" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="TLV320AIC3204IRHBR" prefix="IC">
<description>&lt;b&gt;Very-Low-Power Stereo Audio CODEC With PowerTune&amp;#153; Technology&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="http://www.ti.com/lit/gpn/tlv320aic3204"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="TLV320AIC3204IRHBR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN50P500X500X100-33N">
<connects>
<connect gate="G$1" pin="AVDD" pad="24"/>
<connect gate="G$1" pin="AVSS" pad="17"/>
<connect gate="G$1" pin="BCLK" pad="2"/>
<connect gate="G$1" pin="DIN/MFP1" pad="4"/>
<connect gate="G$1" pin="DOUT/MFP2" pad="5"/>
<connect gate="G$1" pin="DVDD" pad="29"/>
<connect gate="G$1" pin="DVSS" pad="28"/>
<connect gate="G$1" pin="EP" pad="33"/>
<connect gate="G$1" pin="GPIO/MFP5_(32)" pad="32"/>
<connect gate="G$1" pin="HPL" pad="25"/>
<connect gate="G$1" pin="HPR" pad="27"/>
<connect gate="G$1" pin="IN1_L" pad="13"/>
<connect gate="G$1" pin="IN1_R" pad="14"/>
<connect gate="G$1" pin="IN2_L" pad="15"/>
<connect gate="G$1" pin="IN2_R" pad="16"/>
<connect gate="G$1" pin="IN3_L" pad="20"/>
<connect gate="G$1" pin="IN3_R" pad="21"/>
<connect gate="G$1" pin="IOVSS" pad="7"/>
<connect gate="G$1" pin="LDOIN" pad="26"/>
<connect gate="G$1" pin="LDO_SELECT" pad="30"/>
<connect gate="G$1" pin="LOL" pad="22"/>
<connect gate="G$1" pin="LOR" pad="23"/>
<connect gate="G$1" pin="MCLK_(1)" pad="1"/>
<connect gate="G$1" pin="MICBIAS" pad="19"/>
<connect gate="G$1" pin="MISO/MFP4" pad="11"/>
<connect gate="G$1" pin="OVIDD" pad="6"/>
<connect gate="G$1" pin="REF" pad="18"/>
<connect gate="G$1" pin="SCL/SSZ" pad="9"/>
<connect gate="G$1" pin="SCLK/MFP3" pad="8"/>
<connect gate="G$1" pin="SDA/MOSI" pad="10"/>
<connect gate="G$1" pin="SPI_SELECT" pad="12"/>
<connect gate="G$1" pin="WCLK" pad="3"/>
<connect gate="G$1" pin="~RESET" pad="31"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Very-Low-Power Stereo Audio CODEC With PowerTune&amp;#153; Technology" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Texas Instruments" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="TLV320AIC3204IRHBR" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="595-V320AIC3204IRHBR" constant="no"/>
</technology>
</technologies>
</device>
<device name="H" package="QFN50P500X500X100-33H">
<connects>
<connect gate="G$1" pin="AVDD" pad="24"/>
<connect gate="G$1" pin="AVSS" pad="17"/>
<connect gate="G$1" pin="BCLK" pad="2"/>
<connect gate="G$1" pin="DIN/MFP1" pad="4"/>
<connect gate="G$1" pin="DOUT/MFP2" pad="5"/>
<connect gate="G$1" pin="DVDD" pad="29"/>
<connect gate="G$1" pin="DVSS" pad="28"/>
<connect gate="G$1" pin="EP" pad="33"/>
<connect gate="G$1" pin="GPIO/MFP5_(32)" pad="32"/>
<connect gate="G$1" pin="HPL" pad="25"/>
<connect gate="G$1" pin="HPR" pad="27"/>
<connect gate="G$1" pin="IN1_L" pad="13"/>
<connect gate="G$1" pin="IN1_R" pad="14"/>
<connect gate="G$1" pin="IN2_L" pad="15"/>
<connect gate="G$1" pin="IN2_R" pad="16"/>
<connect gate="G$1" pin="IN3_L" pad="20"/>
<connect gate="G$1" pin="IN3_R" pad="21"/>
<connect gate="G$1" pin="IOVSS" pad="7"/>
<connect gate="G$1" pin="LDOIN" pad="26"/>
<connect gate="G$1" pin="LDO_SELECT" pad="30"/>
<connect gate="G$1" pin="LOL" pad="22"/>
<connect gate="G$1" pin="LOR" pad="23"/>
<connect gate="G$1" pin="MCLK_(1)" pad="1"/>
<connect gate="G$1" pin="MICBIAS" pad="19"/>
<connect gate="G$1" pin="MISO/MFP4" pad="11"/>
<connect gate="G$1" pin="OVIDD" pad="6"/>
<connect gate="G$1" pin="REF" pad="18"/>
<connect gate="G$1" pin="SCL/SSZ" pad="9"/>
<connect gate="G$1" pin="SCLK/MFP3" pad="8"/>
<connect gate="G$1" pin="SDA/MOSI" pad="10"/>
<connect gate="G$1" pin="SPI_SELECT" pad="12"/>
<connect gate="G$1" pin="WCLK" pad="3"/>
<connect gate="G$1" pin="~RESET" pad="31"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SSM2305RMZ-REEL7" prefix="IC">
<description>&lt;b&gt;Filterless High Efficiency Mono 2.8 W Class-D Audio Amplifier&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/SSM2305RMZ-REEL7.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="SSM2305RMZ-REEL7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P490X110-8N">
<connects>
<connect gate="G$1" pin="GND" pad="7"/>
<connect gate="G$1" pin="IN+" pad="3"/>
<connect gate="G$1" pin="IN-" pad="4"/>
<connect gate="G$1" pin="NC" pad="2"/>
<connect gate="G$1" pin="OUT+" pad="5"/>
<connect gate="G$1" pin="OUT-" pad="8"/>
<connect gate="G$1" pin="SD" pad="1"/>
<connect gate="G$1" pin="VDD" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Filterless High Efficiency Mono 2.8 W Class-D Audio Amplifier" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Analog Devices" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="SSM2305RMZ-REEL7" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="584-SSM2305RMZ-R7" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MASWSS0115TR-3000" prefix="S">
<description>&lt;b&gt;RF Switch ICs DC-3.0GHz ISO 22dB IL &lt;.3dB @ 2.4GHz&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/MASWSS0115TR-3000.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MASWSS0115TR-3000" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT65P210X110-6N">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="RF1" pad="1"/>
<connect gate="G$1" pin="RF2" pad="3"/>
<connect gate="G$1" pin="RFC" pad="5"/>
<connect gate="G$1" pin="V1" pad="6"/>
<connect gate="G$1" pin="V2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="RF Switch ICs DC-3.0GHz ISO 22dB IL &lt;.3dB @ 2.4GHz" constant="no"/>
<attribute name="HEIGHT" value="1.1mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="MACOM" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MASWSS0115TR-3000" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="937-MASWSS0115-T" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AZ1086?-*" prefix="IC">
<gates>
<gate name="G$1" symbol="LINEAR-REGULATOR-OUT2" x="0" y="0"/>
</gates>
<devices>
<device name="H" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IN" pad="IN"/>
<connect gate="G$1" pin="OUT@1" pad="OUT"/>
<connect gate="G$1" pin="OUT@2" pad="VOUT"/>
</connects>
<technologies>
<technology name="3.3"/>
<technology name="5.0"/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LMV324">
<gates>
<gate name="G$1" symbol="OPAMP" x="-25.4" y="15.24"/>
<gate name="G$2" symbol="PWR+-" x="-25.4" y="15.24" addlevel="request"/>
<gate name="G$3" symbol="OPAMP" x="2.54" y="15.24"/>
<gate name="G$4" symbol="OPAMP" x="-25.4" y="-7.62"/>
<gate name="G$5" symbol="OPAMP" x="2.54" y="-7.62"/>
</gates>
<devices>
<device name="" package="TSSOP14">
<connects>
<connect gate="G$1" pin="+IN" pad="3"/>
<connect gate="G$1" pin="-IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="1"/>
<connect gate="G$2" pin="V+" pad="4"/>
<connect gate="G$2" pin="V-" pad="11"/>
<connect gate="G$3" pin="+IN" pad="5"/>
<connect gate="G$3" pin="-IN" pad="6"/>
<connect gate="G$3" pin="OUT" pad="7"/>
<connect gate="G$4" pin="+IN" pad="10"/>
<connect gate="G$4" pin="-IN" pad="9"/>
<connect gate="G$4" pin="OUT" pad="8"/>
<connect gate="G$5" pin="+IN" pad="12"/>
<connect gate="G$5" pin="-IN" pad="13"/>
<connect gate="G$5" pin="OUT" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MCP4901T-E_MS" prefix="IC">
<description>&lt;b&gt;Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://mouser.componentsearchengine.com/Datasheets/1/MCP4901T-E_MS.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="MCP4901T-E_MS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOP65P488X118-8N">
<connects>
<connect gate="G$1" pin="!CS" pad="2"/>
<connect gate="G$1" pin="!LDAC" pad="5"/>
<connect gate="G$1" pin="SCK" pad="3"/>
<connect gate="G$1" pin="SDI" pad="4"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="8"/>
<connect gate="G$1" pin="VREF" pad="6"/>
<connect gate="G$1" pin="VSS" pad="7"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Digital to Analog Converters - DAC Single 12-bit DAC w/SPI interface" constant="no"/>
<attribute name="HEIGHT" value="1.18mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Microchip" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="MCP4901T-E/MS" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AnalogDevicesMixers">
<packages>
<package name="QFN-16_UF">
<smd name="1" x="-1.827396875" y="0.975" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="2" x="-1.827396875" y="0.325" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="3" x="-1.827396875" y="-0.325" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="4" x="-1.827396875" y="-0.975" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="5" x="-0.975" y="-1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="6" x="-0.325" y="-1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="7" x="0.325" y="-1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="8" x="0.975" y="-1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="9" x="1.827396875" y="-0.975" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="10" x="1.827396875" y="-0.325" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="11" x="1.827396875" y="0.325" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="12" x="1.827396875" y="0.975" dx="0.35" dy="1.0548" layer="1" rot="R270"/>
<smd name="13" x="0.975" y="1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="14" x="0.325" y="1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="15" x="-0.325" y="1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="16" x="-0.975" y="1.827396875" dx="0.35" dy="1.0548" layer="1" rot="R180"/>
<smd name="17" x="0" y="0" dx="2.25" dy="2.25" layer="1" cream="no"/>
<wire x1="-1.4732" y1="2.1844" x2="-2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="1.4732" x2="2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="1.4732" y1="-2.1844" x2="2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-2.1844" x2="-1.4732" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="-2.1844" x2="2.1844" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="2.1844" x2="1.4732" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="2.1844" x2="-2.1844" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-1.4732" x2="-2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<polygon width="0" layer="51">
<vertex x="2.8628" y="-0.1345"/>
<vertex x="2.8628" y="-0.5155"/>
<vertex x="2.6088" y="-0.5155"/>
<vertex x="2.6088" y="-0.1345"/>
</polygon>
<text x="-3.556" y="0.6604" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0" layer="31">
<vertex x="-1.125" y="1.125"/>
<vertex x="-1.125" y="-1.125"/>
<vertex x="1.125" y="-1.125"/>
<vertex x="1.125" y="1.125"/>
</polygon>
<text x="-0.635" y="0" size="0.0254" layer="31" ratio="6" rot="SR0">Copyright (C) 2015 Accelerated Designs. All rights reserved</text>
<wire x1="-2.0574" y1="0.7874" x2="-0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="2.0574" x2="1.143" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="1.143" y1="2.0574" x2="0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.1524" y1="2.0574" x2="0.508" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.0574" x2="0.1524" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.0574" x2="-0.1524" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1524" y1="2.0574" x2="-0.508" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="2.0574" x2="-0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.7874" y1="2.0574" x2="-1.143" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.7874" x2="-2.0574" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="1.143" x2="-2.0574" y2="0.7874" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.1524" x2="-2.0574" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.508" x2="-2.0574" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.508" x2="-2.0574" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.1524" x2="-2.0574" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-1.143" x2="-2.0574" y2="-0.7874" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.7874" x2="-2.0574" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.7874" y1="-2.0574" x2="-1.143" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-2.0574" x2="-0.7874" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1524" y1="-2.0574" x2="-0.508" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-2.0574" x2="-0.1524" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-2.0574" x2="0.1524" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.1524" y1="-2.0574" x2="0.508" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-2.0574" x2="0.7874" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="-2.0574" x2="1.143" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.7874" x2="2.0574" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-1.143" x2="2.0574" y2="-0.7874" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.1524" x2="2.0574" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.508" x2="2.0574" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.508" x2="2.0574" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.1524" x2="2.0574" y2="0.508" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="1.143" x2="2.0574" y2="0.7874" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.7874" x2="2.0574" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-2.0574" x2="2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-2.0574" x2="2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="2.0574" x2="-2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="2.0574" x2="-2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<text x="-1.8796" y="0.6604" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
</package>
<package name="QFN-16_UF-H">
<smd name="1" x="-1.878196875" y="0.975" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="2" x="-1.878196875" y="0.325" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="3" x="-1.878196875" y="-0.325" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="4" x="-1.878196875" y="-0.975" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="5" x="-0.975" y="-1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="6" x="-0.325" y="-1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="7" x="0.325" y="-1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="8" x="0.975" y="-1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="9" x="1.878196875" y="-0.975" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="10" x="1.878196875" y="-0.325" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="11" x="1.878196875" y="0.325" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="12" x="1.878196875" y="0.975" dx="0.35" dy="1.1564" layer="1" rot="R270"/>
<smd name="13" x="0.975" y="1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="14" x="0.325" y="1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="15" x="-0.325" y="1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<smd name="16" x="-0.975" y="1.878196875" dx="0.35" dy="1.1564" layer="1" rot="R180"/>
<wire x1="-1.4732" y1="2.1844" x2="-2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="1.4732" x2="2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="1.4732" y1="-2.1844" x2="2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-2.1844" x2="-1.4732" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="-2.1844" x2="2.1844" y2="-1.4732" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="2.1844" x2="1.4732" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="2.1844" x2="-2.1844" y2="1.4732" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-1.4732" x2="-2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<polygon width="0" layer="51">
<vertex x="2.9644" y="-0.1345"/>
<vertex x="2.9644" y="-0.5155"/>
<vertex x="2.7104" y="-0.5155"/>
<vertex x="2.7104" y="-0.1345"/>
</polygon>
<text x="-3.6576" y="0.6604" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0" layer="31">
<vertex x="-1.125" y="1.125"/>
<vertex x="-1.125" y="-1.125"/>
<vertex x="1.125" y="-1.125"/>
<vertex x="1.125" y="1.125"/>
</polygon>
<text x="-0.635" y="0" size="0.0254" layer="31" ratio="6" rot="SR0">Copyright (C) 2016 Accelerated Designs. All rights reserved</text>
<wire x1="-2.0574" y1="0.7874" x2="-0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="2.0574" x2="1.143" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="1.143" y1="2.0574" x2="0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.1524" y1="2.0574" x2="0.508" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.508" y1="2.0574" x2="0.1524" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="2.0574" x2="-0.1524" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1524" y1="2.0574" x2="-0.508" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="2.0574" x2="-0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.7874" y1="2.0574" x2="-1.143" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.7874" x2="-2.0574" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="1.143" x2="-2.0574" y2="0.7874" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.1524" x2="-2.0574" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.508" x2="-2.0574" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.508" x2="-2.0574" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.1524" x2="-2.0574" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-1.143" x2="-2.0574" y2="-0.7874" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.7874" x2="-2.0574" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.7874" y1="-2.0574" x2="-1.143" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-2.0574" x2="-0.7874" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1524" y1="-2.0574" x2="-0.508" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-2.0574" x2="-0.1524" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.508" y1="-2.0574" x2="0.1524" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.1524" y1="-2.0574" x2="0.508" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-2.0574" x2="0.7874" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.7874" y1="-2.0574" x2="1.143" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.7874" x2="2.0574" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-1.143" x2="2.0574" y2="-0.7874" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.1524" x2="2.0574" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.508" x2="2.0574" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.508" x2="2.0574" y2="0.1524" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.1524" x2="2.0574" y2="0.508" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="1.143" x2="2.0574" y2="0.7874" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.7874" x2="2.0574" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-2.0574" x2="2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-2.0574" x2="2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="2.0574" x2="-2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="2.0574" x2="-2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<text x="-1.8796" y="0.6604" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<pad name="17" x="0" y="0" drill="1.6" diameter="2.25" shape="square"/>
</package>
<package name="QFN-24_UF">
<smd name="1" x="-1.9524" y="1.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="2" x="-1.9524" y="0.75" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="3" x="-1.9524" y="0.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="4" x="-1.9524" y="-0.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="5" x="-1.9524" y="-0.75" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="6" x="-1.9524" y="-1.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="7" x="-1.25" y="-1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="8" x="-0.75" y="-1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="9" x="-0.25" y="-1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="10" x="0.25" y="-1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="11" x="0.75" y="-1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="12" x="1.25" y="-1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="13" x="1.9524" y="-1.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="14" x="1.9524" y="-0.75" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="15" x="1.9524" y="-0.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="16" x="1.9524" y="0.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="17" x="1.9524" y="0.75" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="18" x="1.9524" y="1.25" dx="0.3" dy="0.8048" layer="1" rot="R270"/>
<smd name="19" x="1.25" y="1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="20" x="0.75" y="1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="21" x="0.25" y="1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="22" x="-0.25" y="1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="24" x="-1.25" y="1.9" dx="0.3" dy="0.8048" layer="1" rot="R180"/>
<smd name="25" x="0" y="0" dx="2.55" dy="2.55" layer="1" cream="no"/>
<wire x1="-1.7272" y1="2.1844" x2="-2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="1.7272" x2="2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="1.7272" y1="-2.1844" x2="2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-2.1844" x2="-1.7272" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="-2.1844" x2="2.1844" y2="-1.7272" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="2.1844" x2="1.7272" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="2.1844" x2="-2.1844" y2="1.7272" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-1.7272" x2="-2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<polygon width="0" layer="51">
<vertex x="0.0595" y="-2.6088"/>
<vertex x="0.0595" y="-2.8628"/>
<vertex x="0.4405" y="-2.8628"/>
<vertex x="0.4405" y="-2.6088"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.5595" y="2.6088"/>
<vertex x="0.5595" y="2.8628"/>
<vertex x="0.9405" y="2.8628"/>
<vertex x="0.9405" y="2.6088"/>
</polygon>
<text x="-3.556" y="0.8636" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0" layer="31">
<vertex x="-1.175" y="1.175"/>
<vertex x="-1.175" y="0.1"/>
<vertex x="-0.1" y="0.1"/>
<vertex x="-0.1" y="1.175"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.175" y="-0.1"/>
<vertex x="-1.175" y="-1.175"/>
<vertex x="-0.1" y="-1.175"/>
<vertex x="-0.1" y="-0.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="0.1" y="1.175"/>
<vertex x="0.1" y="0.1"/>
<vertex x="1.175" y="0.1"/>
<vertex x="1.175" y="1.175"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="0.1" y="-0.1"/>
<vertex x="0.1" y="-1.175"/>
<vertex x="1.175" y="-1.175"/>
<vertex x="1.175" y="-0.1"/>
</polygon>
<wire x1="-2.0574" y1="0.7874" x2="-0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="1.0922" y1="2.0574" x2="1.397" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.0574" x2="1.0922" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="2.0574" x2="0.889" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.0574" x2="0.6096" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.1016" y1="2.0574" x2="0.4064" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.4064" y1="2.0574" x2="0.1016" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.4064" y1="2.0574" x2="-0.1016" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1016" y1="2.0574" x2="-0.4064" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.0574" x2="-0.6096" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="2.0574" x2="-0.889" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="2.0574" x2="-1.0922" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-1.0922" y1="2.0574" x2="-1.397" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="1.0922" x2="-2.0574" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="1.397" x2="-2.0574" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.6096" x2="-2.0574" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.889" x2="-2.0574" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.1016" x2="-2.0574" y2="0.4064" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.4064" x2="-2.0574" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.4064" x2="-2.0574" y2="-0.1016" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.1016" x2="-2.0574" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.889" x2="-2.0574" y2="-0.6096" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.6096" x2="-2.0574" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-1.397" x2="-2.0574" y2="-1.0922" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-1.0922" x2="-2.0574" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-1.0922" y1="-2.0574" x2="-1.397" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-2.0574" x2="-1.0922" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="-2.0574" x2="-0.889" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-2.0574" x2="-0.6096" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1016" y1="-2.0574" x2="-0.4064" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.4064" y1="-2.0574" x2="-0.1016" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.4064" y1="-2.0574" x2="0.1016" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.1016" y1="-2.0574" x2="0.4064" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.0574" x2="0.6096" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="-2.0574" x2="0.889" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-2.0574" x2="1.0922" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="1.0922" y1="-2.0574" x2="1.397" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-1.0922" x2="2.0574" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-1.397" x2="2.0574" y2="-1.0922" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.6096" x2="2.0574" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.889" x2="2.0574" y2="-0.6096" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.1016" x2="2.0574" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.4064" x2="2.0574" y2="-0.1016" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.4064" x2="2.0574" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.1016" x2="2.0574" y2="0.4064" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.889" x2="2.0574" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.6096" x2="2.0574" y2="0.889" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="1.397" x2="2.0574" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="1.0922" x2="2.0574" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-2.0574" x2="2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-2.0574" x2="2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="2.0574" x2="-2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="2.0574" x2="-2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<text x="-2.1336" y="0.8636" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
</package>
<package name="QFN-24_UF-H">
<smd name="1" x="-2.0032" y="1.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="2" x="-2.0032" y="0.75" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="3" x="-2.0032" y="0.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="4" x="-2.0032" y="-0.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="5" x="-2.0032" y="-0.75" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="6" x="-2.0032" y="-1.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="7" x="-1.25" y="-2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="8" x="-0.75" y="-2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="9" x="-0.25" y="-2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="10" x="0.25" y="-2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="11" x="0.75" y="-2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="12" x="1.25" y="-2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="13" x="2.0032" y="-1.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="14" x="2.0032" y="-0.75" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="15" x="2.0032" y="-0.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="16" x="2.0032" y="0.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="17" x="2.0032" y="0.75" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="18" x="2.0032" y="1.25" dx="0.3" dy="0.9064" layer="1" rot="R270"/>
<smd name="19" x="1.25" y="2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="20" x="0.75" y="2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="21" x="0.25" y="2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="22" x="-0.25" y="2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="23" x="-0.75" y="2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<smd name="24" x="-1.25" y="2" dx="0.3" dy="0.9064" layer="1" rot="R180"/>
<wire x1="-1.7272" y1="2.1844" x2="-2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="1.7272" x2="2.1844" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="1.7272" y1="-2.1844" x2="2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-2.1844" x2="-1.7272" y2="-2.1844" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="-2.1844" x2="2.1844" y2="-1.7272" width="0.1524" layer="51"/>
<wire x1="2.1844" y1="2.1844" x2="1.7272" y2="2.1844" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="2.1844" x2="-2.1844" y2="1.7272" width="0.1524" layer="51"/>
<wire x1="-2.1844" y1="-1.7272" x2="-2.1844" y2="-2.1844" width="0.1524" layer="51"/>
<polygon width="0" layer="51">
<vertex x="0.0595" y="-2.7104"/>
<vertex x="0.0595" y="-2.9644"/>
<vertex x="0.4405" y="-2.9644"/>
<vertex x="0.4405" y="-2.7104"/>
</polygon>
<polygon width="0" layer="51">
<vertex x="0.5595" y="2.7104"/>
<vertex x="0.5595" y="2.9644"/>
<vertex x="0.9405" y="2.9644"/>
<vertex x="0.9405" y="2.7104"/>
</polygon>
<text x="-3.6576" y="0.8636" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<polygon width="0" layer="31">
<vertex x="-1.175" y="1.175"/>
<vertex x="-1.175" y="0.1"/>
<vertex x="-0.1" y="0.1"/>
<vertex x="-0.1" y="1.175"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="-1.175" y="-0.1"/>
<vertex x="-1.175" y="-1.175"/>
<vertex x="-0.1" y="-1.175"/>
<vertex x="-0.1" y="-0.1"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="0.1" y="1.175"/>
<vertex x="0.1" y="0.1"/>
<vertex x="1.175" y="0.1"/>
<vertex x="1.175" y="1.175"/>
</polygon>
<polygon width="0" layer="31">
<vertex x="0.1" y="-0.1"/>
<vertex x="0.1" y="-1.175"/>
<vertex x="1.175" y="-1.175"/>
<vertex x="1.175" y="-0.1"/>
</polygon>
<wire x1="-2.0574" y1="0.7874" x2="-0.7874" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="1.0922" y1="2.0574" x2="1.397" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.0574" x2="1.0922" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="2.0574" x2="0.889" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.0574" x2="0.6096" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.1016" y1="2.0574" x2="0.4064" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="0.4064" y1="2.0574" x2="0.1016" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.4064" y1="2.0574" x2="-0.1016" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1016" y1="2.0574" x2="-0.4064" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.0574" x2="-0.6096" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="2.0574" x2="-0.889" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="2.0574" x2="-1.0922" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-1.0922" y1="2.0574" x2="-1.397" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="1.0922" x2="-2.0574" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="1.397" x2="-2.0574" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.6096" x2="-2.0574" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.889" x2="-2.0574" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.1016" x2="-2.0574" y2="0.4064" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="0.4064" x2="-2.0574" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.4064" x2="-2.0574" y2="-0.1016" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.1016" x2="-2.0574" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.889" x2="-2.0574" y2="-0.6096" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-0.6096" x2="-2.0574" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-1.397" x2="-2.0574" y2="-1.0922" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-1.0922" x2="-2.0574" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-1.0922" y1="-2.0574" x2="-1.397" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="-2.0574" x2="-1.0922" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.6096" y1="-2.0574" x2="-0.889" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-2.0574" x2="-0.6096" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.1016" y1="-2.0574" x2="-0.4064" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="-0.4064" y1="-2.0574" x2="-0.1016" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.4064" y1="-2.0574" x2="0.1016" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.1016" y1="-2.0574" x2="0.4064" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.0574" x2="0.6096" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="0.6096" y1="-2.0574" x2="0.889" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="1.397" y1="-2.0574" x2="1.0922" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="1.0922" y1="-2.0574" x2="1.397" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-1.0922" x2="2.0574" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-1.397" x2="2.0574" y2="-1.0922" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.6096" x2="2.0574" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.889" x2="2.0574" y2="-0.6096" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.1016" x2="2.0574" y2="-0.4064" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-0.4064" x2="2.0574" y2="-0.1016" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.4064" x2="2.0574" y2="0.1016" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.1016" x2="2.0574" y2="0.4064" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.889" x2="2.0574" y2="0.6096" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="0.6096" x2="2.0574" y2="0.889" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="1.397" x2="2.0574" y2="1.0922" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="1.0922" x2="2.0574" y2="1.397" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="-2.0574" x2="2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="-2.0574" x2="2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="2.0574" y1="2.0574" x2="-2.0574" y2="2.0574" width="0.1524" layer="21"/>
<wire x1="-2.0574" y1="2.0574" x2="-2.0574" y2="-2.0574" width="0.1524" layer="21"/>
<text x="-2.1336" y="0.8636" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<pad name="25" x="0" y="0" drill="1.6" diameter="2.25" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="LT5506EUF_1">
<pin name="GND_2" x="0" y="0" direction="pwr"/>
<pin name="IF+" x="0" y="-2.54" direction="in"/>
<pin name="IF-" x="0" y="-5.08" direction="in"/>
<pin name="GND_3" x="0" y="-7.62" direction="pwr"/>
<pin name="VCC_2" x="0" y="-10.16" direction="pwr"/>
<pin name="VCTRL" x="0" y="-12.7" direction="in"/>
<pin name="IFDET" x="0" y="-15.24" direction="out"/>
<pin name="VCC" x="0" y="-17.78" direction="pwr"/>
<pin name="EN" x="45.72" y="-20.32" direction="in" rot="R180"/>
<pin name="2XLO-" x="45.72" y="-17.78" direction="in" rot="R180"/>
<pin name="2XLO+" x="45.72" y="-15.24" direction="in" rot="R180"/>
<pin name="STBY" x="45.72" y="-12.7" direction="in" rot="R180"/>
<pin name="QOUT-" x="45.72" y="-10.16" direction="out" rot="R180"/>
<pin name="QOUT+" x="45.72" y="-7.62" direction="out" rot="R180"/>
<pin name="IOUT-" x="45.72" y="-5.08" direction="out" rot="R180"/>
<pin name="IOUT+" x="45.72" y="-2.54" direction="out" rot="R180"/>
<pin name="GND" x="45.72" y="0" direction="pwr" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-25.4" x2="38.1" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="38.1" y1="-25.4" x2="38.1" y2="5.08" width="0.1524" layer="94"/>
<wire x1="38.1" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="18.1356" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="17.5006" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
<symbol name="LTC5598IUFTRPBF">
<pin name="EN" x="0" y="0" direction="in"/>
<pin name="GND_2" x="0" y="-2.54" direction="pwr"/>
<pin name="LOP" x="0" y="-5.08" direction="in"/>
<pin name="LOM" x="0" y="-7.62" direction="in"/>
<pin name="GND_3" x="0" y="-10.16" direction="pwr"/>
<pin name="CAPA" x="0" y="-12.7" direction="pas"/>
<pin name="CAPB" x="0" y="-15.24" direction="pas"/>
<pin name="GND_4" x="0" y="-17.78" direction="pwr"/>
<pin name="BBMQ" x="0" y="-20.32" direction="in"/>
<pin name="BBPQ" x="0" y="-22.86" direction="in"/>
<pin name="GND_5" x="0" y="-25.4" direction="pwr"/>
<pin name="GND_6" x="0" y="-27.94" direction="pwr"/>
<pin name="NC_2" x="60.96" y="-30.48" direction="nc" rot="R180"/>
<pin name="GNDRF_2" x="60.96" y="-27.94" direction="pwr" rot="R180"/>
<pin name="NC" x="60.96" y="-25.4" direction="nc" rot="R180"/>
<pin name="RF" x="60.96" y="-22.86" direction="out" rot="R180"/>
<pin name="GNDRF" x="60.96" y="-20.32" direction="pwr" rot="R180"/>
<pin name="VCC2" x="60.96" y="-17.78" direction="pwr" rot="R180"/>
<pin name="GND_7" x="60.96" y="-15.24" direction="pwr" rot="R180"/>
<pin name="GND_8" x="60.96" y="-12.7" direction="pwr" rot="R180"/>
<pin name="BBPI" x="60.96" y="-10.16" direction="in" rot="R180"/>
<pin name="BBMI" x="60.96" y="-7.62" direction="in" rot="R180"/>
<pin name="GND" x="60.96" y="-5.08" direction="pwr" rot="R180"/>
<pin name="VCC1" x="60.96" y="-2.54" direction="pwr" rot="R180"/>
<pin name="EPAD" x="60.96" y="0" direction="pas" rot="R180"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-35.56" x2="53.34" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-35.56" x2="53.34" y2="5.08" width="0.1524" layer="94"/>
<wire x1="53.34" y1="5.08" x2="7.62" y2="5.08" width="0.1524" layer="94"/>
<text x="25.7556" y="9.1186" size="2.0828" layer="95" ratio="6" rot="SR0">&gt;Name</text>
<text x="25.1206" y="6.5786" size="2.0828" layer="96" ratio="6" rot="SR0">&gt;Value</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="LT5506EUF" prefix="IC">
<gates>
<gate name="A" symbol="LT5506EUF_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-16_UF">
<connects>
<connect gate="A" pin="2XLO+" pad="11"/>
<connect gate="A" pin="2XLO-" pad="10"/>
<connect gate="A" pin="EN" pad="9"/>
<connect gate="A" pin="GND" pad="17"/>
<connect gate="A" pin="GND_2" pad="1"/>
<connect gate="A" pin="GND_3" pad="4"/>
<connect gate="A" pin="IF+" pad="2"/>
<connect gate="A" pin="IF-" pad="3"/>
<connect gate="A" pin="IFDET" pad="7"/>
<connect gate="A" pin="IOUT+" pad="16"/>
<connect gate="A" pin="IOUT-" pad="15"/>
<connect gate="A" pin="QOUT+" pad="14"/>
<connect gate="A" pin="QOUT-" pad="13"/>
<connect gate="A" pin="STBY" pad="12"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="VCC_2" pad="5"/>
<connect gate="A" pin="VCTRL" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="COPYRIGHT" value="Copyright (C) 2015 Accelerated Designs. All rights reserved" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="LT5506EUF" constant="no"/>
<attribute name="VENDOR" value="Linear Technology" constant="no"/>
</technology>
</technologies>
</device>
<device name="HAND" package="QFN-16_UF-H">
<connects>
<connect gate="A" pin="2XLO+" pad="11"/>
<connect gate="A" pin="2XLO-" pad="10"/>
<connect gate="A" pin="EN" pad="9"/>
<connect gate="A" pin="GND" pad="17"/>
<connect gate="A" pin="GND_2" pad="1"/>
<connect gate="A" pin="GND_3" pad="4"/>
<connect gate="A" pin="IF+" pad="2"/>
<connect gate="A" pin="IF-" pad="3"/>
<connect gate="A" pin="IFDET" pad="7"/>
<connect gate="A" pin="IOUT+" pad="16"/>
<connect gate="A" pin="IOUT-" pad="15"/>
<connect gate="A" pin="QOUT+" pad="14"/>
<connect gate="A" pin="QOUT-" pad="13"/>
<connect gate="A" pin="STBY" pad="12"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="VCC_2" pad="5"/>
<connect gate="A" pin="VCTRL" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC5598IUFTRPBF" prefix="IC">
<gates>
<gate name="A" symbol="LTC5598IUFTRPBF" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFN-24_UF">
<connects>
<connect gate="A" pin="BBMI" pad="22"/>
<connect gate="A" pin="BBMQ" pad="9"/>
<connect gate="A" pin="BBPI" pad="21"/>
<connect gate="A" pin="BBPQ" pad="10"/>
<connect gate="A" pin="CAPA" pad="6"/>
<connect gate="A" pin="CAPB" pad="7"/>
<connect gate="A" pin="EN" pad="1"/>
<connect gate="A" pin="EPAD" pad="25"/>
<connect gate="A" pin="GND" pad="23"/>
<connect gate="A" pin="GNDRF" pad="17"/>
<connect gate="A" pin="GNDRF_2" pad="14"/>
<connect gate="A" pin="GND_2" pad="2"/>
<connect gate="A" pin="GND_3" pad="5"/>
<connect gate="A" pin="GND_4" pad="8"/>
<connect gate="A" pin="GND_5" pad="11"/>
<connect gate="A" pin="GND_6" pad="12"/>
<connect gate="A" pin="GND_7" pad="19"/>
<connect gate="A" pin="GND_8" pad="20"/>
<connect gate="A" pin="LOM" pad="4"/>
<connect gate="A" pin="LOP" pad="3"/>
<connect gate="A" pin="NC" pad="15"/>
<connect gate="A" pin="NC_2" pad="13"/>
<connect gate="A" pin="RF" pad="16"/>
<connect gate="A" pin="VCC1" pad="24"/>
<connect gate="A" pin="VCC2" pad="18"/>
</connects>
<technologies>
<technology name="">
<attribute name="MANUFACTURER_PART_NUMBER" value="ltc5598iuf#trpbf" constant="no"/>
<attribute name="VENDOR" value="Linear Technology" constant="no"/>
</technology>
</technologies>
</device>
<device name="HAND" package="QFN-24_UF-H">
<connects>
<connect gate="A" pin="BBMI" pad="22"/>
<connect gate="A" pin="BBMQ" pad="9"/>
<connect gate="A" pin="BBPI" pad="21"/>
<connect gate="A" pin="BBPQ" pad="10"/>
<connect gate="A" pin="CAPA" pad="6"/>
<connect gate="A" pin="CAPB" pad="7"/>
<connect gate="A" pin="EN" pad="1"/>
<connect gate="A" pin="EPAD" pad="25"/>
<connect gate="A" pin="GND" pad="23"/>
<connect gate="A" pin="GNDRF" pad="17"/>
<connect gate="A" pin="GNDRF_2" pad="14"/>
<connect gate="A" pin="GND_2" pad="2"/>
<connect gate="A" pin="GND_3" pad="5"/>
<connect gate="A" pin="GND_4" pad="8"/>
<connect gate="A" pin="GND_5" pad="11"/>
<connect gate="A" pin="GND_6" pad="12"/>
<connect gate="A" pin="GND_7" pad="19"/>
<connect gate="A" pin="GND_8" pad="20"/>
<connect gate="A" pin="LOM" pad="4"/>
<connect gate="A" pin="LOP" pad="3"/>
<connect gate="A" pin="NC" pad="15"/>
<connect gate="A" pin="NC_2" pad="13"/>
<connect gate="A" pin="RF" pad="16"/>
<connect gate="A" pin="VCC1" pad="24"/>
<connect gate="A" pin="VCC2" pad="18"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="macom">
<packages>
<package name="ETC4-1-2TR">
<smd name="1" x="-1.27" y="1.72" dx="0.76" dy="1.5" layer="1"/>
<smd name="2" x="0" y="1.72" dx="0.76" dy="1.5" layer="1"/>
<smd name="3" x="1.27" y="1.72" dx="0.76" dy="1.5" layer="1"/>
<smd name="4" x="1.27" y="-1.72" dx="0.76" dy="1.5" layer="1"/>
<smd name="5" x="-1.27" y="-1.72" dx="0.76" dy="1.5" layer="1"/>
<wire x1="-1.9" y1="1.4" x2="1.9" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.4" x2="1.9" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.9" y1="-1.4" x2="-1.9" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.4" x2="-1.9" y2="1.4" width="0.127" layer="21"/>
<text x="-2.54" y="2.54" size="1.27" layer="27" font="vector" ratio="20">&gt;VALUE</text>
<text x="-2.54" y="3.81" size="1.27" layer="25" font="vector" ratio="20">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="TRANS-CT">
<wire x1="1.27" y1="2.032" x2="1.27" y2="1.27" width="0.1016" layer="94"/>
<wire x1="1.27" y1="0.508" x2="1.27" y2="-0.254" width="0.1016" layer="94"/>
<wire x1="1.27" y1="-1.016" x2="1.27" y2="-1.778" width="0.1016" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-3.356" width="0.1016" layer="94"/>
<wire x1="-0.254" y1="-5.08" x2="-0.254" y2="-3.3866" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.254" y1="-3.3866" x2="-0.254" y2="-1.6932" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.254" y1="-1.6934" x2="-0.254" y2="0" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="1.6934" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.254" y1="1.6934" x2="-0.254" y2="3.3866" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="-0.254" y1="3.3866" x2="-0.254" y2="5.08" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="1.27" y1="3.556" x2="1.27" y2="2.794" width="0.1016" layer="94"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="4.318" width="0.1016" layer="94"/>
<wire x1="2.794" y1="2.54" x2="2.794" y2="0.8466" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="2.794" y1="0.8466" x2="2.794" y2="-0.8466" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="2.794" y1="-0.8466" x2="2.794" y2="-2.54" width="0.254" layer="94" curve="180" cap="flat"/>
<wire x1="1.27" y1="-4.064" x2="1.27" y2="-4.88" width="0.1016" layer="94"/>
<text x="-2.54" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="2.286" y="-5.588" size="1.778" layer="96">&gt;VALUE</text>
<pin name="E1" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="A1" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="E2" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="A2" x="5.08" y="2.54" visible="pad" length="short" direction="pas" swaplevel="2" rot="R180"/>
<pin name="Z" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ETC4-1-2TR" prefix="T">
<gates>
<gate name="A" symbol="TRANS-CT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ETC4-1-2TR">
<connects>
<connect gate="A" pin="A1" pad="1"/>
<connect gate="A" pin="A2" pad="4"/>
<connect gate="A" pin="E1" pad="3"/>
<connect gate="A" pin="E2" pad="5"/>
<connect gate="A" pin="Z" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="IQD-Frequency-Products">
<description>&lt;b&gt;Crystals and Oscillators&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by IQD Frequency Products&lt;/author&gt;</description>
<packages>
<package name="7.5X5-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-3.15" y="-1.25" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="3.15" y="-1.25" dx="2.2" dy="1.4" layer="1"/>
<smd name="3" x="3.15" y="1.25" dx="2.2" dy="1.4" layer="1" rot="R180"/>
<smd name="4" x="-3.15" y="1.25" dx="2.2" dy="1.4" layer="1" rot="R180"/>
<text x="-4.0192" y="2.815" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.0192" y="-4.593" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-5.5" y1="2.5" x2="4.73" y2="2.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="2.5" x2="4.73" y2="-2.5" width="0.127" layer="21"/>
<wire x1="4.73" y1="-2.5" x2="-4.74" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-4.74" y1="-2.5" x2="-4.74" y2="2.46" width="0.127" layer="21"/>
<wire x1="-5.5" y1="2.5" x2="-5.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-5.5" y1="-2.5" x2="-4.74" y2="-2.5" width="0.127" layer="21"/>
<circle x="-5.05" y="-2.87" radius="0.2" width="0" layer="21"/>
</package>
<package name="5.2X3.4-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-4.2" y1="-2.3" x2="3.368" y2="-2.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="2.276" x2="-3.3" y2="-2.176" width="0.2032" layer="21"/>
<wire x1="3.368" y1="2.3" x2="-4.2" y2="2.3" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-2.276" x2="3.4" y2="2.276" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="-1.1" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="2" x="2.1" y="-1.1" dx="1.4" dy="1.6" layer="1" rot="R90"/>
<smd name="3" x="2.1" y="1.1" dx="1.4" dy="1.6" layer="1" rot="R270"/>
<smd name="4" x="-2.1" y="1.1" dx="1.4" dy="1.6" layer="1" rot="R270"/>
<text x="3.04" y="4.39" size="1.27" layer="25" rot="R180">&gt;NAME</text>
<text x="-3.686" y="-4.608" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-4.2" y1="2.3" x2="-4.2" y2="-2.3" width="0.2032" layer="21"/>
<circle x="-3.7" y="-2.8" radius="0.2" width="0" layer="21"/>
</package>
<package name="3.4X2.7-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<wire x1="-3.1" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="21"/>
<wire x1="2.4" y1="-1.9" x2="-3.1" y2="-1.9" width="0.2032" layer="21"/>
<smd name="1" x="-1.1" y="-0.8" dx="1.4" dy="1.15" layer="1"/>
<smd name="2" x="1.1" y="-0.8" dx="1.4" dy="1.15" layer="1"/>
<smd name="3" x="1.1" y="0.8" dx="1.4" dy="1.15" layer="1" rot="R180"/>
<smd name="4" x="-1.1" y="0.8" dx="1.4" dy="1.15" layer="1" rot="R180"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.91" width="0.2" layer="21"/>
<wire x1="-2.42" y1="1.9" x2="-2.42" y2="-1.91" width="0.2" layer="21"/>
<wire x1="-3.1" y1="1.9" x2="-3.1" y2="-1.9" width="0.2032" layer="21"/>
<circle x="-2.7" y="-2.3" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.5X2-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.85" y="-0.65" dx="1.2" dy="1" layer="1"/>
<smd name="2" x="0.85" y="-0.65" dx="1.2" dy="1" layer="1"/>
<smd name="3" x="0.85" y="0.65" dx="1.2" dy="1" layer="1" rot="R180"/>
<smd name="4" x="-0.85" y="0.65" dx="1.2" dy="1" layer="1" rot="R180"/>
<text x="-3.27" y="2.07" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.47" y="-3.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.1" y1="-1.5" x2="2" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2" y1="-1.5" x2="2" y2="1.5" width="0.127" layer="21"/>
<wire x1="2" y1="1.5" x2="-2.1" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.5" x2="-2.1" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.5" x2="-2.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="-2.6" y1="1.5" x2="-2.6" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.6" y1="-1.5" x2="-2.1" y2="-1.5" width="0.127" layer="21"/>
<circle x="-2.3" y="-1.8" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.6X2.1-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.8" y="-0.6" dx="0.9" dy="0.8" layer="1"/>
<smd name="2" x="0.8" y="-0.6" dx="0.9" dy="0.8" layer="1"/>
<smd name="3" x="0.8" y="0.6" dx="0.9" dy="0.8" layer="1" rot="R180"/>
<smd name="4" x="-0.8" y="0.6" dx="0.9" dy="0.8" layer="1" rot="R180"/>
<text x="-3.24" y="2.04" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.54" y="-3.41" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.6" y1="1.4" x2="1.7" y2="1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="1.4" x2="1.7" y2="-1.4" width="0.127" layer="21"/>
<wire x1="1.7" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-1.4" x2="-1.6" y2="1.4" width="0.127" layer="21"/>
<wire x1="-1.6" y1="1.4" x2="-2" y2="1.4" width="0.127" layer="21"/>
<wire x1="-2" y1="1.4" x2="-2" y2="-1.4" width="0.127" layer="21"/>
<wire x1="-2" y1="-1.4" x2="-1.6" y2="-1.4" width="0.127" layer="21"/>
<circle x="-1.8" y="-1.7" radius="0.2" width="0" layer="21"/>
</package>
<package name="2.0X1.6-4-PAD">
<description>&lt;b&gt;IQD Frequency Products SMD Package&lt;/b&gt;</description>
<smd name="1" x="-0.637" y="-0.487" dx="0.875" dy="0.775" layer="1"/>
<smd name="2" x="0.637" y="-0.487" dx="0.875" dy="0.775" layer="1"/>
<smd name="3" x="0.637" y="0.487" dx="0.875" dy="0.775" layer="1" rot="R180"/>
<smd name="4" x="-0.637" y="0.487" dx="0.875" dy="0.775" layer="1" rot="R180"/>
<text x="-2.27" y="1.77" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.67" y="-3.34" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.4" y1="1.3" x2="1.5" y2="1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.3" x2="1.5" y2="-1.3" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.3" x2="-1.4" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="-1.3" x2="-1.4" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.4" y1="1.3" x2="-1.9" y2="1.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.3" x2="-1.9" y2="-1.3" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.3" x2="-1.4" y2="-1.3" width="0.127" layer="21"/>
<circle x="-1.6" y="-1.6" radius="0.2" width="0" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="Q-SHIELD2">
<wire x1="2.286" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0.254" y2="0" width="0.1524" layer="94"/>
<wire x1="0.889" y1="1.524" x2="0.889" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.889" y1="-1.524" x2="1.651" y2="-1.524" width="0.254" layer="94"/>
<wire x1="1.651" y1="-1.524" x2="1.651" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.651" y1="1.524" x2="0.889" y2="1.524" width="0.254" layer="94"/>
<wire x1="2.286" y1="1.778" x2="2.286" y2="0" width="0.254" layer="94"/>
<wire x1="2.286" y1="0" x2="2.286" y2="-1.778" width="0.254" layer="94"/>
<wire x1="0.254" y1="1.778" x2="0.254" y2="0" width="0.254" layer="94"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.905" x2="-1.778" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="2.54" x2="4.318" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="4.318" y1="2.54" x2="4.318" y2="1.905" width="0.1524" layer="94" style="shortdash"/>
<wire x1="4.318" y1="-1.905" x2="4.318" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="-2.54" x2="4.318" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-1.778" y1="-2.54" x2="-1.778" y2="-1.905" width="0.1524" layer="94" style="shortdash"/>
<text x="-2.54" y="6.096" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="3" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="pad" length="short" direction="pas" swaplevel="1"/>
<pin name="4" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTALS-GND-LID" prefix="X">
<description>&lt;b&gt;CRYSTALS WITH CAN GROUNDED&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q-SHIELD2" x="0" y="0"/>
</gates>
<devices>
<device name="-4-PAD" package="7.5X5-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name="12SMX-B"/>
</technologies>
</device>
<device name="-CFPX-104" package="5.2X3.4-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPX-180" package="3.4X2.7-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPX-181" package="2.5X2-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-CFPX-218" package="2.6X2.1-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-IQXC-42" package="2.0X1.6-4-PAD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Supply_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Supply symbol library&lt;br&gt;
$Rev: 25542 $ 
&lt;p&gt;
since 2008&lt;br&gt;
by&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+5V">
<pin name="+5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="+3V3">
<pin name="+3V3" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="VCC_1">
<pin name="VCC_1" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="VCC_2">
<pin name="VCC_2" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="1.27" y1="0.635" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="0.635" width="0.254" layer="94"/>
<text x="-2.54" y="0" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V">
<description>5V</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3">
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_1" prefix="P+">
<gates>
<gate name="G$1" symbol="VCC_1" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC_2" prefix="P+">
<gates>
<gate name="G$1" symbol="VCC_2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Passive_Robotech">
<description>&lt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by SamacSys&lt;/author&gt;</description>
<packages>
<package name="1005">
<description>Metric Code Size 1005</description>
<smd name="1" x="-0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<smd name="2" x="0.473" y="0" dx="0.8128" dy="0.4064" layer="1" rot="R90"/>
<text x="-0.5" y="0.425" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.508" y1="0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="0.508" y1="0.254" x2="-0.508" y2="0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="0.254" x2="-0.508" y2="-0.254" width="0.127" layer="51"/>
<wire x1="-0.508" y1="-0.254" x2="0.508" y2="-0.254" width="0.127" layer="51"/>
</package>
<package name="1608">
<description>Metric Code Size 1608</description>
<smd name="1" x="-0.875" y="0" dx="1.016" dy="0.762" layer="1" rot="R90"/>
<smd name="2" x="0.875" y="0" dx="0.762" dy="1.016" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.762" y1="0.381" x2="0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="0.762" y1="-0.381" x2="-0.762" y2="-0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="-0.381" x2="-0.762" y2="0.381" width="0.127" layer="51"/>
<wire x1="-0.762" y1="0.381" x2="0.762" y2="0.381" width="0.127" layer="51"/>
</package>
<package name="3216">
<description>Metric Code Size 3216</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="0.762" x2="-1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-0.762" x2="1.524" y2="-0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="-0.762" x2="1.524" y2="0.762" width="0.127" layer="51"/>
<wire x1="1.524" y1="0.762" x2="-1.524" y2="0.762" width="0.127" layer="51"/>
</package>
<package name="3225">
<description>Metric Code Size 3225</description>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-1.524" y1="1.27" x2="1.524" y2="1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="1.27" x2="1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="1.524" y1="-1.27" x2="-1.524" y2="-1.27" width="0.127" layer="51"/>
<wire x1="-1.524" y1="-1.27" x2="-1.524" y2="1.27" width="0.127" layer="51"/>
</package>
<package name="4532">
<description>Metric Code Size 4532</description>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.159" y1="1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="1.524" x2="2.159" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="5650">
<description>Metric Code Size 5650</description>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.794" y1="2.413" x2="2.794" y2="2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="2.413" x2="2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="2.794" y1="-2.413" x2="-2.794" y2="-2.413" width="0.127" layer="51"/>
<wire x1="-2.794" y1="-2.413" x2="-2.794" y2="2.413" width="0.127" layer="51"/>
</package>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
</package>
<package name="2012-C">
<smd name="1" x="-0.9207" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="0.9206" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1.381" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1.381" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="0.889" y1="0.635" x2="-0.889" y2="0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="0.635" x2="-0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-0.889" y1="-0.635" x2="0.889" y2="-0.635" width="0.127" layer="51"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" font="vector" ratio="12">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" font="vector" ratio="12">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
</package>
<package name="2012-R">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.524" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.524" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="250-80">
<wire x1="12.5" y1="4" x2="-12.5" y2="4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="4" x2="-12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="-12.5" y1="-4" x2="12.5" y2="-4" width="0.127" layer="21"/>
<wire x1="12.5" y1="-4" x2="12.5" y2="4" width="0.127" layer="21"/>
<pad name="P$1" x="-15.24" y="0" drill="1.2" shape="octagon"/>
<pad name="P$2" x="15.24" y="0" drill="1.2" shape="octagon"/>
<rectangle x1="-15.24" y1="-0.508" x2="-12.573" y2="0.508" layer="51"/>
<rectangle x1="12.573" y1="-0.508" x2="15.24" y2="0.508" layer="51"/>
</package>
<package name="6032">
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="-2.909" y1="1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="1.524" x2="2.909" y2="-1.524" width="0.127" layer="51"/>
<wire x1="-2.909" y1="1.524" x2="2.909" y2="1.524" width="0.127" layer="51"/>
<wire x1="2.909" y1="-1.524" x2="-2.909" y2="-1.524" width="0.127" layer="51"/>
</package>
<package name="CEMENTR/48">
<description>Cement Resistor
Width=48mm, Depth=10mm, Height=10mm</description>
<pad name="1" x="-29.21" y="0" drill="1.5" shape="octagon"/>
<pad name="2" x="29.21" y="0" drill="1.5" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="29.21" y1="0" x2="28.194" y2="0" width="0.6096" layer="51"/>
<wire x1="-29.21" y1="0" x2="-28.194" y2="0" width="0.6096" layer="51"/>
<rectangle x1="27.305" y1="-0.3048" x2="28.1686" y2="0.3048" layer="21"/>
<rectangle x1="-28.1686" y1="-0.3048" x2="-27.305" y2="0.3048" layer="21"/>
<wire x1="27.305" y1="0" x2="24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-27.305" y1="0" x2="-24.384" y2="0" width="0.6096" layer="21"/>
<wire x1="-24" y1="5" x2="24" y2="5" width="0.127" layer="21"/>
<wire x1="24" y1="5" x2="24" y2="-5" width="0.127" layer="21"/>
<wire x1="24" y1="-5" x2="-24" y2="-5" width="0.127" layer="21"/>
<wire x1="-24" y1="-5" x2="-24" y2="5" width="0.127" layer="21"/>
</package>
<package name="PEC12R-4XXXF-SXXXX_1">
<description>&lt;b&gt;PEC12R-4XXXF-SXXXX&lt;/b&gt;&lt;br&gt;</description>
<pad name="1" x="-2.5" y="-7.5" drill="1.2" diameter="1.8"/>
<pad name="2" x="2.5" y="-7.5" drill="1.2" diameter="1.8"/>
<pad name="3" x="0" y="-7.5" drill="1.2" diameter="1.8"/>
<pad name="4" x="-2.5" y="7" drill="1.2" diameter="1.8"/>
<pad name="5" x="2.5" y="7" drill="1.2" diameter="1.8"/>
<pad name="6" x="5.6" y="0" drill="3.28" diameter="4.8"/>
<pad name="7" x="-5.6" y="0" drill="3.28" diameter="4.8"/>
<text x="-0.357" y="-1.016" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.357" y="-1.016" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-6.2" y1="6.7" x2="6.2" y2="6.7" width="0.254" layer="51"/>
<wire x1="6.2" y1="6.7" x2="6.2" y2="-6.7" width="0.254" layer="51"/>
<wire x1="6.2" y1="-6.7" x2="-6.2" y2="-6.7" width="0.254" layer="51"/>
<wire x1="-6.2" y1="-6.7" x2="-6.2" y2="6.7" width="0.254" layer="51"/>
<wire x1="-6.2" y1="6.7" x2="-6.2" y2="2.6" width="0.254" layer="21"/>
<wire x1="-6.2" y1="-6.7" x2="-6.2" y2="-2.6" width="0.254" layer="21"/>
<wire x1="6.2" y1="6.7" x2="6.2" y2="2.6" width="0.254" layer="21"/>
<wire x1="6.2" y1="-6.7" x2="6.2" y2="-2.6" width="0.254" layer="21"/>
<circle x="-2.533" y="-8.863" radius="0.0821" width="0.254" layer="25"/>
</package>
<package name="B3FS4052P">
<description>&lt;b&gt;B3FS-4052P&lt;/b&gt;&lt;br&gt;</description>
<smd name="1" x="6.15" y="-2.45" dx="2.4" dy="2.3" layer="1"/>
<smd name="2" x="-8.25" y="-2.45" dx="2.4" dy="2.3" layer="1"/>
<smd name="3" x="6.15" y="2.45" dx="2.4" dy="2.3" layer="1"/>
<smd name="4" x="-8.25" y="2.45" dx="2.4" dy="2.3" layer="1"/>
<text x="-0.6" y="0" size="1.27" layer="25" align="center">&gt;NAME</text>
<text x="-0.6" y="0" size="1.27" layer="27" align="center">&gt;VALUE</text>
<wire x1="-7.05" y1="6" x2="4.95" y2="6" width="0.2" layer="51"/>
<wire x1="4.95" y1="6" x2="4.95" y2="-6" width="0.2" layer="51"/>
<wire x1="4.95" y1="-6" x2="-7.05" y2="-6" width="0.2" layer="51"/>
<wire x1="-7.05" y1="-6" x2="-7.05" y2="6" width="0.2" layer="51"/>
<wire x1="-10.45" y1="7" x2="9.25" y2="7" width="0.1" layer="51"/>
<wire x1="9.25" y1="7" x2="9.25" y2="-7" width="0.1" layer="51"/>
<wire x1="9.25" y1="-7" x2="-10.45" y2="-7" width="0.1" layer="51"/>
<wire x1="-10.45" y1="-7" x2="-10.45" y2="7" width="0.1" layer="51"/>
<wire x1="-7.037" y1="-4" x2="-7.05" y2="-6" width="0.1" layer="21"/>
<wire x1="-7.05" y1="-6" x2="4.969" y2="-6" width="0.1" layer="21"/>
<wire x1="4.969" y1="-6" x2="4.969" y2="-4" width="0.1" layer="21"/>
<wire x1="-7.037" y1="4" x2="-7.05" y2="6" width="0.1" layer="21"/>
<wire x1="-7.05" y1="6" x2="4.95" y2="6" width="0.1" layer="21"/>
<wire x1="4.95" y1="6" x2="4.969" y2="4" width="0.1" layer="21"/>
</package>
<package name="L5-7,5">
<pad name="P$1" x="2.5" y="0" drill="0.8"/>
<pad name="P$2" x="-2.5" y="0" drill="0.8"/>
<circle x="0" y="0" radius="4" width="0.127" layer="21"/>
<wire x1="-1.5" y1="0" x2="-0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="-0.5" y1="0" x2="0.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<wire x1="0.5" y1="0" x2="1.5" y2="0" width="0.0634" layer="21" curve="-180"/>
<text x="-3" y="-2.5" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-3.5" y="-4" size="1.27" layer="27" font="vector">&gt;VALUE</text>
</package>
<package name="L22">
<pad name="P$1" x="11.43" y="0" drill="2.8" diameter="6.4516"/>
<pad name="P$2" x="-11.43" y="0" drill="2.8" diameter="6.4516"/>
<circle x="0" y="0" radius="11.359225" width="0.127" layer="21"/>
<text x="-3.81" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.81" y="-1.27" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="T50H">
<circle x="0" y="0" radius="2.83980625" width="0.127" layer="21"/>
<circle x="0" y="0" radius="6.35" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="-6.35" drill="1"/>
<pad name="P$2" x="2.54" y="-6.35" drill="1"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T50V">
<wire x1="-2.54" y1="6.35" x2="2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="2.54" y1="-6.35" x2="-2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-6.35" x2="-2.54" y2="-5.08" width="0.127" layer="21"/>
<pad name="P$1" x="-2.54" y="0" drill="1"/>
<pad name="P$2" x="2.54" y="0" drill="1"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="5.08" x2="2.54" y2="3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="3.81" x2="2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="2.54" y2="0" width="0.127" layer="21"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-3.81" x2="2.54" y2="-5.08" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-5.08" x2="2.54" y2="-6.35" width="0.127" layer="21"/>
<wire x1="-2.54" y1="6.35" x2="2.54" y2="5.08" width="0.127" layer="21"/>
<text x="-2.54" y="8.89" size="1.27" layer="21">&gt;NAME</text>
<text x="-2.54" y="7.62" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="T20V">
<pad name="P$1" x="-1.27" y="0" drill="0.8"/>
<pad name="P$2" x="1.27" y="0" drill="0.8"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<text x="-1.905" y="4.445" size="1.27" layer="21">&gt;NAME</text>
<text x="-1.905" y="3.175" size="1.27" layer="21">&gt;VALUE</text>
</package>
<package name="2125">
<description>Metric Code Size 2012</description>
<smd name="1" x="-1.0477" y="0" dx="1.016" dy="1.8" layer="1"/>
<smd name="2" x="1.0476" y="0" dx="1.016" dy="1.8" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27" font="vector">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="-1.016" y2="0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="0.635" x2="-1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="-1.016" y1="-0.635" x2="1.016" y2="-0.635" width="0.127" layer="51"/>
<wire x1="1.016" y1="-0.635" x2="1.016" y2="0.635" width="0.127" layer="51"/>
</package>
<package name="SOLENOID-D5-L2.5">
<pad name="P$1" x="-1.27" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="1.27" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.127" layer="27"/>
<wire x1="-0.635" y1="0" x2="0" y2="2.54" width="0.127" layer="27"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="27"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="0" width="0.127" layer="27"/>
</package>
<package name="SOLENOID-D5-L5">
<pad name="P$1" x="-2.54" y="0" drill="0.8" shape="octagon"/>
<pad name="P$2" x="2.54" y="0" drill="0.8" shape="octagon"/>
<text x="-2.54" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.127" layer="21"/>
<wire x1="-1.905" y1="0" x2="-1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="-0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="2.54" width="0.127" layer="21"/>
<wire x1="0" y1="2.54" x2="0" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="-2.54" width="0.127" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.27" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="2.54" x2="1.27" y2="-2.54" width="0.127" layer="21"/>
<wire x1="1.27" y1="-2.54" x2="1.905" y2="2.54" width="0.127" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.905" y2="0" width="0.127" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.8782" cap="flat"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.3729" cap="flat"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="R">
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
</symbol>
<symbol name="PEC12R-4215F-S0024">
<wire x1="5.08" y1="2.54" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="15.24" y2="2.54" width="0.254" layer="94"/>
<wire x1="15.24" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="16.51" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="16.51" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="A" x="0" y="0" length="middle"/>
<pin name="B" x="0" y="-2.54" length="middle"/>
<pin name="C" x="0" y="-5.08" length="middle"/>
<pin name="1" x="20.32" y="0" length="middle" rot="R180"/>
<pin name="2" x="20.32" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="B3FS-4052P">
<wire x1="5.08" y1="2.54" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="25.4" y2="2.54" width="0.254" layer="94"/>
<wire x1="25.4" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="26.67" y="7.62" size="1.778" layer="95" align="center-left">&gt;NAME</text>
<text x="26.67" y="5.08" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="COM_1" x="0" y="0" length="middle"/>
<pin name="COM_2" x="0" y="-2.54" length="middle"/>
<pin name="NO_1" x="30.48" y="0" length="middle" rot="R180"/>
<pin name="NO_2" x="30.48" y="-2.54" length="middle" rot="R180"/>
</symbol>
<symbol name="L">
<text x="-1.27" y="-5.08" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="3.81" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<wire x1="0" y1="-2.54" x2="0.635" y2="-1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-1.905" x2="0" y2="-1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="-1.27" x2="0.635" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="-0.635" x2="0" y2="0" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="0" x2="0.635" y2="0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.27" width="0.254" layer="94" curve="90"/>
<wire x1="0" y1="1.27" x2="0.635" y2="1.905" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94" curve="90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7.5" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-C">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-6032" package="6032">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/48" package="CEMENTR/48">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PEC12R-4215F-S0024" prefix="U">
<description>&lt;b&gt;Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/PEC12R-4215F-S0024.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="PEC12R-4215F-S0024" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PEC12R-4XXXF-SXXXX_1">
<connects>
<connect gate="G$1" pin="1" pad="4"/>
<connect gate="G$1" pin="2" pad="5"/>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="C" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Encoders HORZ 24DET 24PULSE 15mm SHAFT SPST SW" constant="no"/>
<attribute name="HEIGHT" value="mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Bourns" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="PEC12R-4215F-S0024" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="652-PEC12R-4215F-S24" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="B3FS-4052P" prefix="S">
<description>&lt;b&gt;Tactile Switches 12x12 SurfMt tactile swtch project plungr&lt;/b&gt;&lt;p&gt;
Source: &lt;a href="https://componentsearchengine.com/Datasheets/1/B3FS-4052P.pdf"&gt; Datasheet &lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="B3FS-4052P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="B3FS4052P">
<connects>
<connect gate="G$1" pin="COM_1" pad="1"/>
<connect gate="G$1" pin="COM_2" pad="2"/>
<connect gate="G$1" pin="NO_1" pad="3"/>
<connect gate="G$1" pin="NO_2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="Tactile Switches 12x12 SurfMt tactile swtch project plungr" constant="no"/>
<attribute name="HEIGHT" value="7mm" constant="no"/>
<attribute name="MANUFACTURER_NAME" value="Omron Electronics" constant="no"/>
<attribute name="MANUFACTURER_PART_NUMBER" value="B3FS-4052P" constant="no"/>
<attribute name="MOUSER_PART_NUMBER" value="653-B3FS-4052P" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/2.5" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1005" package="1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1608" package="1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2012" package="2012-R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3216" package="3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-3225" package="3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-4532" package="4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-5650" package="5650">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="250-80">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/5(VERTICAL)" package="L5-7,5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="22(VERTICAL)" package="L22">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50H" package="T50H">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T50V" package="T50V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="T20V" package="T20V">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-2125" package="2125">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L2.5" package="SOLENOID-D5-L2.5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D5-L5" package="SOLENOID-D5-L5">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
<connect gate="G$1" pin="2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Connector_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Connector Library &lt;br&gt;
$Rev: 25542 $
&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
Hideo Tanida&lt;br&gt;
Makoto Shimazu&lt;br&gt;
Mayu Kojima&lt;br&gt;
Takefumi Hiraki&lt;br&gt;
Soichiro Iwataki&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="IL-G-04-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-5.0531" y="3.7751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="4.5481" y="-1.9319" size="1.27" layer="21" ratio="14" rot="R90">1</text>
<text x="-2.8687" y="-1.9319" size="1.27" layer="21" ratio="14" rot="R90">4</text>
<pad name="4" x="-3.81" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.9" shape="octagon" rot="R90"/>
<pad name="1" x="3.81" y="0" drill="0.9" shape="octagon" rot="R90"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<text x="5.0531" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<wire x1="5.83" y1="3.421" x2="5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="5.81" y1="3.421" x2="5.556" y2="3.421" width="0.254" layer="21"/>
<wire x1="5.556" y1="3.421" x2="-5.556" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.556" y1="3.421" x2="-5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.81" y1="3.421" x2="-5.83" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.83" y1="3.421" x2="-5.83" y2="-2.921" width="0.254" layer="21"/>
<wire x1="5.83" y1="3.421" x2="5.83" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-5.83" y1="-2.921" x2="-4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-4.445" y1="-2.921" x2="-4.445" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-4.445" y1="-2.286" x2="-3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.286" x2="-3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.921" x2="-4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.921" x2="4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.921" x2="3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.286" x2="4.445" y2="-2.286" width="0.254" layer="21"/>
<wire x1="4.445" y1="-2.286" x2="4.445" y2="-2.921" width="0.254" layer="21"/>
<wire x1="4.445" y1="-2.921" x2="5.83" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-5.81" y1="2.405" x2="-5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.81" y1="2.405" x2="-5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="-5.556" y1="2.405" x2="5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="5.556" y1="2.405" x2="5.81" y2="2.405" width="0.254" layer="21"/>
<wire x1="5.81" y1="2.405" x2="5.81" y2="3.421" width="0.254" layer="21"/>
<wire x1="-5.81" y1="2.405" x2="-5.556" y2="1.897" width="0.254" layer="21"/>
<wire x1="-5.556" y1="3.421" x2="-5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="-5.556" y1="1.897" x2="5.556" y2="1.897" width="0.254" layer="21"/>
<wire x1="5.556" y1="1.897" x2="5.81" y2="2.405" width="0.254" layer="21"/>
<wire x1="5.556" y1="3.421" x2="5.556" y2="2.405" width="0.254" layer="21"/>
<wire x1="-5.81" y1="3.04" x2="5.81" y2="3.04" width="0.3" layer="41"/>
</package>
<package name="5251-02-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="2.1321" y="-3.6561" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="1.27" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<text x="-2.8941" y="-2.8941" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-1.27" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51" rot="R180"/>
<wire x1="1.27" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="-1.27" y1="9.906" x2="-1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="9.906" width="0.254" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-1.016" y1="10.414" x2="1.016" y2="10.414" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-3.175" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-3.175" width="0.254" layer="21"/>
<wire x1="2.54" y1="-3.175" x2="-2.54" y2="-3.175" width="0.254" layer="21"/>
<rectangle x1="-1.524" y1="0.254" x2="-1.016" y2="8.382" layer="51"/>
<rectangle x1="1.016" y1="0.254" x2="1.524" y2="8.382" layer="51"/>
<wire x1="-2.54" y1="-0.762" x2="2.54" y2="-0.762" width="0.254" layer="48"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
<text x="2.286" y="-2.794" size="1.27" layer="51" rot="R90">1</text>
<text x="-1.016" y="-2.794" size="1.27" layer="51" rot="R90">2</text>
</package>
<package name="5251-02-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-2.5131" y="3.2751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="2.032" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-0.762" y="-2.032" size="1.27" layer="51" ratio="14" rot="R90">2</text>
<pad name="2" x="-1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="1" x="1.27" y="0" drill="1" shape="octagon" rot="R90"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<text x="2.5131" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="-2.54" y1="2.921" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.921" x2="-1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="1.016" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="1.27" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-2.921" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="2.54" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.905" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.905" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.27" y1="1.905" x2="1.27" y2="2.921" width="0.254" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="-1.016" y1="2.921" x2="-1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.016" y1="1.397" x2="1.016" y2="1.397" width="0.254" layer="21"/>
<wire x1="1.016" y1="1.397" x2="1.27" y2="1.905" width="0.254" layer="21"/>
<wire x1="1.016" y1="2.921" x2="1.016" y2="1.905" width="0.254" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.3" layer="41"/>
</package>
<package name="D3200S-2-TAB-HORIZONTAL">
<pad name="1" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.1" shape="long" rot="R270"/>
<text x="-5.715" y="14.605" size="3.81" layer="21" rot="SR0">AMP</text>
<text x="-2.8575" y="12.7" size="1.27" layer="21" rot="SR0">D-3200</text>
<text x="-8.89" y="24.13" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="-0.6" x2="9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="9.55" y1="-0.6" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="9.55" y1="0" x2="9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="9.55" y1="21.5" x2="-9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="-9.55" y1="21.5" x2="-9.55" y2="0" width="0.127" layer="27"/>
<text x="-8.89" y="22.225" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="0" x2="-9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="-9.55" y1="0" x2="9.55" y2="0" width="0.127" layer="27"/>
<hole x="-6.35" y="7.62" drill="3.2"/>
<hole x="6.35" y="7.62" drill="3.2"/>
<wire x1="-8.255" y1="6.858" x2="-7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="-7.112" y1="5.715" x2="-5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="-5.588" y1="5.715" x2="-4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-4.445" y1="6.858" x2="-4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="-4.445" y1="8.382" x2="-5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="-5.588" y1="9.525" x2="-7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="-7.112" y1="9.525" x2="-8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="-8.255" y1="8.382" x2="-8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="4.445" y1="6.858" x2="4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="4.445" y1="8.382" x2="5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="5.588" y1="9.525" x2="7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="7.112" y1="9.525" x2="8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="8.255" y1="8.382" x2="8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="8.255" y1="6.858" x2="7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="7.112" y1="5.715" x2="5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="5.588" y1="5.715" x2="4.445" y2="6.858" width="0.508" layer="42"/>
</package>
<package name="D3200S-2-TAB-VERTICAL">
<pad name="2" x="-2.54" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="2.54" y="0" drill="1" shape="long" rot="R270"/>
<text x="-9.525" y="6.985" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="4.7" x2="9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="4.7" x2="9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="-4.7" x2="-9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="-9.55" y1="-4.7" x2="-9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.68" x2="8.5" y2="3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="3.68" x2="8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="-3.68" x2="-8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.68" x2="-8.5" y2="3.68" width="0.127" layer="21"/>
<text x="-9.525" y="5.08" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.525" y1="0.555" x2="-8.525" y2="0.555" width="0.127" layer="21"/>
<wire x1="-8.525" y1="-1.445" x2="-9.525" y2="-1.445" width="0.127" layer="21"/>
<hole x="-5.715" y="-1.27" drill="2"/>
<wire x1="-6.985" y1="-1.778" x2="-6.985" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-6.985" y1="-0.762" x2="-6.223" y2="0" width="0.508" layer="42"/>
<wire x1="-6.223" y1="0" x2="-5.207" y2="0" width="0.508" layer="42"/>
<wire x1="-5.207" y1="0" x2="-4.445" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-4.445" y1="-0.762" x2="-4.445" y2="-1.778" width="0.508" layer="42"/>
<wire x1="-4.445" y1="-1.778" x2="-5.207" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-5.207" y1="-2.54" x2="-6.223" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-6.223" y1="-2.54" x2="-6.985" y2="-1.778" width="0.508" layer="42"/>
<hole x="5.715" y="-1.27" drill="2"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="-0.762" width="0.508" layer="42"/>
<wire x1="4.445" y1="-0.762" x2="5.207" y2="0" width="0.508" layer="42"/>
<wire x1="5.207" y1="0" x2="6.223" y2="0" width="0.508" layer="42"/>
<wire x1="6.223" y1="0" x2="6.985" y2="-0.762" width="0.508" layer="42"/>
<wire x1="6.985" y1="-0.762" x2="6.985" y2="-1.778" width="0.508" layer="42"/>
<wire x1="6.985" y1="-1.778" x2="6.223" y2="-2.54" width="0.508" layer="42"/>
<wire x1="6.223" y1="-2.54" x2="5.207" y2="-2.54" width="0.508" layer="42"/>
<wire x1="5.207" y1="-2.54" x2="4.445" y2="-1.778" width="0.508" layer="42"/>
</package>
<package name="D3200S-2-TAB-VERTICAL-AND-HORIZONTAL">
<pad name="1" x="2.54" y="0" drill="1.1" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="0" drill="1.1" shape="long" rot="R270"/>
<text x="-5.715" y="14.605" size="3.81" layer="21" rot="SR0">AMP</text>
<text x="-2.8575" y="12.7" size="1.27" layer="21" rot="SR0">D-3200</text>
<text x="-8.89" y="24.13" size="1.27" layer="25">&gt;NAME</text>
<wire x1="-9.55" y1="-0.6" x2="9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="9.55" y1="-0.6" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="9.55" y1="0" x2="9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="9.55" y1="21.5" x2="-9.55" y2="21.5" width="0.127" layer="27"/>
<wire x1="-9.55" y1="21.5" x2="-9.55" y2="0" width="0.127" layer="27"/>
<text x="-8.89" y="22.225" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-9.55" y1="0" x2="-9.55" y2="-0.6" width="0.127" layer="27"/>
<wire x1="-9.55" y1="0" x2="9.55" y2="0" width="0.127" layer="27"/>
<wire x1="-9.55" y1="-4.7" x2="9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="-4.7" x2="9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="9.55" y1="4.7" x2="-9.55" y2="4.7" width="0.127" layer="21"/>
<wire x1="-9.55" y1="4.7" x2="-9.55" y2="-4.7" width="0.127" layer="21"/>
<wire x1="8.5" y1="3.68" x2="8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="8.5" y1="-3.68" x2="-8.5" y2="-3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="-3.68" x2="-8.5" y2="3.68" width="0.127" layer="21"/>
<wire x1="-8.5" y1="3.68" x2="8.5" y2="3.68" width="0.127" layer="21"/>
<hole x="-6.35" y="7.62" drill="3.2"/>
<hole x="6.35" y="7.62" drill="3.2"/>
<hole x="-6.35" y="-1.27" drill="2"/>
<hole x="6.35" y="-1.27" drill="2"/>
<wire x1="-8.255" y1="6.858" x2="-8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="-8.255" y1="8.382" x2="-7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="-7.112" y1="9.525" x2="-5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="-5.588" y1="9.525" x2="-4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="-4.445" y1="8.382" x2="-4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-4.445" y1="6.858" x2="-5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="-5.588" y1="5.715" x2="-7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="-7.112" y1="5.715" x2="-8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="4.445" y1="6.858" x2="4.445" y2="8.382" width="0.508" layer="42"/>
<wire x1="4.445" y1="8.382" x2="5.588" y2="9.525" width="0.508" layer="42"/>
<wire x1="5.588" y1="9.525" x2="7.112" y2="9.525" width="0.508" layer="42"/>
<wire x1="7.112" y1="9.525" x2="8.255" y2="8.382" width="0.508" layer="42"/>
<wire x1="8.255" y1="8.382" x2="8.255" y2="6.858" width="0.508" layer="42"/>
<wire x1="8.255" y1="6.858" x2="7.112" y2="5.715" width="0.508" layer="42"/>
<wire x1="7.112" y1="5.715" x2="5.588" y2="5.715" width="0.508" layer="42"/>
<wire x1="5.588" y1="5.715" x2="4.445" y2="6.858" width="0.508" layer="42"/>
<wire x1="-7.62" y1="-1.778" x2="-7.62" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-7.62" y1="-0.762" x2="-6.858" y2="0" width="0.508" layer="42"/>
<wire x1="-6.858" y1="0" x2="-5.842" y2="0" width="0.508" layer="42"/>
<wire x1="-5.842" y1="0" x2="-5.08" y2="-0.762" width="0.508" layer="42"/>
<wire x1="-5.08" y1="-0.762" x2="-5.08" y2="-1.778" width="0.508" layer="42"/>
<wire x1="-5.08" y1="-1.778" x2="-5.842" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-5.842" y1="-2.54" x2="-6.858" y2="-2.54" width="0.508" layer="42"/>
<wire x1="-6.858" y1="-2.54" x2="-7.62" y2="-1.778" width="0.508" layer="42"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="-0.762" width="0.508" layer="42"/>
<wire x1="5.08" y1="-0.762" x2="5.842" y2="0" width="0.508" layer="42"/>
<wire x1="5.842" y1="0" x2="6.858" y2="0" width="0.508" layer="42"/>
<wire x1="6.858" y1="0" x2="7.62" y2="-0.762" width="0.508" layer="42"/>
<wire x1="7.62" y1="-0.762" x2="7.62" y2="-1.778" width="0.508" layer="42"/>
<wire x1="7.62" y1="-1.778" x2="6.858" y2="-2.54" width="0.508" layer="42"/>
<wire x1="6.858" y1="-2.54" x2="5.842" y2="-2.54" width="0.508" layer="42"/>
<wire x1="5.842" y1="-2.54" x2="5.08" y2="-1.778" width="0.508" layer="42"/>
</package>
<package name="5251-03-A">
<description>&lt;b&gt;MOLEX 2.54mm KK RA CONNECTOR&lt;/b&gt;
&lt;br&gt;Fixed Orientation</description>
<text x="3.6561" y="-3.4021" size="1.016" layer="25" ratio="14" rot="R180">&gt;NAME</text>
<pad name="2" x="0" y="0" drill="1" shape="long" rot="R270"/>
<pad name="1" x="2.54" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R180"/>
<text x="-4.1641" y="-3.1481" size="0.8128" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="-2.54" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<rectangle x1="-0.254" y1="0.254" x2="0.254" y2="8.382" layer="51"/>
<rectangle x1="2.286" y1="0.254" x2="2.794" y2="8.382" layer="51"/>
<pad name="3" x="-2.54" y="0" drill="1" shape="long" rot="R270"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="-2.794" y1="0.254" x2="-2.286" y2="8.382" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51" rot="R180"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51" rot="R180"/>
<wire x1="2.54" y1="1.905" x2="3.81" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="9.906" width="0.254" layer="21"/>
<wire x1="-2.54" y1="9.906" x2="-2.286" y2="10.414" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="9.906" width="0.254" layer="21"/>
<wire x1="2.54" y1="9.906" x2="2.286" y2="10.414" width="0.254" layer="21"/>
<wire x1="2.286" y1="10.414" x2="-2.286" y2="10.414" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-3.175" x2="-3.81" y2="1.905" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="3.81" y1="1.905" x2="3.81" y2="-3.175" width="0.254" layer="21"/>
<wire x1="3.81" y1="-3.175" x2="-3.81" y2="-3.175" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51" rot="R180"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.3" layer="41"/>
<text x="3.302" y="-2.794" size="1.27" layer="51" rot="R90">1</text>
<text x="-2.032" y="-2.794" size="1.27" layer="51" rot="R90">3</text>
</package>
<package name="5251-03-S">
<description>&lt;b&gt;MOLEX 2.54mm KK  CONNECTOR&lt;/b&gt;</description>
<text x="-3.7831" y="3.2751" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<pad name="3" x="-2.54" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="2" x="0" y="0" drill="1" shape="octagon" rot="R90"/>
<pad name="1" x="2.54" y="0" drill="1" shape="octagon" rot="R90"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<text x="3.2781" y="-1.9319" size="1.27" layer="51" ratio="14" rot="R90">1</text>
<text x="-2.1067" y="-1.9319" size="1.27" layer="51" ratio="14" rot="R90">3</text>
<text x="3.7831" y="-3.5291" size="0.8128" layer="27" ratio="10" rot="R180">&gt;VALUE</text>
<wire x1="3.81" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.286" y2="2.921" width="0.254" layer="21"/>
<wire x1="2.286" y1="2.921" x2="-2.286" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.286" y1="2.921" x2="-2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-3.81" y2="2.921" width="0.254" layer="21"/>
<wire x1="-3.81" y1="2.921" x2="-3.81" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.81" y1="2.921" x2="3.81" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-2.921" x2="-3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.921" x2="-3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.286" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.921" x2="-0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.921" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="0.635" y2="-2.921" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.921" x2="1.905" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.921" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.286" x2="3.175" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.921" x2="3.81" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.286" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.286" y1="1.905" x2="2.286" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.286" y1="1.905" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.286" y2="1.397" width="0.254" layer="21"/>
<wire x1="-2.286" y1="2.921" x2="-2.286" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.286" y1="1.397" x2="2.286" y2="1.397" width="0.254" layer="21"/>
<wire x1="2.286" y1="1.397" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.286" y1="2.921" x2="2.286" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.3" layer="41"/>
</package>
<package name="5251-03-S_LONG">
<pad name="2" x="0" y="0" drill="1" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1" shape="long" rot="R90"/>
<pad name="1" x="2.54" y="0" drill="1" shape="long" rot="R90"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.3" layer="41"/>
<wire x1="-3.81" y1="2.921" x2="-3.81" y2="-2.921" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-2.921" x2="3.81" y2="-2.921" width="0.254" layer="21"/>
<wire x1="3.81" y1="-2.921" x2="3.81" y2="2.921" width="0.254" layer="21"/>
<wire x1="3.81" y1="2.921" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="2.54" y1="2.921" x2="2.286" y2="2.921" width="0.254" layer="21"/>
<wire x1="2.286" y1="2.921" x2="-2.286" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.286" y1="2.921" x2="-2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-3.81" y2="2.921" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.8575" x2="-3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-2.286" x2="-1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-1.905" y1="-2.286" x2="-1.905" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.8575" x2="-0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="-0.635" y1="-2.286" x2="0.635" y2="-2.286" width="0.254" layer="21"/>
<wire x1="0.635" y1="-2.286" x2="0.635" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.8575" x2="1.905" y2="-2.286" width="0.254" layer="21"/>
<wire x1="1.905" y1="-2.286" x2="3.175" y2="-2.286" width="0.254" layer="21"/>
<wire x1="3.175" y1="-2.286" x2="3.175" y2="-2.8575" width="0.254" layer="21"/>
<wire x1="-2.54" y1="2.921" x2="-2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.286" y2="1.397" width="0.254" layer="21"/>
<wire x1="-2.286" y1="1.397" x2="2.286" y2="1.397" width="0.254" layer="21"/>
<wire x1="2.286" y1="1.397" x2="2.54" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="2.921" width="0.254" layer="21"/>
<wire x1="-2.286" y1="2.921" x2="-2.286" y2="1.905" width="0.254" layer="21"/>
<wire x1="-2.286" y1="1.905" x2="2.286" y2="1.905" width="0.254" layer="21"/>
<wire x1="2.286" y1="1.905" x2="2.286" y2="2.921" width="0.254" layer="21"/>
<text x="-1.905" y="-1.905" size="1.27" layer="51" rot="R90">3</text>
<text x="3.175" y="-1.905" size="1.27" layer="51" rot="R90">1</text>
<text x="-3.81" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.4925" size="1.27" layer="27" rot="R180">&gt;VALUE</text>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
</package>
<package name="UX60A-MB-5ST">
<description>&lt;b&gt;MINI USB Connector HI-SPEED Certified&lt;/b&gt; Metal Shield SMT Type Without Positioning Post&lt;p&gt;
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<text x="5.08" y="-4.445" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-4.445" y="-6.985" size="1.27" layer="27">&gt;VALUE</text>
<wire x1="-6.3509" y1="3.8059" x2="2.5511" y2="3.8059" width="0.1016" layer="21"/>
<wire x1="2.5511" y1="3.8059" x2="2.5511" y2="2.5442" width="0.1016" layer="21"/>
<wire x1="2.5511" y1="2.5442" x2="2.1866" y2="2.1797" width="0.1016" layer="21" curve="-90.0314"/>
<wire x1="2.1866" y1="2.1797" x2="1.6539" y2="2.1797" width="0.1016" layer="21"/>
<wire x1="1.6539" y1="2.1797" x2="1.2894" y2="1.8152" width="0.1016" layer="51" curve="90"/>
<wire x1="1.2894" y1="1.8152" x2="1.2894" y2="-1.8578" width="0.1016" layer="51"/>
<wire x1="1.2894" y1="-1.8578" x2="1.6118" y2="-2.1802" width="0.1016" layer="51" curve="90"/>
<wire x1="1.6118" y1="-2.1802" x2="2.2006" y2="-2.1802" width="0.1016" layer="21"/>
<wire x1="2.2006" y1="-2.1802" x2="2.5511" y2="-2.5307" width="0.1016" layer="21" curve="-90.0327"/>
<wire x1="2.5511" y1="-2.5307" x2="2.5511" y2="-3.8064" width="0.1016" layer="21"/>
<wire x1="2.5511" y1="-3.8064" x2="-2.7341" y2="-3.8064" width="0.1016" layer="21"/>
<wire x1="-2.7341" y1="-3.8064" x2="-6.3509" y2="-3.8064" width="0.1016" layer="21"/>
<wire x1="-6.3509" y1="-3.8064" x2="-6.3509" y2="3.8059" width="0.1016" layer="21"/>
<wire x1="2.5511" y1="-3.8064" x2="2.5511" y2="-4.7457" width="0.1016" layer="21"/>
<wire x1="2.5511" y1="-4.7457" x2="2.3969" y2="-4.8999" width="0.1016" layer="21" curve="-90.3716"/>
<wire x1="2.3969" y1="-4.8999" x2="2.3968" y2="-4.8999" width="0.1016" layer="21" curve="-0.037277"/>
<wire x1="2.3968" y1="-4.8999" x2="2.3969" y2="-4.8999" width="0.1016" layer="21"/>
<wire x1="2.3969" y1="-4.8999" x2="2.2287" y2="-4.7317" width="0.1016" layer="21" curve="-90"/>
<wire x1="2.2287" y1="-4.7317" x2="2.2287" y2="-4.5775" width="0.1016" layer="21"/>
<wire x1="2.2287" y1="-4.5775" x2="2.0044" y2="-4.3532" width="0.1016" layer="21" curve="90.1535"/>
<wire x1="2.0044" y1="-4.3532" x2="1.9763" y2="-4.3532" width="0.1016" layer="21"/>
<wire x1="1.9763" y1="-4.3532" x2="1.752" y2="-4.5775" width="0.1016" layer="21" curve="90"/>
<wire x1="1.752" y1="-4.5775" x2="1.752" y2="-4.7457" width="0.1016" layer="21"/>
<wire x1="1.752" y1="-4.7457" x2="1.5978" y2="-4.8999" width="0.1016" layer="21" curve="-90"/>
<wire x1="1.5978" y1="-4.8999" x2="1.4296" y2="-4.7317" width="0.1016" layer="21" curve="-90"/>
<wire x1="1.4296" y1="-4.7317" x2="1.4296" y2="-3.8625" width="0.1016" layer="21"/>
<wire x1="-2.7341" y1="-3.8064" x2="-2.7341" y2="-4.7457" width="0.1016" layer="21"/>
<wire x1="-2.7341" y1="-4.7457" x2="-2.8883" y2="-4.8999" width="0.1016" layer="21" curve="-90.4476"/>
<wire x1="-2.8883" y1="-4.8999" x2="-2.8884" y2="-4.8999" width="0.1016" layer="21" curve="-0.037301"/>
<wire x1="-2.8884" y1="-4.8999" x2="-2.8883" y2="-4.8999" width="0.1016" layer="21"/>
<wire x1="-2.8883" y1="-4.8999" x2="-3.0565" y2="-4.7317" width="0.1016" layer="21" curve="-90"/>
<wire x1="-3.0565" y1="-4.7317" x2="-3.0565" y2="-4.5775" width="0.1016" layer="21"/>
<wire x1="-3.0565" y1="-4.5775" x2="-3.2808" y2="-4.3532" width="0.1016" layer="21" curve="90.1535"/>
<wire x1="-3.2808" y1="-4.3532" x2="-3.3089" y2="-4.3532" width="0.1016" layer="21"/>
<wire x1="-3.3089" y1="-4.3532" x2="-3.5332" y2="-4.5775" width="0.1016" layer="21" curve="90.0511"/>
<wire x1="-3.5332" y1="-4.5775" x2="-3.5332" y2="-4.7457" width="0.1016" layer="21"/>
<wire x1="-3.5332" y1="-4.7457" x2="-3.6874" y2="-4.8999" width="0.1016" layer="21" curve="-90"/>
<wire x1="-3.6874" y1="-4.8999" x2="-3.8556" y2="-4.7317" width="0.1016" layer="21" curve="-90"/>
<wire x1="-3.8556" y1="-4.7317" x2="-3.8556" y2="-3.8625" width="0.1016" layer="21"/>
<wire x1="-3.8555" y1="3.8058" x2="-3.8555" y2="4.7451" width="0.1016" layer="21"/>
<wire x1="-3.8555" y1="4.7451" x2="-3.7013" y2="4.8993" width="0.1016" layer="21" curve="-90.4465"/>
<wire x1="-3.7013" y1="4.8993" x2="-3.7012" y2="4.8993" width="0.1016" layer="21" curve="-0.037301"/>
<wire x1="-3.7012" y1="4.8993" x2="-3.7013" y2="4.8993" width="0.1016" layer="21"/>
<wire x1="-3.7013" y1="4.8993" x2="-3.5331" y2="4.7311" width="0.1016" layer="21" curve="-90"/>
<wire x1="-3.5331" y1="4.7311" x2="-3.5331" y2="4.5769" width="0.1016" layer="21"/>
<wire x1="-3.5331" y1="4.5769" x2="-3.3088" y2="4.3526" width="0.1016" layer="21" curve="90.1023"/>
<wire x1="-3.3088" y1="4.3526" x2="-3.2807" y2="4.3526" width="0.1016" layer="21"/>
<wire x1="-3.2807" y1="4.3526" x2="-3.0564" y2="4.5769" width="0.1016" layer="21" curve="90.1023"/>
<wire x1="-3.0564" y1="4.5769" x2="-3.0564" y2="4.7451" width="0.1016" layer="21"/>
<wire x1="-3.0564" y1="4.7451" x2="-2.9022" y2="4.8993" width="0.1016" layer="21" curve="-90"/>
<wire x1="-2.9022" y1="4.8993" x2="-2.734" y2="4.7311" width="0.1016" layer="21" curve="-90"/>
<wire x1="-2.734" y1="4.7311" x2="-2.734" y2="3.8619" width="0.1016" layer="21"/>
<wire x1="1.4297" y1="3.8058" x2="1.4297" y2="4.7451" width="0.1016" layer="21"/>
<wire x1="1.4297" y1="4.7451" x2="1.5839" y2="4.8993" width="0.1016" layer="21" curve="-90.298"/>
<wire x1="1.5839" y1="4.8993" x2="1.584" y2="4.8993" width="0.1016" layer="21" curve="-0.037253"/>
<wire x1="1.584" y1="4.8993" x2="1.5839" y2="4.8993" width="0.1016" layer="21"/>
<wire x1="1.5839" y1="4.8993" x2="1.7521" y2="4.7311" width="0.1016" layer="21" curve="-90"/>
<wire x1="1.7521" y1="4.7311" x2="1.7521" y2="4.5769" width="0.1016" layer="21"/>
<wire x1="1.7521" y1="4.5769" x2="1.9764" y2="4.3526" width="0.1016" layer="21" curve="90"/>
<wire x1="1.9764" y1="4.3526" x2="2.0045" y2="4.3526" width="0.1016" layer="21"/>
<wire x1="2.0045" y1="4.3526" x2="2.2288" y2="4.5769" width="0.1016" layer="21" curve="90.0511"/>
<wire x1="2.2288" y1="4.5769" x2="2.2288" y2="4.7451" width="0.1016" layer="21"/>
<wire x1="2.2288" y1="4.7451" x2="2.383" y2="4.8993" width="0.1016" layer="21" curve="-90"/>
<wire x1="2.383" y1="4.8993" x2="2.5512" y2="4.7311" width="0.1016" layer="21" curve="-90"/>
<wire x1="2.5512" y1="4.7311" x2="2.5512" y2="3.7918" width="0.1016" layer="21"/>
<wire x1="-5.1593" y1="3.7498" x2="-5.1593" y2="2.2077" width="0.1016" layer="21"/>
<wire x1="-5.1593" y1="2.2077" x2="-4.8088" y2="1.8572" width="0.1016" layer="21" curve="90.0327"/>
<wire x1="-4.8088" y1="1.8572" x2="-4.7668" y2="1.8572" width="0.1016" layer="21"/>
<wire x1="-4.7668" y1="1.8572" x2="-1.0938" y2="1.689" width="0.1016" layer="21"/>
<wire x1="-1.0938" y1="1.689" x2="-1.0798" y2="1.9834" width="0.1016" layer="21" curve="179.767"/>
<wire x1="-1.0798" y1="1.9834" x2="-4.4724" y2="2.2077" width="0.1016" layer="21"/>
<wire x1="-4.4724" y1="2.2077" x2="-4.4584" y2="2.6984" width="0.1016" layer="21" curve="-175.705"/>
<wire x1="-4.4584" y1="2.6984" x2="-1.0518" y2="2.9227" width="0.1016" layer="21"/>
<wire x1="-1.0518" y1="2.9227" x2="-0.9396" y2="3.0488" width="0.1016" layer="21" curve="89.1411"/>
<wire x1="-0.9396" y1="3.0488" x2="-0.9396" y2="3.7498" width="0.1016" layer="21"/>
<wire x1="-0.6452" y1="0.7497" x2="-0.9817" y2="1.0862" width="0.1016" layer="21" curve="89.9319"/>
<wire x1="-0.9817" y1="1.0862" x2="-5.0051" y2="1.2965" width="0.1016" layer="21"/>
<wire x1="-5.0051" y1="1.2965" x2="-5.0191" y2="1.0301" width="0.1016" layer="21" curve="180"/>
<wire x1="-5.0191" y1="1.0301" x2="-1.9069" y2="0.8619" width="0.1016" layer="21"/>
<wire x1="-1.9069" y1="0.8619" x2="-1.4723" y2="0.4273" width="0.1016" layer="21" curve="-90"/>
<wire x1="-1.4723" y1="0.4273" x2="-1.4723" y2="-0.4559" width="0.1016" layer="21"/>
<wire x1="-1.4723" y1="-0.4559" x2="-1.8929" y2="-0.8765" width="0.1016" layer="21" curve="-90"/>
<wire x1="-1.8929" y1="-0.8765" x2="-1.963" y2="-0.8765" width="0.1016" layer="21"/>
<wire x1="-1.963" y1="-0.8765" x2="-5.0051" y2="-1.0167" width="0.1016" layer="21"/>
<wire x1="-5.0051" y1="-1.0167" x2="-5.0051" y2="-1.297" width="0.1016" layer="21" curve="174.689"/>
<wire x1="-5.0051" y1="-1.297" x2="-1.0097" y2="-1.0868" width="0.1016" layer="21"/>
<wire x1="-1.0097" y1="-1.0868" x2="-0.6452" y2="-0.7223" width="0.1016" layer="21" curve="90"/>
<wire x1="-0.6452" y1="-0.7223" x2="-0.6452" y2="0.7497" width="0.1016" layer="21"/>
<wire x1="-5.1592" y1="-3.7504" x2="-5.1592" y2="-2.2083" width="0.1016" layer="21"/>
<wire x1="-5.1592" y1="-2.2083" x2="-4.8087" y2="-1.8578" width="0.1016" layer="21" curve="-89.9673"/>
<wire x1="-4.8087" y1="-1.8578" x2="-4.7667" y2="-1.8578" width="0.1016" layer="21"/>
<wire x1="-4.7667" y1="-1.8578" x2="-1.0937" y2="-1.6896" width="0.1016" layer="21"/>
<wire x1="-1.0937" y1="-1.6896" x2="-1.0797" y2="-1.984" width="0.1016" layer="21" curve="-179.767"/>
<wire x1="-1.0797" y1="-1.984" x2="-4.4723" y2="-2.2083" width="0.1016" layer="21"/>
<wire x1="-4.4723" y1="-2.2083" x2="-4.4583" y2="-2.699" width="0.1016" layer="21" curve="175.705"/>
<wire x1="-4.4583" y1="-2.699" x2="-1.0517" y2="-2.9233" width="0.1016" layer="21"/>
<wire x1="-1.0517" y1="-2.9233" x2="-0.9395" y2="-3.0494" width="0.1016" layer="21" curve="-89.1411"/>
<wire x1="-0.9395" y1="-3.0494" x2="-0.9395" y2="-3.7504" width="0.1016" layer="21"/>
<rectangle x1="1.3034" y1="1.4367" x2="2.6072" y2="1.7731" layer="51"/>
<rectangle x1="1.3034" y1="0.6376" x2="2.6072" y2="0.974" layer="51"/>
<rectangle x1="1.3034" y1="-0.1615" x2="2.6072" y2="0.1749" layer="51"/>
<rectangle x1="1.3034" y1="-0.9606" x2="2.6072" y2="-0.6242" layer="51"/>
<rectangle x1="1.3034" y1="-1.7596" x2="2.6072" y2="-1.4232" layer="51"/>
<smd name="M3" x="2" y="4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="M4" x="2" y="-4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="M2" x="-3.3" y="4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="M1" x="-3.3" y="-4.2" dx="2.2" dy="2.5" layer="1"/>
<smd name="1" x="2.25" y="1.6" dx="2" dy="0.5" layer="1"/>
<smd name="2" x="2.25" y="0.8" dx="2" dy="0.5" layer="1"/>
<smd name="3" x="2.25" y="0" dx="2" dy="0.5" layer="1"/>
<smd name="4" x="2.25" y="-0.8" dx="2" dy="0.5" layer="1"/>
<smd name="5" x="2.25" y="-1.6" dx="2" dy="0.5" layer="1"/>
<rectangle x1="-6.4" y1="-3.85" x2="-4.425" y2="3.85" layer="41"/>
<rectangle x1="-2.175" y1="-3.85" x2="-0.45" y2="3.85" layer="41"/>
<rectangle x1="-4.425" y1="-2.925" x2="-2.175" y2="2.925" layer="41"/>
<rectangle x1="-0.45" y1="-1.85" x2="1.225" y2="1.85" layer="41"/>
</package>
<package name="MA02-1">
<pad name="1" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-2.54" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<text x="-3.81" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-2.54" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<text x="-3.81" y="-0.635" size="1.27" layer="21" ratio="10">1</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="M-1">
<pin name="S" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="1.016" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="PWRN">
<pin name="GND" x="0" y="-7.62" visible="pad" length="middle" direction="pwr" rot="R90"/>
<pin name="VCC" x="0" y="7.62" visible="pad" length="middle" direction="pwr" rot="R270"/>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
<text x="-0.635" y="-0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.905" y="-5.842" size="1.27" layer="95" rot="R90">GND</text>
<text x="1.905" y="2.54" size="1.27" layer="95" rot="R90">VCC</text>
</symbol>
<symbol name="MV">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="-0.762" y="1.397" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="M">
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
</symbol>
<symbol name="MINI-USB">
<text x="-2.54" y="11.43" size="1.778" layer="95" font="vector">&gt;NAME</text>
<text x="10.16" y="-7.62" size="1.778" layer="96" font="vector" rot="R90">&gt;VALUE</text>
<pin name="1" x="-5.08" y="5.08" visible="pin" direction="in"/>
<pin name="2" x="-5.08" y="2.54" visible="pin" direction="in"/>
<pin name="3" x="-5.08" y="0" visible="pin" direction="in"/>
<pin name="4" x="-5.08" y="-2.54" visible="pin" direction="in"/>
<pin name="5" x="-5.08" y="-5.08" visible="pin" direction="in"/>
<wire x1="-2.54" y1="6.35" x2="-2.54" y2="-6.35" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-6.35" x2="-1.27" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="-1.27" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="1.016" y2="-8.128" width="0.254" layer="94" curve="-53.1301"/>
<wire x1="1.016" y1="-8.128" x2="2.54" y2="-8.89" width="0.254" layer="94" curve="53.1301"/>
<wire x1="2.54" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="6.35" y2="-7.62" width="0.254" layer="94" curve="90"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="6.35" x2="-1.27" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="-1.27" y1="7.62" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="1.016" y2="8.128" width="0.254" layer="94" curve="53.1301"/>
<wire x1="1.016" y1="8.128" x2="2.54" y2="8.89" width="0.254" layer="94" curve="-53.1301"/>
<wire x1="2.54" y1="8.89" x2="5.08" y2="8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="8.89" x2="6.35" y2="7.62" width="0.254" layer="94" curve="-90"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="1.27" y2="-6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="-6.35" x2="3.81" y2="-6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="-6.35" x2="3.81" y2="6.35" width="0.254" layer="94"/>
<wire x1="3.81" y1="6.35" x2="1.27" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.27" y1="6.35" x2="0" y2="5.08" width="0.254" layer="94"/>
<pin name="SHELL" x="2.54" y="-10.16" length="point" rot="R90"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.89" width="0.254" layer="94"/>
</symbol>
<symbol name="MA02-1">
<text x="-3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<pin name="P$1" x="5.08" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="P$2" x="5.08" y="0" length="middle" direction="pas" rot="R180"/>
<wire x1="1.27" y1="-2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<text x="-3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="2.54" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="5.08" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<text x="-3.81" y="5.842" size="1.778" layer="95">&gt;NAME</text>
<wire x1="-3.81" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="UART" prefix="CON">
<description>&lt;b&gt;Robotech &lt;br&gt; Universal Asynchronous Receiver Transmitter &lt;/b&gt;</description>
<gates>
<gate name="_RX" symbol="M-1" x="7.62" y="2.54"/>
<gate name="_TX" symbol="M-1" x="7.62" y="-2.54"/>
<gate name="_PWR" symbol="PWRN" x="-10.16" y="0" addlevel="request"/>
</gates>
<devices>
<device name="-S" package="IL-G-04-S">
<connects>
<connect gate="_PWR" pin="GND" pad="1"/>
<connect gate="_PWR" pin="VCC" pad="4"/>
<connect gate="_RX" pin="S" pad="2"/>
<connect gate="_TX" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="POWER-SOURCE" prefix="CON">
<gates>
<gate name="_VCC" symbol="M-1" x="5.08" y="5.08"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-5.08"/>
</gates>
<devices>
<device name="-M-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-M-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-H" package="D3200S-2-TAB-HORIZONTAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-V" package="D3200S-2-TAB-VERTICAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-D-V-H" package="D3200S-2-TAB-VERTICAL-AND-HORIZONTAL">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_VCC" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RT-2PIN" prefix="CON">
<gates>
<gate name="_SIGNAL" symbol="M-1" x="5.08" y="2.54"/>
<gate name="_GND" symbol="M-1" x="5.08" y="-2.54"/>
</gates>
<devices>
<device name="-A" package="5251-02-A">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="5251-02-S">
<connects>
<connect gate="_GND" pin="S" pad="1"/>
<connect gate="_SIGNAL" pin="S" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOLEX-5251-03" prefix="CON">
<description>&lt;b&gt;CONNECTOR&lt;/b&gt;&lt;p&gt;

Molex 5251 3-pin&lt;p&gt;

wire to board 2.54 mm (.1 inch) pitch header</description>
<gates>
<gate name="-1" symbol="MV" x="0" y="5.08" swaplevel="1"/>
<gate name="-2" symbol="M" x="0" y="0" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="-5.08" swaplevel="1"/>
</gates>
<devices>
<device name="-A" package="5251-03-A">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S" package="5251-03-S">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-S_LONG" package="5251-03-S_LONG">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MINI-USB-" prefix="CON">
<description>&lt;b&gt;MINI USB Connector&lt;/b&gt;&lt;p&gt;
Source: http://www.hirose.co.jp/cataloge_hp/e24000019.pdf</description>
<gates>
<gate name="G$1" symbol="MINI-USB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="UX60A-MB-5ST">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="SHELL" pad="M1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA02-1" prefix="CON">
<description>&lt;b&gt;Jumper Pin Header&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA02-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA02-1">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="1X07">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="3.81" y1="0.635" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.81" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.35" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.27" x2="-8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="8.255" y2="1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.27" x2="8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0.635" x2="8.89" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-0.635" x2="8.255" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-1.27" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="0" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-8.9662" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-8.89" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="4.826" y1="-0.254" x2="5.334" y2="0.254" layer="51"/>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<rectangle x1="-5.334" y1="-0.254" x2="-4.826" y2="0.254" layer="51"/>
<rectangle x1="-7.874" y1="-0.254" x2="-7.366" y2="0.254" layer="51"/>
<rectangle x1="7.366" y1="-0.254" x2="7.874" y2="0.254" layer="51"/>
</package>
<package name="1X07/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-8.89" y1="-1.905" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="0.635" x2="-8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="6.985" x2="-7.62" y2="1.27" width="0.762" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.985" x2="-5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="6.985" x2="-2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="6.985" x2="0" y2="1.27" width="0.762" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="0.635" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="6.985" x2="2.54" y2="1.27" width="0.762" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0.635" x2="3.81" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="6.985" x2="5.08" y2="1.27" width="0.762" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="8.89" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.89" y1="-1.905" x2="8.89" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0.635" x2="6.35" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="6.985" x2="7.62" y2="1.27" width="0.762" layer="21"/>
<pad name="1" x="-7.62" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="0" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="2.54" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<pad name="7" x="7.62" y="-3.81" drill="1.016" shape="long" rot="R90"/>
<text x="-9.525" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="10.795" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-8.001" y1="0.635" x2="-7.239" y2="1.143" layer="21"/>
<rectangle x1="-5.461" y1="0.635" x2="-4.699" y2="1.143" layer="21"/>
<rectangle x1="-2.921" y1="0.635" x2="-2.159" y2="1.143" layer="21"/>
<rectangle x1="-0.381" y1="0.635" x2="0.381" y2="1.143" layer="21"/>
<rectangle x1="2.159" y1="0.635" x2="2.921" y2="1.143" layer="21"/>
<rectangle x1="4.699" y1="0.635" x2="5.461" y2="1.143" layer="21"/>
<rectangle x1="7.239" y1="0.635" x2="8.001" y2="1.143" layer="21"/>
<rectangle x1="-8.001" y1="-2.921" x2="-7.239" y2="-1.905" layer="21"/>
<rectangle x1="-5.461" y1="-2.921" x2="-4.699" y2="-1.905" layer="21"/>
<rectangle x1="-2.921" y1="-2.921" x2="-2.159" y2="-1.905" layer="21"/>
<rectangle x1="-0.381" y1="-2.921" x2="0.381" y2="-1.905" layer="21"/>
<rectangle x1="2.159" y1="-2.921" x2="2.921" y2="-1.905" layer="21"/>
<rectangle x1="4.699" y1="-2.921" x2="5.461" y2="-1.905" layer="21"/>
<rectangle x1="7.239" y1="-2.921" x2="8.001" y2="-1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD7">
<wire x1="-6.35" y1="-10.16" x2="1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="10.16" x2="-6.35" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="10.16" x2="-6.35" y2="-10.16" width="0.4064" layer="94"/>
<text x="-6.35" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="7" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-1X7" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINHD7" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X07">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="1X07/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="lcd-aitendo">
<packages>
<package name="Z180SN009">
<wire x1="-17" y1="45.83" x2="-17" y2="0" width="0.127" layer="21"/>
<wire x1="-17" y1="0" x2="-13.97" y2="0" width="0.127" layer="21"/>
<wire x1="-13.97" y1="0" x2="-7.62" y2="0" width="0.127" layer="21"/>
<wire x1="-7.62" y1="0" x2="10.16" y2="0" width="0.127" layer="21"/>
<wire x1="10.16" y1="0" x2="13.97" y2="0" width="0.127" layer="21"/>
<wire x1="13.97" y1="0" x2="17" y2="0" width="0.127" layer="21"/>
<wire x1="17" y1="0" x2="17" y2="45.83" width="0.127" layer="21"/>
<wire x1="17" y1="45.83" x2="-17" y2="45.83" width="0.127" layer="21"/>
<wire x1="-14.015" y1="43" x2="14.015" y2="43" width="0.127" layer="21"/>
<wire x1="14.015" y1="43" x2="14.015" y2="7.96" width="0.127" layer="21"/>
<wire x1="14.015" y1="7.96" x2="-14.015" y2="7.96" width="0.127" layer="21"/>
<wire x1="-14.015" y1="7.96" x2="-14.015" y2="43" width="0.127" layer="21"/>
<smd name="1" x="5.2" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="2" x="4.4" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="3" x="3.6" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="4" x="2.8" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="5" x="2" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="6" x="1.2" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="7" x="0.4" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="8" x="-0.4" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="9" x="-1.2" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="10" x="-2" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="11" x="-2.8" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="12" x="-3.6" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="13" x="-4.4" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<smd name="14" x="-5.2" y="11.55" dx="5" dy="0.4" layer="1" rot="R90"/>
<wire x1="-13.97" y1="0" x2="-13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.97" y1="2.54" x2="-15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="-15.24" y1="2.54" x2="-15.24" y2="44.45" width="0.127" layer="21"/>
<wire x1="-15.24" y1="44.45" x2="15.24" y2="44.45" width="0.127" layer="21"/>
<wire x1="15.24" y1="44.45" x2="15.24" y2="2.54" width="0.127" layer="21"/>
<wire x1="15.24" y1="2.54" x2="13.97" y2="2.54" width="0.127" layer="21"/>
<wire x1="13.97" y1="2.54" x2="13.97" y2="0" width="0.127" layer="21"/>
<text x="-16.51" y="48.26" size="1.27" layer="25">&gt;NAME</text>
<text x="-16.51" y="46.99" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="Z180SN009">
<pin name="LED+" x="-22.86" y="12.7" length="middle"/>
<pin name="!RST" x="-22.86" y="10.16" length="middle"/>
<pin name="A0" x="-22.86" y="7.62" length="middle"/>
<pin name="SDA" x="-22.86" y="5.08" length="middle"/>
<pin name="SCK" x="-22.86" y="2.54" length="middle"/>
<pin name="VCC" x="-22.86" y="0" length="middle"/>
<pin name="CS" x="-22.86" y="-2.54" length="middle"/>
<pin name="GND@1" x="-22.86" y="-5.08" length="middle"/>
<wire x1="-17.78" y1="-17.78" x2="-17.78" y2="15.24" width="0.254" layer="94"/>
<wire x1="-17.78" y1="15.24" x2="15.24" y2="15.24" width="0.254" layer="94"/>
<wire x1="15.24" y1="15.24" x2="15.24" y2="-17.78" width="0.254" layer="94"/>
<wire x1="15.24" y1="-17.78" x2="-17.78" y2="-17.78" width="0.254" layer="94"/>
<pin name="LED-" x="-22.86" y="-7.62" length="middle"/>
<pin name="VCCIO" x="-22.86" y="-10.16" length="middle"/>
<pin name="GND@2" x="-22.86" y="-12.7" length="middle"/>
<pin name="GND@3" x="-22.86" y="-15.24" length="middle"/>
<text x="-7.62" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="Z180SN009" prefix="LCD">
<gates>
<gate name="G$1" symbol="Z180SN009" x="0" y="0"/>
</gates>
<devices>
<device name="" package="Z180SN009">
<connects>
<connect gate="G$1" pin="!RST" pad="6"/>
<connect gate="G$1" pin="A0" pad="7"/>
<connect gate="G$1" pin="CS" pad="12"/>
<connect gate="G$1" pin="GND@1" pad="2"/>
<connect gate="G$1" pin="GND@2" pad="5"/>
<connect gate="G$1" pin="GND@3" pad="13"/>
<connect gate="G$1" pin="LED+" pad="4"/>
<connect gate="G$1" pin="LED-" pad="3"/>
<connect gate="G$1" pin="SCK" pad="9"/>
<connect gate="G$1" pin="SDA" pad="8"/>
<connect gate="G$1" pin="VCC" pad="10"/>
<connect gate="G$1" pin="VCCIO" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Diode_Robotech">
<description>&lt;h3&gt;RoboTech EAGLE Library&lt;/h3&gt;
Diode Library &lt;br&gt;
$Rev: 25542 $

&lt;p&gt;
Since 2007&lt;br&gt;
by RoboTech&lt;br&gt;
Jun'ichi Takisawa&lt;br&gt;
Hiroki Yabe&lt;br&gt;
Katsuhiko Nishimra&lt;br&gt;
Takuo Sawada&lt;br&gt;
&lt;/p&gt;</description>
<packages>
<package name="SC59">
<description>SC-59 (SOT23)</description>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<smd name="C" x="0" y="1.2" dx="1" dy="1.4" layer="1"/>
<smd name="E" x="0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<smd name="B" x="-0.95" y="-1.2" dx="1" dy="1.4" layer="1"/>
<rectangle x1="-0.2286" y1="0.9112" x2="0.2286" y2="1.4954" layer="51"/>
<rectangle x1="0.7112" y1="-1.4954" x2="1.1684" y2="-0.9112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.4954" x2="-0.7112" y2="-0.9112" layer="51"/>
<wire x1="1.4224" y1="0.8604" x2="1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="1.4224" y1="-0.8604" x2="-1.4224" y2="-0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="-0.8604" x2="-1.4224" y2="0.8604" width="0.127" layer="51"/>
<wire x1="-1.4224" y1="0.8604" x2="1.4224" y2="0.8604" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="D">
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1SS196" prefix="D">
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SC59">
<connects>
<connect gate="G$1" pin="A" pad="E"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="micro-stm" deviceset="STM32F401R*T" device="" technology="C"/>
<part name="IC2" library="silabs" deviceset="SI5351" device=""/>
<part name="IC3" library="IC_Robotech" deviceset="TLV320AIC3204IRHBR" device="H" value="TLV320AIC3204IRHBRH"/>
<part name="IC4" library="IC_Robotech" deviceset="SSM2305RMZ-REEL7" device=""/>
<part name="S1" library="IC_Robotech" deviceset="MASWSS0115TR-3000" device=""/>
<part name="IC5" library="IC_Robotech" deviceset="AZ1086?-*" device="H" technology="3.3"/>
<part name="IC6" library="IC_Robotech" deviceset="AZ1086?-*" device="H" technology="5.0"/>
<part name="T1" library="macom" deviceset="ETC4-1-2TR" device=""/>
<part name="IC7" library="AnalogDevicesMixers" deviceset="LT5506EUF" device="HAND"/>
<part name="IC8" library="AnalogDevicesMixers" deviceset="LTC5598IUFTRPBF" device="HAND"/>
<part name="U$1" library="IC_Robotech" deviceset="LMV324" device=""/>
<part name="X1" library="IQD-Frequency-Products" deviceset="CRYSTALS-GND-LID" device="-4-PAD" technology="12SMX-B"/>
<part name="X2" library="IQD-Frequency-Products" deviceset="CRYSTALS-GND-LID" device="-4-PAD" technology="12SMX-B"/>
<part name="SUPPLY1" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY2" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY3" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY4" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY5" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY6" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY7" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY8" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY9" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY10" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY11" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY12" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY13" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$2" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="U$3" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="U$4" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="U$5" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$6" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$7" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$8" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$9" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$10" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="C1" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C2" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C3" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C4" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C5" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C6" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C7" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C8" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C9" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C10" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C11" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C12" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C13" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C14" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C15" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C16" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C17" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C18" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C19" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="U$11" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="SUPPLY14" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY15" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY16" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C20" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C21" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY17" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY18" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY19" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C22" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C23" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C24" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY20" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY21" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C25" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C26" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R1" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R2" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R3" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R4" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R5" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R6" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C27" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C28" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C29" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C30" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY22" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY23" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R7" library="Passive_Robotech" deviceset="R" device="-2012" value="3.9K"/>
<part name="R8" library="Passive_Robotech" deviceset="R" device="-2012" value="5.1K"/>
<part name="C31" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY24" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY25" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="R9" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C32" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY26" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C33" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="R10" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R11" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R12" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R13" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C34" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C35" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY27" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY28" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY29" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$12" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="C36" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY30" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C37" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C38" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY31" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY32" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY33" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY34" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON1" library="Connector_Robotech" deviceset="UART" device="-S"/>
<part name="R14" library="Passive_Robotech" deviceset="R" device="-2012" value="3.9K"/>
<part name="U$13" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="SUPPLY35" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY36" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY37" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY38" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C39" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C40" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C41" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C42" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY39" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C43" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="U1" library="Passive_Robotech" deviceset="PEC12R-4215F-S0024" device=""/>
<part name="U2" library="Passive_Robotech" deviceset="PEC12R-4215F-S0024" device=""/>
<part name="S2" library="Passive_Robotech" deviceset="B3FS-4052P" device=""/>
<part name="S3" library="Passive_Robotech" deviceset="B3FS-4052P" device=""/>
<part name="S4" library="Passive_Robotech" deviceset="B3FS-4052P" device=""/>
<part name="SUPPLY41" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY42" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$14" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="SUPPLY43" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY44" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C44" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C45" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C46" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="C47" library="Passive_Robotech" deviceset="C" device="-3225"/>
<part name="SUPPLY45" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY46" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY47" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY48" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$15" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$16" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="R15" library="Passive_Robotech" deviceset="R" device="-2012" value="3.9K"/>
<part name="R16" library="Passive_Robotech" deviceset="R" device="-2012" value="3.9K"/>
<part name="SUPPLY49" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY50" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY51" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY52" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY53" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON2" library="Connector_Robotech" deviceset="POWER-SOURCE" device="-M-A"/>
<part name="CON3" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="CON4" library="Connector_Robotech" deviceset="MOLEX-5251-03" device="-A"/>
<part name="SUPPLY54" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="CON5" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="CON6" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="SUPPLY55" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C48" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY56" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="JP1" library="pinhead" deviceset="PINHD-1X7" device=""/>
<part name="SUPPLY57" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="P+1" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="P+2" library="Supply_Robotech" deviceset="VCC_1" device=""/>
<part name="U$17" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="LCD1" library="lcd-aitendo" deviceset="Z180SN009" device=""/>
<part name="R17" library="Passive_Robotech" deviceset="R" device="-2012" value="10"/>
<part name="U$18" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="C49" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C50" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="U$19" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$20" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="SUPPLY58" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY59" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY60" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="D1" library="Diode_Robotech" deviceset="1SS196" device=""/>
<part name="CON7" library="Connector_Robotech" deviceset="MINI-USB-" device=""/>
<part name="SUPPLY61" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY62" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="IC9" library="IC_Robotech" deviceset="MCP4901T-E_MS" device=""/>
<part name="R18" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="C51" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C52" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="C53" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="U$21" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="U$22" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="SUPPLY63" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY64" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY65" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="SUPPLY66" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="L1" library="Passive_Robotech" deviceset="L" device="-2125" value="BK2125HM121"/>
<part name="L2" library="Passive_Robotech" deviceset="L" device="-2125" value="BK2125HM121"/>
<part name="SUPPLY67" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$23" library="Supply_Robotech" deviceset="+5V" device=""/>
<part name="C54" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="CON8" library="Connector_Robotech" deviceset="RT-2PIN" device="-A"/>
<part name="SUPPLY68" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C55" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY69" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="C56" library="Passive_Robotech" deviceset="C" device="-2012"/>
<part name="SUPPLY70" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$24" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="P+3" library="Supply_Robotech" deviceset="VCC_2" device=""/>
<part name="P+4" library="Supply_Robotech" deviceset="VCC_2" device=""/>
<part name="CON9" library="Connector_Robotech" deviceset="MA02-1" device=""/>
<part name="SUPPLY40" library="Supply_Robotech" deviceset="GND" device=""/>
<part name="U$25" library="Supply_Robotech" deviceset="+3V3" device=""/>
<part name="R19" library="Passive_Robotech" deviceset="R" device="-2012"/>
<part name="R20" library="Passive_Robotech" deviceset="R" device="-2012" value="4.7k"/>
<part name="R21" library="Passive_Robotech" deviceset="R" device="-2012" value="1.2k"/>
<part name="SUPPLY71" library="Supply_Robotech" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-187.96" y="91.44" size="1.778" layer="250">CW Key</text>
<text x="-185.42" y="73.66" size="1.778" layer="250">PTT</text>
<text x="137.16" y="106.68" size="1.778" layer="250">MIC</text>
<text x="137.16" y="88.9" size="1.778" layer="250">SPK</text>
</plain>
<instances>
<instance part="IC1" gate="MCU" x="-106.68" y="30.48"/>
<instance part="IC2" gate="G$1" x="-7.62" y="-40.64"/>
<instance part="IC3" gate="G$1" x="-38.1" y="33.02"/>
<instance part="IC4" gate="G$1" x="93.98" y="81.28" rot="MR180"/>
<instance part="S1" gate="G$1" x="198.12" y="20.32"/>
<instance part="IC5" gate="G$1" x="-60.96" y="119.38"/>
<instance part="IC6" gate="G$1" x="-111.76" y="119.38"/>
<instance part="T1" gate="A" x="180.34" y="-17.78"/>
<instance part="IC7" gate="A" x="147.32" y="-10.16" rot="MR0"/>
<instance part="IC8" gate="A" x="109.22" y="48.26"/>
<instance part="U$1" gate="G$1" x="60.96" y="55.88"/>
<instance part="U$1" gate="G$3" x="60.96" y="40.64"/>
<instance part="U$1" gate="G$4" x="60.96" y="17.78"/>
<instance part="U$1" gate="G$5" x="60.96" y="5.08"/>
<instance part="X1" gate="G$1" x="-149.86" y="15.24"/>
<instance part="X2" gate="G$1" x="-33.02" y="-35.56" rot="R270"/>
<instance part="SUPPLY1" gate="GND" x="-149.86" y="7.62"/>
<instance part="SUPPLY2" gate="GND" x="-147.32" y="7.62"/>
<instance part="SUPPLY3" gate="GND" x="-129.54" y="2.54"/>
<instance part="SUPPLY4" gate="GND" x="10.16" y="-60.96"/>
<instance part="SUPPLY5" gate="GND" x="-38.1" y="-10.16"/>
<instance part="SUPPLY6" gate="GND" x="15.24" y="33.02"/>
<instance part="SUPPLY7" gate="GND" x="12.7" y="20.32" rot="R90"/>
<instance part="SUPPLY8" gate="GND" x="-43.18" y="15.24"/>
<instance part="SUPPLY9" gate="GND" x="96.52" y="-43.18"/>
<instance part="SUPPLY10" gate="GND" x="149.86" y="-38.1"/>
<instance part="SUPPLY11" gate="GND" x="177.8" y="10.16"/>
<instance part="SUPPLY12" gate="GND" x="132.08" y="81.28"/>
<instance part="SUPPLY13" gate="GND" x="175.26" y="-27.94"/>
<instance part="U$2" gate="1" x="-93.98" y="121.92"/>
<instance part="U$3" gate="1" x="182.88" y="48.26"/>
<instance part="U$4" gate="1" x="170.18" y="-20.32"/>
<instance part="U$5" gate="G$1" x="-40.64" y="124.46"/>
<instance part="U$6" gate="G$1" x="-134.62" y="10.16"/>
<instance part="U$7" gate="G$1" x="-40.64" y="35.56"/>
<instance part="U$8" gate="G$1" x="20.32" y="27.94"/>
<instance part="U$9" gate="G$1" x="25.4" y="-43.18"/>
<instance part="U$10" gate="G$1" x="-22.86" y="-33.02"/>
<instance part="C1" gate="G$1" x="170.18" y="-25.4"/>
<instance part="C2" gate="G$1" x="187.96" y="-15.24" rot="R90"/>
<instance part="C3" gate="G$1" x="187.96" y="25.4" rot="R90"/>
<instance part="C4" gate="G$1" x="73.66" y="-12.7" rot="R90"/>
<instance part="C5" gate="G$1" x="73.66" y="-20.32" rot="R90"/>
<instance part="C6" gate="G$1" x="73.66" y="-25.4" rot="R90"/>
<instance part="C7" gate="G$1" x="73.66" y="-33.02" rot="R90"/>
<instance part="C8" gate="G$1" x="81.28" y="88.9" rot="R270"/>
<instance part="C9" gate="G$1" x="81.28" y="83.82" rot="R270"/>
<instance part="C10" gate="G$1" x="137.16" y="83.82"/>
<instance part="C11" gate="G$1" x="-134.62" y="-15.24"/>
<instance part="C12" gate="G$1" x="-137.16" y="2.54"/>
<instance part="C13" gate="G$1" x="-48.26" y="20.32" rot="R270"/>
<instance part="C14" gate="G$1" x="22.86" y="22.86" rot="R90"/>
<instance part="C15" gate="G$1" x="-22.86" y="-53.34"/>
<instance part="C16" gate="G$1" x="10.16" y="-50.8"/>
<instance part="C17" gate="G$1" x="33.02" y="35.56"/>
<instance part="C18" gate="G$1" x="33.02" y="2.54" rot="R270"/>
<instance part="C19" gate="G$1" x="165.1" y="-30.48"/>
<instance part="U$11" gate="1" x="165.1" y="-25.4"/>
<instance part="SUPPLY14" gate="GND" x="165.1" y="-38.1"/>
<instance part="SUPPLY15" gate="GND" x="170.18" y="-38.1"/>
<instance part="SUPPLY16" gate="GND" x="185.42" y="-22.86"/>
<instance part="C20" gate="G$1" x="190.5" y="43.18"/>
<instance part="C21" gate="G$1" x="182.88" y="17.78"/>
<instance part="SUPPLY17" gate="GND" x="190.5" y="35.56"/>
<instance part="SUPPLY18" gate="GND" x="182.88" y="10.16"/>
<instance part="SUPPLY19" gate="GND" x="106.68" y="15.24"/>
<instance part="C22" gate="G$1" x="76.2" y="-40.64" rot="R90"/>
<instance part="C23" gate="G$1" x="99.06" y="-35.56"/>
<instance part="C24" gate="G$1" x="160.02" y="-33.02"/>
<instance part="SUPPLY20" gate="GND" x="160.02" y="-40.64"/>
<instance part="SUPPLY21" gate="GND" x="193.04" y="15.24"/>
<instance part="C25" gate="G$1" x="233.68" y="17.78" rot="R90"/>
<instance part="C26" gate="G$1" x="101.6" y="35.56"/>
<instance part="R1" gate="G$1" x="43.18" y="58.42"/>
<instance part="R2" gate="G$1" x="43.18" y="38.1"/>
<instance part="R3" gate="G$1" x="58.42" y="33.02"/>
<instance part="R4" gate="G$1" x="40.64" y="20.32"/>
<instance part="R5" gate="G$1" x="43.18" y="2.54"/>
<instance part="R6" gate="G$1" x="58.42" y="-2.54"/>
<instance part="C27" gate="G$1" x="48.26" y="53.34"/>
<instance part="C28" gate="G$1" x="55.88" y="27.94" rot="R90"/>
<instance part="C29" gate="G$1" x="48.26" y="15.24"/>
<instance part="C30" gate="G$1" x="55.88" y="-7.62" rot="R90"/>
<instance part="SUPPLY22" gate="GND" x="48.26" y="7.62"/>
<instance part="SUPPLY23" gate="GND" x="48.26" y="45.72"/>
<instance part="R7" gate="G$1" x="20.32" y="-5.08"/>
<instance part="R8" gate="G$1" x="27.94" y="-10.16" rot="R90"/>
<instance part="C31" gate="G$1" x="33.02" y="-7.62"/>
<instance part="SUPPLY24" gate="GND" x="27.94" y="-17.78"/>
<instance part="SUPPLY25" gate="GND" x="33.02" y="-15.24"/>
<instance part="R9" gate="G$1" x="99.06" y="40.64" rot="R180"/>
<instance part="C32" gate="G$1" x="93.98" y="35.56"/>
<instance part="SUPPLY26" gate="GND" x="93.98" y="20.32"/>
<instance part="C33" gate="G$1" x="88.9" y="43.18" rot="R90"/>
<instance part="R10" gate="G$1" x="60.96" y="-12.7"/>
<instance part="R11" gate="G$1" x="60.96" y="-20.32"/>
<instance part="R12" gate="G$1" x="60.96" y="-25.4"/>
<instance part="R13" gate="G$1" x="60.96" y="-33.02"/>
<instance part="C34" gate="G$1" x="50.8" y="-15.24"/>
<instance part="C35" gate="G$1" x="50.8" y="-27.94"/>
<instance part="SUPPLY27" gate="GND" x="-22.86" y="-60.96"/>
<instance part="SUPPLY28" gate="GND" x="-43.18" y="-38.1" rot="R270"/>
<instance part="SUPPLY29" gate="GND" x="27.94" y="20.32"/>
<instance part="U$12" gate="G$1" x="15.24" y="15.24"/>
<instance part="C36" gate="G$1" x="22.86" y="10.16" rot="R90"/>
<instance part="SUPPLY30" gate="GND" x="30.48" y="10.16" rot="R90"/>
<instance part="C37" gate="G$1" x="-157.48" y="12.7"/>
<instance part="C38" gate="G$1" x="-142.24" y="12.7"/>
<instance part="SUPPLY31" gate="GND" x="-134.62" y="-22.86"/>
<instance part="SUPPLY32" gate="GND" x="-137.16" y="-5.08"/>
<instance part="SUPPLY33" gate="GND" x="-142.24" y="5.08"/>
<instance part="SUPPLY34" gate="GND" x="-157.48" y="5.08"/>
<instance part="CON1" gate="_RX" x="-78.74" y="15.24"/>
<instance part="CON1" gate="_TX" x="-78.74" y="12.7"/>
<instance part="R14" gate="G$1" x="-10.16" y="99.06" rot="R90"/>
<instance part="IC1" gate="PWR" x="-114.3" y="-43.18"/>
<instance part="U$13" gate="G$1" x="-121.92" y="-25.4"/>
<instance part="SUPPLY35" gate="GND" x="-121.92" y="-63.5"/>
<instance part="SUPPLY36" gate="GND" x="-116.84" y="-63.5"/>
<instance part="SUPPLY37" gate="GND" x="-111.76" y="-63.5"/>
<instance part="SUPPLY38" gate="GND" x="-106.68" y="-63.5"/>
<instance part="C39" gate="G$1" x="-121.92" y="-40.64"/>
<instance part="C40" gate="G$1" x="-116.84" y="-40.64"/>
<instance part="C41" gate="G$1" x="-111.76" y="-40.64"/>
<instance part="C42" gate="G$1" x="-106.68" y="-40.64"/>
<instance part="SUPPLY39" gate="GND" x="-55.88" y="20.32" rot="R270"/>
<instance part="C43" gate="G$1" x="17.78" y="50.8"/>
<instance part="U1" gate="G$1" x="-175.26" y="58.42" rot="R180"/>
<instance part="U2" gate="G$1" x="-175.26" y="45.72" rot="R180"/>
<instance part="S2" gate="G$1" x="-205.74" y="33.02"/>
<instance part="S3" gate="G$1" x="-205.74" y="22.86"/>
<instance part="S4" gate="G$1" x="-205.74" y="12.7"/>
<instance part="SUPPLY41" gate="GND" x="73.66" y="88.9" rot="R270"/>
<instance part="SUPPLY42" gate="GND" x="137.16" y="76.2"/>
<instance part="U$14" gate="1" x="137.16" y="86.36"/>
<instance part="SUPPLY43" gate="GND" x="-60.96" y="109.22"/>
<instance part="SUPPLY44" gate="GND" x="-111.76" y="106.68"/>
<instance part="C44" gate="G$1" x="-43.18" y="116.84"/>
<instance part="C45" gate="G$1" x="-93.98" y="116.84"/>
<instance part="C46" gate="G$1" x="-76.2" y="116.84"/>
<instance part="C47" gate="G$1" x="-127" y="114.3"/>
<instance part="SUPPLY45" gate="GND" x="-43.18" y="109.22"/>
<instance part="SUPPLY46" gate="GND" x="-127" y="106.68"/>
<instance part="SUPPLY47" gate="GND" x="-76.2" y="106.68"/>
<instance part="SUPPLY48" gate="GND" x="-93.98" y="106.68"/>
<instance part="U$15" gate="G$1" x="-58.42" y="-30.48"/>
<instance part="U$16" gate="G$1" x="-55.88" y="-30.48"/>
<instance part="R15" gate="G$1" x="-58.42" y="-35.56" rot="R90"/>
<instance part="R16" gate="G$1" x="-55.88" y="-35.56" rot="R90"/>
<instance part="SUPPLY49" gate="GND" x="-205.74" y="5.08"/>
<instance part="SUPPLY50" gate="GND" x="-172.72" y="63.5" rot="R90"/>
<instance part="SUPPLY51" gate="GND" x="-172.72" y="50.8" rot="R90"/>
<instance part="SUPPLY52" gate="GND" x="-198.12" y="60.96" rot="R270"/>
<instance part="SUPPLY53" gate="GND" x="-198.12" y="48.26" rot="R270"/>
<instance part="CON2" gate="_VCC" x="-147.32" y="121.92" rot="R180"/>
<instance part="CON2" gate="_GND" x="-147.32" y="119.38" rot="R180"/>
<instance part="CON3" gate="_SIGNAL" x="-182.88" y="78.74" rot="R180"/>
<instance part="CON3" gate="_GND" x="-182.88" y="76.2" rot="R180"/>
<instance part="CON4" gate="-1" x="-180.34" y="88.9" rot="R180"/>
<instance part="CON4" gate="-2" x="-180.34" y="86.36" rot="R180"/>
<instance part="CON4" gate="-3" x="-180.34" y="83.82" rot="R180"/>
<instance part="SUPPLY54" gate="GND" x="-175.26" y="71.12"/>
<instance part="CON5" gate="_SIGNAL" x="137.16" y="104.14"/>
<instance part="CON6" gate="_SIGNAL" x="137.16" y="93.98"/>
<instance part="CON6" gate="_GND" x="137.16" y="91.44"/>
<instance part="CON5" gate="_GND" x="137.16" y="101.6"/>
<instance part="SUPPLY55" gate="GND" x="129.54" y="99.06"/>
<instance part="C48" gate="G$1" x="-5.08" y="99.06"/>
<instance part="SUPPLY56" gate="GND" x="-142.24" y="116.84"/>
<instance part="JP1" gate="A" x="251.46" y="55.88"/>
<instance part="SUPPLY57" gate="GND" x="241.3" y="45.72"/>
<instance part="P+1" gate="G$1" x="-127" y="137.16"/>
<instance part="P+2" gate="G$1" x="241.3" y="68.58"/>
<instance part="U$17" gate="1" x="233.68" y="68.58"/>
<instance part="LCD1" gate="G$1" x="-35.56" y="-109.22"/>
<instance part="R17" gate="G$1" x="-60.96" y="-88.9" rot="R90"/>
<instance part="U$18" gate="G$1" x="-60.96" y="-81.28"/>
<instance part="C49" gate="G$1" x="-68.58" y="-121.92"/>
<instance part="C50" gate="G$1" x="-76.2" y="-121.92"/>
<instance part="U$19" gate="G$1" x="-68.58" y="-119.38"/>
<instance part="U$20" gate="G$1" x="-76.2" y="-109.22"/>
<instance part="SUPPLY58" gate="GND" x="-76.2" y="-137.16"/>
<instance part="SUPPLY59" gate="GND" x="-68.58" y="-137.16"/>
<instance part="SUPPLY60" gate="GND" x="-60.96" y="-137.16"/>
<instance part="D1" gate="G$1" x="-160.02" y="63.5" rot="R180"/>
<instance part="CON7" gate="G$1" x="-147.32" y="-93.98" rot="MR0"/>
<instance part="SUPPLY61" gate="GND" x="-149.86" y="-106.68"/>
<instance part="SUPPLY62" gate="GND" x="-139.7" y="-104.14"/>
<instance part="IC9" gate="G$1" x="-20.32" y="-71.12"/>
<instance part="R18" gate="G$1" x="22.86" y="-71.12"/>
<instance part="C51" gate="G$1" x="27.94" y="-76.2"/>
<instance part="C52" gate="G$1" x="15.24" y="-81.28"/>
<instance part="C53" gate="G$1" x="-22.86" y="-83.82"/>
<instance part="U$21" gate="G$1" x="-22.86" y="-68.58"/>
<instance part="U$22" gate="G$1" x="15.24" y="-68.58"/>
<instance part="SUPPLY63" gate="GND" x="-22.86" y="-91.44"/>
<instance part="SUPPLY64" gate="GND" x="15.24" y="-91.44"/>
<instance part="SUPPLY65" gate="GND" x="27.94" y="-83.82"/>
<instance part="SUPPLY66" gate="GND" x="10.16" y="-83.82"/>
<instance part="L1" gate="G$1" x="-86.36" y="121.92" rot="R90"/>
<instance part="L2" gate="G$1" x="17.78" y="-43.18" rot="R90"/>
<instance part="U$1" gate="G$2" x="55.88" y="121.92"/>
<instance part="SUPPLY67" gate="GND" x="55.88" y="109.22"/>
<instance part="U$23" gate="1" x="55.88" y="132.08"/>
<instance part="C54" gate="G$1" x="63.5" y="121.92"/>
<instance part="CON8" gate="_SIGNAL" x="246.38" y="17.78"/>
<instance part="CON8" gate="_GND" x="246.38" y="15.24"/>
<instance part="SUPPLY68" gate="GND" x="241.3" y="12.7"/>
<instance part="C55" gate="G$1" x="12.7" y="-7.62"/>
<instance part="SUPPLY69" gate="GND" x="12.7" y="-15.24"/>
<instance part="C56" gate="G$1" x="12.7" y="50.8"/>
<instance part="SUPPLY70" gate="GND" x="12.7" y="43.18"/>
<instance part="U$24" gate="G$1" x="12.7" y="53.34"/>
<instance part="P+3" gate="G$1" x="-76.2" y="127"/>
<instance part="P+4" gate="G$1" x="-132.08" y="-83.82"/>
<instance part="CON9" gate="G$1" x="-152.4" y="-12.7"/>
<instance part="SUPPLY40" gate="GND" x="-144.78" y="-30.48"/>
<instance part="U$25" gate="G$1" x="-147.32" y="-7.62"/>
<instance part="R19" gate="G$1" x="-144.78" y="-20.32" rot="R90"/>
<instance part="R20" gate="G$1" x="-132.08" y="109.22" rot="R90"/>
<instance part="R21" gate="G$1" x="-132.08" y="96.52" rot="R90"/>
<instance part="SUPPLY71" gate="GND" x="-132.08" y="86.36"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="4"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="MCU" pin="VSSA"/>
<pinref part="SUPPLY3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="AVSS"/>
<pinref part="SUPPLY5" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="GND"/>
<pinref part="SUPPLY12" gate="GND" pin="GND"/>
<wire x1="121.92" y1="83.82" x2="132.08" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY10" gate="GND" pin="GND"/>
<pinref part="IC7" gate="A" pin="GND_3"/>
<wire x1="149.86" y1="-35.56" x2="149.86" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-17.78" x2="147.32" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="GND_2"/>
<wire x1="147.32" y1="-10.16" x2="149.86" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="-10.16" x2="149.86" y2="-17.78" width="0.1524" layer="91"/>
<junction x="149.86" y="-17.78"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="GND"/>
<pinref part="SUPPLY9" gate="GND" pin="GND"/>
<wire x1="101.6" y1="-10.16" x2="96.52" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="96.52" y1="-10.16" x2="96.52" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="96.52" y1="-22.86" x2="96.52" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-40.64" x2="96.52" y2="-40.64" width="0.1524" layer="91"/>
<junction x="96.52" y="-40.64"/>
<pinref part="IC7" gate="A" pin="STBY"/>
<wire x1="101.6" y1="-22.86" x2="96.52" y2="-22.86" width="0.1524" layer="91"/>
<junction x="96.52" y="-22.86"/>
</segment>
<segment>
<pinref part="T1" gate="A" pin="Z"/>
<pinref part="SUPPLY13" gate="GND" pin="GND"/>
<wire x1="177.8" y1="-17.78" x2="175.26" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="175.26" y1="-17.78" x2="175.26" y2="-25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="SUPPLY14" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<wire x1="170.18" y1="-30.48" x2="170.18" y2="-35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="T1" gate="A" pin="E2"/>
<pinref part="SUPPLY16" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC8" gate="A" pin="GNDRF_2"/>
<pinref part="SUPPLY11" gate="GND" pin="GND"/>
<wire x1="170.18" y1="20.32" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
<wire x1="177.8" y1="20.32" x2="177.8" y2="12.7" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="GNDRF"/>
<wire x1="170.18" y1="27.94" x2="177.8" y2="27.94" width="0.1524" layer="91"/>
<wire x1="177.8" y1="27.94" x2="177.8" y2="20.32" width="0.1524" layer="91"/>
<junction x="177.8" y="20.32"/>
<pinref part="IC8" gate="A" pin="GND_7"/>
<wire x1="170.18" y1="33.02" x2="177.8" y2="33.02" width="0.1524" layer="91"/>
<wire x1="177.8" y1="33.02" x2="177.8" y2="27.94" width="0.1524" layer="91"/>
<junction x="177.8" y="27.94"/>
<pinref part="IC8" gate="A" pin="GND_8"/>
<wire x1="170.18" y1="35.56" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<wire x1="177.8" y1="35.56" x2="177.8" y2="33.02" width="0.1524" layer="91"/>
<junction x="177.8" y="33.02"/>
<pinref part="IC8" gate="A" pin="GND"/>
<wire x1="170.18" y1="43.18" x2="177.8" y2="43.18" width="0.1524" layer="91"/>
<wire x1="177.8" y1="43.18" x2="177.8" y2="35.56" width="0.1524" layer="91"/>
<junction x="177.8" y="35.56"/>
<pinref part="IC8" gate="A" pin="EPAD"/>
<wire x1="170.18" y1="48.26" x2="177.8" y2="48.26" width="0.1524" layer="91"/>
<wire x1="177.8" y1="48.26" x2="177.8" y2="43.18" width="0.1524" layer="91"/>
<junction x="177.8" y="43.18"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="SUPPLY17" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C21" gate="G$1" pin="2"/>
<pinref part="SUPPLY18" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC8" gate="A" pin="GND_2"/>
<pinref part="SUPPLY19" gate="GND" pin="GND"/>
<wire x1="109.22" y1="45.72" x2="106.68" y2="45.72" width="0.1524" layer="91"/>
<wire x1="106.68" y1="45.72" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="GND_3"/>
<wire x1="106.68" y1="38.1" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<wire x1="106.68" y1="30.48" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<wire x1="106.68" y1="22.86" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<wire x1="106.68" y1="20.32" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<wire x1="109.22" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<junction x="106.68" y="38.1"/>
<pinref part="IC8" gate="A" pin="GND_4"/>
<wire x1="109.22" y1="30.48" x2="106.68" y2="30.48" width="0.1524" layer="91"/>
<junction x="106.68" y="30.48"/>
<pinref part="IC8" gate="A" pin="GND_5"/>
<wire x1="109.22" y1="22.86" x2="106.68" y2="22.86" width="0.1524" layer="91"/>
<junction x="106.68" y="22.86"/>
<pinref part="IC8" gate="A" pin="GND_6"/>
<wire x1="109.22" y1="20.32" x2="106.68" y2="20.32" width="0.1524" layer="91"/>
<junction x="106.68" y="20.32"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="SUPPLY20" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="GND"/>
<pinref part="SUPPLY21" gate="GND" pin="GND"/>
<wire x1="198.12" y1="17.78" x2="193.04" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C29" gate="G$1" pin="2"/>
<pinref part="SUPPLY22" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="SUPPLY23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="SUPPLY24" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="SUPPLY25" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="SUPPLY26" gate="GND" pin="GND"/>
<wire x1="93.98" y1="30.48" x2="93.98" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUPPLY4" gate="GND" pin="GND"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="10.16" y1="-58.42" x2="10.16" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="5.08" y1="-40.64" x2="7.62" y2="-40.64" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-40.64" x2="7.62" y2="-55.88" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-55.88" x2="10.16" y2="-55.88" width="0.1524" layer="91"/>
<junction x="10.16" y="-55.88"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="SUPPLY27" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="4"/>
<pinref part="SUPPLY28" gate="GND" pin="GND"/>
<wire x1="-38.1" y1="-38.1" x2="-40.64" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="2"/>
<wire x1="-38.1" y1="-35.56" x2="-38.1" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-38.1" y="-38.1"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="IOVSS"/>
<pinref part="SUPPLY8" gate="GND" pin="GND"/>
<wire x1="-38.1" y1="17.78" x2="-43.18" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="DVSS"/>
<pinref part="SUPPLY7" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="EP"/>
<pinref part="SUPPLY6" gate="GND" pin="GND"/>
<wire x1="10.16" y1="33.02" x2="10.16" y2="35.56" width="0.1524" layer="91"/>
<wire x1="10.16" y1="35.56" x2="15.24" y2="35.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C14" gate="G$1" pin="2"/>
<pinref part="SUPPLY29" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="SUPPLY30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="SUPPLY31" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="SUPPLY32" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C38" gate="G$1" pin="2"/>
<pinref part="SUPPLY33" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C37" gate="G$1" pin="2"/>
<pinref part="SUPPLY34" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC1" gate="PWR" pin="VSS@1"/>
<pinref part="SUPPLY35" gate="GND" pin="GND"/>
<pinref part="C39" gate="G$1" pin="2"/>
<wire x1="-121.92" y1="-45.72" x2="-121.92" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-121.92" y="-60.96"/>
</segment>
<segment>
<pinref part="IC1" gate="PWR" pin="VSS@2"/>
<pinref part="SUPPLY36" gate="GND" pin="GND"/>
<pinref part="C40" gate="G$1" pin="2"/>
<wire x1="-116.84" y1="-45.72" x2="-116.84" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-116.84" y="-60.96"/>
</segment>
<segment>
<pinref part="IC1" gate="PWR" pin="VSS@3"/>
<pinref part="SUPPLY37" gate="GND" pin="GND"/>
<pinref part="C41" gate="G$1" pin="2"/>
<wire x1="-111.76" y1="-45.72" x2="-111.76" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-111.76" y="-60.96"/>
</segment>
<segment>
<pinref part="IC1" gate="PWR" pin="VSS@4"/>
<pinref part="SUPPLY38" gate="GND" pin="GND"/>
<pinref part="C42" gate="G$1" pin="2"/>
<wire x1="-106.68" y1="-45.72" x2="-106.68" y2="-60.96" width="0.1524" layer="91"/>
<junction x="-106.68" y="-60.96"/>
</segment>
<segment>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="SUPPLY39" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="SUPPLY41" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C10" gate="G$1" pin="2"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="GND"/>
<pinref part="SUPPLY43" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC6" gate="G$1" pin="GND"/>
<pinref part="SUPPLY44" gate="GND" pin="GND"/>
<wire x1="-111.76" y1="111.76" x2="-111.76" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="SUPPLY45" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C47" gate="G$1" pin="2"/>
<pinref part="SUPPLY46" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C46" gate="G$1" pin="2"/>
<pinref part="SUPPLY47" gate="GND" pin="GND"/>
<wire x1="-76.2" y1="109.22" x2="-76.2" y2="111.76" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C45" gate="G$1" pin="2"/>
<pinref part="SUPPLY48" gate="GND" pin="GND"/>
<wire x1="-93.98" y1="111.76" x2="-93.98" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="COM_1"/>
<pinref part="S2" gate="G$1" pin="COM_2"/>
<wire x1="-205.74" y1="33.02" x2="-205.74" y2="30.48" width="0.1524" layer="91"/>
<pinref part="S3" gate="G$1" pin="COM_1"/>
<wire x1="-205.74" y1="30.48" x2="-205.74" y2="22.86" width="0.1524" layer="91"/>
<junction x="-205.74" y="30.48"/>
<pinref part="S3" gate="G$1" pin="COM_2"/>
<wire x1="-205.74" y1="22.86" x2="-205.74" y2="20.32" width="0.1524" layer="91"/>
<junction x="-205.74" y="22.86"/>
<pinref part="S4" gate="G$1" pin="COM_1"/>
<wire x1="-205.74" y1="20.32" x2="-205.74" y2="12.7" width="0.1524" layer="91"/>
<junction x="-205.74" y="20.32"/>
<pinref part="S4" gate="G$1" pin="COM_2"/>
<wire x1="-205.74" y1="12.7" x2="-205.74" y2="10.16" width="0.1524" layer="91"/>
<junction x="-205.74" y="12.7"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
<wire x1="-205.74" y1="7.62" x2="-205.74" y2="10.16" width="0.1524" layer="91"/>
<junction x="-205.74" y="10.16"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="C"/>
<pinref part="SUPPLY50" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="C"/>
<pinref part="SUPPLY51" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="2"/>
<pinref part="SUPPLY52" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="2"/>
<pinref part="SUPPLY53" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON4" gate="-1" pin="S"/>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
<wire x1="-177.8" y1="88.9" x2="-175.26" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-175.26" y1="88.9" x2="-175.26" y2="76.2" width="0.1524" layer="91"/>
<pinref part="CON3" gate="_GND" pin="S"/>
<wire x1="-175.26" y1="76.2" x2="-175.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="76.2" x2="-175.26" y2="76.2" width="0.1524" layer="91"/>
<junction x="-175.26" y="76.2"/>
</segment>
<segment>
<pinref part="CON5" gate="_GND" pin="S"/>
<pinref part="SUPPLY55" gate="GND" pin="GND"/>
<wire x1="132.08" y1="101.6" x2="129.54" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="CON2" gate="_GND" pin="S"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="7"/>
<pinref part="SUPPLY57" gate="GND" pin="GND"/>
<wire x1="248.92" y1="48.26" x2="241.3" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="2"/>
<pinref part="SUPPLY58" gate="GND" pin="GND"/>
<wire x1="-76.2" y1="-134.62" x2="-76.2" y2="-127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="2"/>
<pinref part="SUPPLY59" gate="GND" pin="GND"/>
<wire x1="-68.58" y1="-134.62" x2="-68.58" y2="-127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LCD1" gate="G$1" pin="LED-"/>
<wire x1="-58.42" y1="-116.84" x2="-60.96" y2="-116.84" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-116.84" x2="-60.96" y2="-121.92" width="0.1524" layer="91"/>
<pinref part="SUPPLY60" gate="GND" pin="GND"/>
<wire x1="-60.96" y1="-121.92" x2="-60.96" y2="-124.46" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="GND@2"/>
<wire x1="-60.96" y1="-124.46" x2="-60.96" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-121.92" x2="-60.96" y2="-121.92" width="0.1524" layer="91"/>
<junction x="-60.96" y="-121.92"/>
<pinref part="LCD1" gate="G$1" pin="GND@3"/>
<wire x1="-58.42" y1="-124.46" x2="-60.96" y2="-124.46" width="0.1524" layer="91"/>
<junction x="-60.96" y="-124.46"/>
<pinref part="LCD1" gate="G$1" pin="GND@1"/>
<wire x1="-58.42" y1="-114.3" x2="-60.96" y2="-114.3" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-114.3" x2="-60.96" y2="-116.84" width="0.1524" layer="91"/>
<junction x="-60.96" y="-116.84"/>
</segment>
<segment>
<pinref part="CON7" gate="G$1" pin="SHELL"/>
<pinref part="SUPPLY61" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="CON7" gate="G$1" pin="5"/>
<pinref part="SUPPLY62" gate="GND" pin="GND"/>
<wire x1="-142.24" y1="-99.06" x2="-139.7" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="-99.06" x2="-139.7" y2="-101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C53" gate="G$1" pin="2"/>
<pinref part="SUPPLY63" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="SUPPLY64" gate="GND" pin="GND"/>
<wire x1="15.24" y1="-86.36" x2="15.24" y2="-88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C51" gate="G$1" pin="2"/>
<pinref part="SUPPLY65" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC9" gate="G$1" pin="VSS"/>
<pinref part="SUPPLY66" gate="GND" pin="GND"/>
<wire x1="7.62" y1="-73.66" x2="10.16" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-73.66" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="IC9" gate="G$1" pin="!LDAC"/>
<wire x1="10.16" y1="-78.74" x2="10.16" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="7.62" y1="-78.74" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<junction x="10.16" y="-78.74"/>
</segment>
<segment>
<pinref part="U$1" gate="G$2" pin="V-"/>
<pinref part="SUPPLY67" gate="GND" pin="GND"/>
<wire x1="55.88" y1="111.76" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="2"/>
<wire x1="63.5" y1="116.84" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="55.88" y2="114.3" width="0.1524" layer="91"/>
<junction x="55.88" y="114.3"/>
</segment>
<segment>
<pinref part="CON8" gate="_GND" pin="S"/>
<pinref part="SUPPLY68" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<pinref part="SUPPLY69" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="2"/>
<pinref part="SUPPLY70" gate="GND" pin="GND"/>
<pinref part="C43" gate="G$1" pin="2"/>
<wire x1="17.78" y1="45.72" x2="12.7" y2="45.72" width="0.1524" layer="91"/>
<junction x="12.7" y="45.72"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="SUPPLY40" gate="GND" pin="GND"/>
<wire x1="-144.78" y1="-27.94" x2="-144.78" y2="-25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="SUPPLY71" gate="GND" pin="GND"/>
<wire x1="-132.08" y1="91.44" x2="-132.08" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC7" gate="A" pin="IF+"/>
<pinref part="T1" gate="A" pin="A1"/>
<wire x1="147.32" y1="-12.7" x2="177.8" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="IC7" gate="A" pin="IF-"/>
<wire x1="147.32" y1="-15.24" x2="172.72" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="172.72" y1="-15.24" x2="172.72" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="T1" gate="A" pin="E1"/>
<wire x1="172.72" y1="-22.86" x2="177.8" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="U$11" gate="1" pin="+5V"/>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="IC7" gate="A" pin="VCC"/>
<wire x1="147.32" y1="-27.94" x2="165.1" y2="-27.94" width="0.1524" layer="91"/>
<junction x="165.1" y="-27.94"/>
<wire x1="165.1" y1="-27.94" x2="165.1" y2="-25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC7" gate="A" pin="VCC_2"/>
<pinref part="U$4" gate="1" pin="+5V"/>
<wire x1="147.32" y1="-20.32" x2="170.18" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C1" gate="G$1" pin="1"/>
<wire x1="170.18" y1="-22.86" x2="170.18" y2="-20.32" width="0.1524" layer="91"/>
<junction x="170.18" y="-20.32"/>
</segment>
<segment>
<pinref part="IC8" gate="A" pin="VCC1"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="170.18" y1="45.72" x2="182.88" y2="45.72" width="0.1524" layer="91"/>
<pinref part="U$3" gate="1" pin="+5V"/>
<wire x1="182.88" y1="45.72" x2="190.5" y2="45.72" width="0.1524" layer="91"/>
<wire x1="182.88" y1="48.26" x2="182.88" y2="45.72" width="0.1524" layer="91"/>
<junction x="182.88" y="45.72"/>
<pinref part="IC8" gate="A" pin="VCC2"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="170.18" y1="30.48" x2="182.88" y2="30.48" width="0.1524" layer="91"/>
<wire x1="182.88" y1="30.48" x2="182.88" y2="20.32" width="0.1524" layer="91"/>
<wire x1="182.88" y1="45.72" x2="182.88" y2="30.48" width="0.1524" layer="91"/>
<junction x="182.88" y="30.48"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="VDD"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="121.92" y1="86.36" x2="137.16" y2="86.36" width="0.1524" layer="91"/>
<pinref part="U$14" gate="1" pin="+5V"/>
<junction x="137.16" y="86.36"/>
</segment>
<segment>
<pinref part="U$2" gate="1" pin="+5V"/>
<pinref part="C45" gate="G$1" pin="1"/>
<wire x1="-93.98" y1="121.92" x2="-93.98" y2="119.38" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="OUT@1"/>
<wire x1="-93.98" y1="121.92" x2="-99.06" y2="121.92" width="0.1524" layer="91"/>
<junction x="-93.98" y="121.92"/>
<pinref part="IC6" gate="G$1" pin="OUT@2"/>
<wire x1="-99.06" y1="121.92" x2="-99.06" y2="119.38" width="0.1524" layer="91"/>
<junction x="-99.06" y="121.92"/>
<pinref part="L1" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="121.92" x2="-93.98" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="2"/>
<pinref part="U$17" gate="1" pin="+5V"/>
<wire x1="248.92" y1="60.96" x2="233.68" y2="60.96" width="0.1524" layer="91"/>
<wire x1="233.68" y1="60.96" x2="233.68" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$2" pin="V+"/>
<pinref part="U$23" gate="1" pin="+5V"/>
<wire x1="55.88" y1="132.08" x2="55.88" y2="129.54" width="0.1524" layer="91"/>
<pinref part="C54" gate="G$1" pin="1"/>
<wire x1="63.5" y1="124.46" x2="63.5" y2="129.54" width="0.1524" layer="91"/>
<wire x1="63.5" y1="129.54" x2="55.88" y2="129.54" width="0.1524" layer="91"/>
<junction x="55.88" y="129.54"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="1"/>
<pinref part="T1" gate="A" pin="A2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="193.04" y1="-15.24" x2="195.58" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="195.58" y1="-15.24" x2="195.58" y2="15.24" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="RF2"/>
<wire x1="195.58" y1="15.24" x2="198.12" y2="15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="RF1"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="198.12" y1="20.32" x2="198.12" y2="25.4" width="0.1524" layer="91"/>
<wire x1="198.12" y1="25.4" x2="193.04" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC8" gate="A" pin="RF"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="170.18" y1="25.4" x2="185.42" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="IC7" gate="A" pin="2XLO-"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="101.6" y1="-27.94" x2="99.06" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-27.94" x2="99.06" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="IC7" gate="A" pin="2XLO+"/>
<wire x1="101.6" y1="-25.4" x2="88.9" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="88.9" y1="-25.4" x2="88.9" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="88.9" y1="-40.64" x2="81.28" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="78.74" y1="-33.02" x2="86.36" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="86.36" y1="-33.02" x2="86.36" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="QOUT-"/>
<wire x1="86.36" y1="-20.32" x2="101.6" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="IC7" gate="A" pin="QOUT+"/>
<wire x1="101.6" y1="-17.78" x2="83.82" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-17.78" x2="83.82" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<wire x1="83.82" y1="-25.4" x2="78.74" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC7" gate="A" pin="IOUT-"/>
<wire x1="101.6" y1="-15.24" x2="81.28" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="81.28" y1="-15.24" x2="81.28" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="81.28" y1="-20.32" x2="78.74" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="IC7" gate="A" pin="IOUT+"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="101.6" y1="-12.7" x2="78.74" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="IC7" gate="A" pin="IFDET"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="147.32" y1="-25.4" x2="160.02" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="160.02" y1="-25.4" x2="160.02" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="RFC"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="231.14" y1="17.78" x2="223.52" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="101.6" y1="30.48" x2="104.14" y2="30.48" width="0.1524" layer="91"/>
<wire x1="104.14" y1="30.48" x2="104.14" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="CAPB"/>
<wire x1="104.14" y1="33.02" x2="109.22" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="101.6" y1="38.1" x2="104.14" y2="38.1" width="0.1524" layer="91"/>
<wire x1="104.14" y1="38.1" x2="104.14" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="CAPA"/>
<wire x1="104.14" y1="35.56" x2="109.22" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="U$1" gate="G$5" pin="OUT"/>
<wire x1="68.58" y1="5.08" x2="76.2" y2="5.08" width="0.1524" layer="91"/>
<wire x1="76.2" y1="5.08" x2="175.26" y2="5.08" width="0.1524" layer="91"/>
<wire x1="175.26" y1="5.08" x2="175.26" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="BBMI"/>
<wire x1="175.26" y1="40.64" x2="170.18" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="63.5" y1="-2.54" x2="76.2" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="76.2" y1="-2.54" x2="76.2" y2="5.08" width="0.1524" layer="91"/>
<junction x="76.2" y="5.08"/>
<pinref part="C30" gate="G$1" pin="2"/>
<wire x1="60.96" y1="-7.62" x2="63.5" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="63.5" y1="-7.62" x2="63.5" y2="-2.54" width="0.1524" layer="91"/>
<junction x="63.5" y="-2.54"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="IC8" gate="A" pin="BBPI"/>
<wire x1="170.18" y1="38.1" x2="172.72" y2="38.1" width="0.1524" layer="91"/>
<wire x1="172.72" y1="38.1" x2="172.72" y2="7.62" width="0.1524" layer="91"/>
<wire x1="172.72" y1="7.62" x2="76.2" y2="7.62" width="0.1524" layer="91"/>
<wire x1="76.2" y1="7.62" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$4" pin="OUT"/>
<wire x1="76.2" y1="12.7" x2="76.2" y2="17.78" width="0.1524" layer="91"/>
<wire x1="76.2" y1="17.78" x2="68.58" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$4" pin="-IN"/>
<wire x1="53.34" y1="15.24" x2="53.34" y2="12.7" width="0.1524" layer="91"/>
<wire x1="53.34" y1="12.7" x2="76.2" y2="12.7" width="0.1524" layer="91"/>
<junction x="76.2" y="12.7"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="U$1" gate="G$3" pin="OUT"/>
<wire x1="68.58" y1="40.64" x2="68.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="68.58" y1="33.02" x2="68.58" y2="27.94" width="0.1524" layer="91"/>
<wire x1="68.58" y1="27.94" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="BBMQ"/>
<wire x1="76.2" y1="27.94" x2="109.22" y2="27.94" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="63.5" y1="33.02" x2="68.58" y2="33.02" width="0.1524" layer="91"/>
<junction x="68.58" y="33.02"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="60.96" y1="27.94" x2="68.58" y2="27.94" width="0.1524" layer="91"/>
<junction x="68.58" y="27.94"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="IC8" gate="A" pin="BBPQ"/>
<wire x1="109.22" y1="25.4" x2="71.12" y2="25.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="25.4" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="OUT"/>
<wire x1="71.12" y1="50.8" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="71.12" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="-IN"/>
<wire x1="53.34" y1="53.34" x2="53.34" y2="50.8" width="0.1524" layer="91"/>
<wire x1="53.34" y1="50.8" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<junction x="71.12" y="50.8"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="U$1" gate="G$5" pin="-IN"/>
<wire x1="53.34" y1="-2.54" x2="53.34" y2="2.54" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="48.26" y1="2.54" x2="53.34" y2="2.54" width="0.1524" layer="91"/>
<junction x="53.34" y="2.54"/>
<pinref part="C30" gate="G$1" pin="1"/>
<wire x1="53.34" y1="-7.62" x2="53.34" y2="-2.54" width="0.1524" layer="91"/>
<junction x="53.34" y="-2.54"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$4" pin="+IN"/>
<wire x1="45.72" y1="20.32" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C29" gate="G$1" pin="1"/>
<wire x1="48.26" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<wire x1="48.26" y1="17.78" x2="48.26" y2="20.32" width="0.1524" layer="91"/>
<junction x="48.26" y="20.32"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$3" pin="-IN"/>
<wire x1="48.26" y1="38.1" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="53.34" y1="33.02" x2="53.34" y2="38.1" width="0.1524" layer="91"/>
<junction x="53.34" y="38.1"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="53.34" y1="27.94" x2="53.34" y2="33.02" width="0.1524" layer="91"/>
<junction x="53.34" y="33.02"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<pinref part="U$1" gate="G$1" pin="+IN"/>
<wire x1="48.26" y1="58.42" x2="53.34" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="48.26" y1="55.88" x2="48.26" y2="58.42" width="0.1524" layer="91"/>
<junction x="48.26" y="58.42"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="1"/>
<pinref part="R2" gate="G$1" pin="1"/>
<wire x1="38.1" y1="58.42" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="33.02" y1="38.1" x2="38.1" y2="38.1" width="0.1524" layer="91"/>
<junction x="38.1" y="38.1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="35.56" y1="20.32" x2="35.56" y2="2.54" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="35.56" y1="2.54" x2="38.1" y2="2.54" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<junction x="35.56" y="2.54"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="25.4" y1="2.54" x2="27.94" y2="2.54" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="LOR"/>
<wire x1="10.16" y1="7.62" x2="25.4" y2="7.62" width="0.1524" layer="91"/>
<wire x1="25.4" y1="7.62" x2="25.4" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="33.02" y1="5.08" x2="33.02" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="LOL"/>
<wire x1="33.02" y1="5.08" x2="10.16" y2="5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="REF"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="10.16" y1="-5.08" x2="12.7" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="C55" gate="G$1" pin="1"/>
<wire x1="12.7" y1="-5.08" x2="15.24" y2="-5.08" width="0.1524" layer="91"/>
<junction x="12.7" y="-5.08"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="25.4" y1="-5.08" x2="27.94" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$5" pin="+IN"/>
<wire x1="53.34" y1="7.62" x2="50.8" y2="7.62" width="0.1524" layer="91"/>
<wire x1="50.8" y1="7.62" x2="50.8" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$3" pin="+IN"/>
<wire x1="50.8" y1="43.18" x2="53.34" y2="43.18" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-5.08" x2="33.02" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="33.02" y1="-5.08" x2="50.8" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-5.08" x2="50.8" y2="7.62" width="0.1524" layer="91"/>
<junction x="27.94" y="-5.08"/>
<junction x="50.8" y="7.62"/>
<pinref part="C31" gate="G$1" pin="1"/>
<junction x="33.02" y="-5.08"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="C32" gate="G$1" pin="1"/>
<pinref part="R9" gate="G$1" pin="2"/>
<wire x1="93.98" y1="38.1" x2="93.98" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="IC8" gate="A" pin="LOM"/>
<wire x1="104.14" y1="40.64" x2="109.22" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="IC8" gate="A" pin="LOP"/>
<wire x1="93.98" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="68.58" y1="-35.56" x2="68.58" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-7.62" x2="68.58" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-5.08" x2="83.82" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="C33" gate="G$1" pin="1"/>
<wire x1="83.82" y1="-5.08" x2="83.82" y2="43.18" width="0.1524" layer="91"/>
<wire x1="83.82" y1="43.18" x2="86.36" y2="43.18" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="CLK0"/>
<wire x1="5.08" y1="-35.56" x2="68.58" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="R10" gate="G$1" pin="2"/>
<pinref part="C4" gate="G$1" pin="1"/>
<wire x1="66.04" y1="-12.7" x2="71.12" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="R11" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="66.04" y1="-20.32" x2="71.12" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="C6" gate="G$1" pin="1"/>
<wire x1="66.04" y1="-25.4" x2="71.12" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="66.04" y1="-33.02" x2="71.12" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-25.4" x2="55.88" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-25.4" x2="-48.26" y2="-25.4" width="0.1524" layer="91"/>
<junction x="50.8" y="-25.4"/>
<pinref part="IC3" gate="G$1" pin="IN2_L"/>
<wire x1="-38.1" y1="-2.54" x2="-48.26" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="-2.54" x2="-48.26" y2="-25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-33.02" x2="55.88" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-33.02" x2="43.18" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-33.02" x2="43.18" y2="-27.94" width="0.1524" layer="91"/>
<junction x="50.8" y="-33.02"/>
<wire x1="43.18" y1="-27.94" x2="-50.8" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="IN2_R"/>
<wire x1="-38.1" y1="-5.08" x2="-50.8" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="-5.08" x2="-50.8" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="R11" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-20.32" x2="55.88" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-20.32" x2="43.18" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-20.32" x2="43.18" y2="-22.86" width="0.1524" layer="91"/>
<junction x="50.8" y="-20.32"/>
<wire x1="43.18" y1="-22.86" x2="-45.72" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="IN1_L"/>
<wire x1="-45.72" y1="-22.86" x2="-45.72" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-45.72" y1="2.54" x2="-38.1" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="R10" gate="G$1" pin="1"/>
<wire x1="50.8" y1="-12.7" x2="55.88" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="50.8" y1="-12.7" x2="40.64" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-12.7" x2="40.64" y2="-20.32" width="0.1524" layer="91"/>
<junction x="50.8" y="-12.7"/>
<wire x1="40.64" y1="-20.32" x2="-43.18" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="IN1_R"/>
<wire x1="-38.1" y1="0" x2="-43.18" y2="0" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="0" x2="-43.18" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="VDD"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-35.56" x2="-22.86" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-35.56" x2="-22.86" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="U$10" gate="G$1" pin="+3V3"/>
<wire x1="-22.86" y1="-33.02" x2="-22.86" y2="-35.56" width="0.1524" layer="91"/>
<junction x="-22.86" y="-35.56"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="+3V3"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="LDO_SELECT"/>
<wire x1="10.16" y1="25.4" x2="20.32" y2="25.4" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$7" gate="G$1" pin="+3V3"/>
<pinref part="IC3" gate="G$1" pin="OVIDD"/>
<wire x1="-40.64" y1="35.56" x2="-40.64" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="20.32" x2="-38.1" y2="20.32" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="-45.72" y1="20.32" x2="-40.64" y2="20.32" width="0.1524" layer="91"/>
<junction x="-40.64" y="20.32"/>
<pinref part="IC3" gate="G$1" pin="SPI_SELECT"/>
<wire x1="-38.1" y1="5.08" x2="-40.64" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="5.08" x2="-40.64" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="MCU" pin="VDDA"/>
<pinref part="IC1" gate="MCU" pin="VBAT"/>
<wire x1="-129.54" y1="7.62" x2="-129.54" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U$6" gate="G$1" pin="+3V3"/>
<wire x1="-134.62" y1="10.16" x2="-129.54" y2="10.16" width="0.1524" layer="91"/>
<junction x="-129.54" y="10.16"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="-137.16" y1="5.08" x2="-137.16" y2="10.16" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="10.16" x2="-134.62" y2="10.16" width="0.1524" layer="91"/>
<junction x="-134.62" y="10.16"/>
</segment>
<segment>
<pinref part="IC1" gate="PWR" pin="VDD@1"/>
<pinref part="U$13" gate="G$1" pin="+3V3"/>
<pinref part="IC1" gate="PWR" pin="VDD@2"/>
<wire x1="-121.92" y1="-25.4" x2="-116.84" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-121.92" y="-25.4"/>
<pinref part="IC1" gate="PWR" pin="VDD@3"/>
<wire x1="-116.84" y1="-25.4" x2="-111.76" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-116.84" y="-25.4"/>
<pinref part="IC1" gate="PWR" pin="VDD@4"/>
<wire x1="-111.76" y1="-25.4" x2="-106.68" y2="-25.4" width="0.1524" layer="91"/>
<junction x="-111.76" y="-25.4"/>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="-121.92" y1="-25.4" x2="-121.92" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="-116.84" y1="-25.4" x2="-116.84" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="C41" gate="G$1" pin="1"/>
<wire x1="-111.76" y1="-25.4" x2="-111.76" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<wire x1="-106.68" y1="-25.4" x2="-106.68" y2="-38.1" width="0.1524" layer="91"/>
<junction x="-106.68" y="-25.4"/>
</segment>
<segment>
<pinref part="IC5" gate="G$1" pin="OUT@2"/>
<pinref part="C44" gate="G$1" pin="1"/>
<wire x1="-48.26" y1="119.38" x2="-43.18" y2="119.38" width="0.1524" layer="91"/>
<pinref part="IC5" gate="G$1" pin="OUT@1"/>
<pinref part="U$5" gate="G$1" pin="+3V3"/>
<wire x1="-48.26" y1="121.92" x2="-43.18" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="121.92" x2="-40.64" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-40.64" y1="121.92" x2="-40.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="-43.18" y1="119.38" x2="-43.18" y2="121.92" width="0.1524" layer="91"/>
<junction x="-43.18" y="119.38"/>
<junction x="-43.18" y="121.92"/>
</segment>
<segment>
<pinref part="U$15" gate="G$1" pin="+3V3"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U$16" gate="G$1" pin="+3V3"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="2"/>
<pinref part="U$18" gate="G$1" pin="+3V3"/>
<wire x1="-60.96" y1="-83.82" x2="-60.96" y2="-81.28" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C49" gate="G$1" pin="1"/>
<pinref part="U$19" gate="G$1" pin="+3V3"/>
<pinref part="LCD1" gate="G$1" pin="VCCIO"/>
<wire x1="-58.42" y1="-119.38" x2="-68.58" y2="-119.38" width="0.1524" layer="91"/>
<junction x="-68.58" y="-119.38"/>
</segment>
<segment>
<pinref part="C50" gate="G$1" pin="1"/>
<pinref part="U$20" gate="G$1" pin="+3V3"/>
<wire x1="-76.2" y1="-109.22" x2="-76.2" y2="-119.38" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="VCC"/>
<wire x1="-58.42" y1="-109.22" x2="-76.2" y2="-109.22" width="0.1524" layer="91"/>
<junction x="-76.2" y="-109.22"/>
</segment>
<segment>
<pinref part="IC9" gate="G$1" pin="VDD"/>
<pinref part="C53" gate="G$1" pin="1"/>
<wire x1="-20.32" y1="-71.12" x2="-22.86" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-71.12" x2="-22.86" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="U$21" gate="G$1" pin="+3V3"/>
<wire x1="-22.86" y1="-68.58" x2="-22.86" y2="-71.12" width="0.1524" layer="91"/>
<junction x="-22.86" y="-71.12"/>
</segment>
<segment>
<pinref part="IC9" gate="G$1" pin="VREF"/>
<pinref part="C52" gate="G$1" pin="1"/>
<wire x1="7.62" y1="-76.2" x2="15.24" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-76.2" x2="15.24" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="U$22" gate="G$1" pin="+3V3"/>
<wire x1="15.24" y1="-68.58" x2="15.24" y2="-76.2" width="0.1524" layer="91"/>
<junction x="15.24" y="-76.2"/>
</segment>
<segment>
<pinref part="L2" gate="G$1" pin="2"/>
<pinref part="U$9" gate="G$1" pin="+3V3"/>
<wire x1="22.86" y1="-43.18" x2="25.4" y2="-43.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="1"/>
<pinref part="U$24" gate="G$1" pin="+3V3"/>
<pinref part="C43" gate="G$1" pin="1"/>
<wire x1="17.78" y1="53.34" x2="12.7" y2="53.34" width="0.1524" layer="91"/>
<junction x="12.7" y="53.34"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="LDOIN"/>
<pinref part="U$12" gate="G$1" pin="+3V3"/>
<wire x1="10.16" y1="15.24" x2="15.24" y2="15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$25" gate="G$1" pin="+3V3"/>
<pinref part="CON9" gate="G$1" pin="P$1"/>
<wire x1="-147.32" y1="-7.62" x2="-147.32" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="XB"/>
<wire x1="-27.94" y1="-40.64" x2="-20.32" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="3"/>
<wire x1="-33.02" y1="-40.64" x2="-27.94" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="XA"/>
<wire x1="-20.32" y1="-38.1" x2="-27.94" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="-38.1" x2="-27.94" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="1"/>
<wire x1="-27.94" y1="-33.02" x2="-33.02" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="VCAP1"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="-129.54" y1="-10.16" x2="-134.62" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="-10.16" x2="-134.62" y2="-12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="3"/>
<pinref part="IC1" gate="MCU" pin="OSC_OUT"/>
<wire x1="-144.78" y1="15.24" x2="-142.24" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C38" gate="G$1" pin="1"/>
<wire x1="-142.24" y1="15.24" x2="-129.54" y2="15.24" width="0.1524" layer="91"/>
<junction x="-142.24" y="15.24"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="OSC_IN"/>
<pinref part="X1" gate="G$1" pin="1"/>
<wire x1="-129.54" y1="17.78" x2="-152.4" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="17.78" x2="-152.4" y2="15.24" width="0.1524" layer="91"/>
<pinref part="C37" gate="G$1" pin="1"/>
<wire x1="-157.48" y1="15.24" x2="-157.48" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-157.48" y1="17.78" x2="-152.4" y2="17.78" width="0.1524" layer="91"/>
<junction x="-152.4" y="17.78"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA10"/>
<pinref part="CON1" gate="_RX" pin="S"/>
<wire x1="-83.82" y1="15.24" x2="-93.98" y2="15.24" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="15.24" x2="-93.98" y2="-96.52" width="0.1524" layer="91"/>
<junction x="-83.82" y="15.24"/>
<pinref part="CON7" gate="G$1" pin="4"/>
<wire x1="-93.98" y1="-96.52" x2="-142.24" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA5"/>
<wire x1="-83.82" y1="2.54" x2="-73.66" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="2.54" x2="-60.96" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="2.54" x2="-60.96" y2="15.24" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="SCLK/MFP3"/>
<wire x1="-60.96" y1="15.24" x2="-38.1" y2="15.24" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="SCK"/>
<wire x1="-58.42" y1="-106.68" x2="-73.66" y2="-106.68" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="-106.68" x2="-73.66" y2="-76.2" width="0.1524" layer="91"/>
<junction x="-73.66" y="2.54"/>
<pinref part="IC9" gate="G$1" pin="SCK"/>
<wire x1="-73.66" y1="-76.2" x2="-73.66" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-76.2" x2="-73.66" y2="-76.2" width="0.1524" layer="91"/>
<junction x="-73.66" y="-76.2"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA6"/>
<wire x1="-83.82" y1="5.08" x2="-55.88" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="5.08" x2="-55.88" y2="10.16" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="SDA/MOSI"/>
<wire x1="-55.88" y1="10.16" x2="-38.1" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA7"/>
<pinref part="IC3" gate="G$1" pin="MISO/MFP4"/>
<wire x1="-83.82" y1="7.62" x2="-71.12" y2="7.62" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="SDA"/>
<wire x1="-71.12" y1="7.62" x2="-38.1" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-104.14" x2="-71.12" y2="-104.14" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="-104.14" x2="-71.12" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-71.12" y="7.62"/>
<pinref part="IC9" gate="G$1" pin="SDI"/>
<wire x1="-71.12" y1="-78.74" x2="-71.12" y2="7.62" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="-78.74" x2="-71.12" y2="-78.74" width="0.1524" layer="91"/>
<junction x="-71.12" y="-78.74"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA4"/>
<wire x1="-83.82" y1="0" x2="-58.42" y2="0" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="SCL/SSZ"/>
<wire x1="-58.42" y1="0" x2="-58.42" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="12.7" x2="-38.1" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="~RESET"/>
<wire x1="10.16" y1="27.94" x2="-27.94" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="27.94" x2="-27.94" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="-15.24" x2="-53.34" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="-15.24" x2="-53.34" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PA3"/>
<wire x1="-53.34" y1="-2.54" x2="-68.58" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="LCD1" gate="G$1" pin="!RST"/>
<wire x1="-68.58" y1="-2.54" x2="-83.82" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-2.54" x2="-68.58" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="-99.06" x2="-58.42" y2="-99.06" width="0.1524" layer="91"/>
<junction x="-68.58" y="-2.54"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="C14" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="DVDD"/>
<wire x1="10.16" y1="22.86" x2="20.32" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB14"/>
<wire x1="-83.82" y1="66.04" x2="-58.42" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="66.04" x2="-58.42" y2="22.86" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="DOUT/MFP2"/>
<wire x1="-58.42" y1="22.86" x2="-38.1" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="IC3" gate="G$1" pin="DIN/MFP1"/>
<wire x1="-38.1" y1="25.4" x2="-55.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="25.4" x2="-55.88" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PB15"/>
<wire x1="-55.88" y1="68.58" x2="-83.82" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB12"/>
<wire x1="-83.82" y1="60.96" x2="-53.34" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="60.96" x2="-53.34" y2="27.94" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="WCLK"/>
<wire x1="-53.34" y1="27.94" x2="-38.1" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB13"/>
<wire x1="-83.82" y1="63.5" x2="-50.8" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-50.8" y1="63.5" x2="-50.8" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="BCLK"/>
<wire x1="-50.8" y1="30.48" x2="-38.1" y2="30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PC6"/>
<wire x1="-129.54" y1="45.72" x2="-134.62" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="45.72" x2="-134.62" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-134.62" y1="73.66" x2="-48.26" y2="73.66" width="0.1524" layer="91"/>
<wire x1="-48.26" y1="73.66" x2="-48.26" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="MCLK_(1)"/>
<wire x1="-48.26" y1="33.02" x2="-38.1" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="IN-"/>
<wire x1="83.82" y1="88.9" x2="93.98" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="IC4" gate="G$1" pin="IN+"/>
<wire x1="83.82" y1="83.82" x2="83.82" y2="86.36" width="0.1524" layer="91"/>
<wire x1="83.82" y1="86.36" x2="93.98" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="76.2" y1="83.82" x2="-2.54" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="83.82" x2="-2.54" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="HPR"/>
<wire x1="-2.54" y1="17.78" x2="10.16" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB6"/>
<wire x1="-83.82" y1="48.26" x2="-66.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-66.04" y1="48.26" x2="-66.04" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="SCL"/>
<wire x1="-66.04" y1="-43.18" x2="-58.42" y2="-43.18" width="0.1524" layer="91"/>
<pinref part="R15" gate="G$1" pin="1"/>
<wire x1="-58.42" y1="-43.18" x2="-20.32" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="-40.64" x2="-58.42" y2="-43.18" width="0.1524" layer="91"/>
<junction x="-58.42" y="-43.18"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="SDA"/>
<wire x1="-20.32" y1="-45.72" x2="-55.88" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-45.72" x2="-63.5" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="-63.5" y1="-45.72" x2="-63.5" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PB7"/>
<wire x1="-63.5" y1="50.8" x2="-83.82" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R16" gate="G$1" pin="1"/>
<wire x1="-55.88" y1="-40.64" x2="-55.88" y2="-45.72" width="0.1524" layer="91"/>
<junction x="-55.88" y="-45.72"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="IC2" gate="G$1" pin="CLK1"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="5.08" y1="-38.1" x2="73.66" y2="-38.1" width="0.1524" layer="91"/>
<wire x1="73.66" y1="-38.1" x2="73.66" y2="-40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="B"/>
<wire x1="-175.26" y1="60.96" x2="-162.56" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="60.96" x2="-160.02" y2="60.96" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC12"/>
<wire x1="-160.02" y1="60.96" x2="-129.54" y2="60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="A"/>
<wire x1="-175.26" y1="58.42" x2="-165.1" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="58.42" x2="-157.48" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC11"/>
<wire x1="-157.48" y1="58.42" x2="-129.54" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="B"/>
<wire x1="-175.26" y1="48.26" x2="-154.94" y2="48.26" width="0.1524" layer="91"/>
<wire x1="-154.94" y1="48.26" x2="-154.94" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC10"/>
<wire x1="-154.94" y1="55.88" x2="-129.54" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="A"/>
<wire x1="-175.26" y1="45.72" x2="-152.4" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="45.72" x2="-152.4" y2="53.34" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC9"/>
<wire x1="-152.4" y1="53.34" x2="-129.54" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<wire x1="-195.58" y1="40.64" x2="-149.86" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-149.86" y1="40.64" x2="-149.86" y2="50.8" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC8"/>
<wire x1="-149.86" y1="50.8" x2="-129.54" y2="50.8" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="40.64" x2="-200.66" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="40.64" x2="-200.66" y2="58.42" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="1"/>
<wire x1="-200.66" y1="58.42" x2="-195.58" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="1"/>
<wire x1="-195.58" y1="45.72" x2="-195.58" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="38.1" x2="-147.32" y2="38.1" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="38.1" x2="-147.32" y2="48.26" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC7"/>
<wire x1="-147.32" y1="48.26" x2="-129.54" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="NO_1"/>
<wire x1="-175.26" y1="33.02" x2="-147.32" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-147.32" y1="33.02" x2="-142.24" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="33.02" x2="-142.24" y2="43.18" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC5"/>
<wire x1="-142.24" y1="43.18" x2="-129.54" y2="43.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="S3" gate="G$1" pin="NO_1"/>
<wire x1="-175.26" y1="22.86" x2="-172.72" y2="22.86" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="22.86" x2="-172.72" y2="30.48" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="30.48" x2="-139.7" y2="30.48" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC4"/>
<wire x1="-139.7" y1="30.48" x2="-139.7" y2="40.64" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="40.64" x2="-129.54" y2="40.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="S4" gate="G$1" pin="NO_1"/>
<wire x1="-175.26" y1="12.7" x2="-170.18" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="12.7" x2="-170.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-170.18" y1="27.94" x2="-137.16" y2="27.94" width="0.1524" layer="91"/>
<wire x1="-137.16" y1="27.94" x2="-137.16" y2="38.1" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC3"/>
<wire x1="-137.16" y1="38.1" x2="-129.54" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="CON3" gate="_SIGNAL" pin="S"/>
<wire x1="-177.8" y1="78.74" x2="-165.1" y2="78.74" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="78.74" x2="-165.1" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="63.5" x2="-165.1" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="CON4" gate="-3" pin="S"/>
<wire x1="-177.8" y1="83.82" x2="-162.56" y2="83.82" width="0.1524" layer="91"/>
<wire x1="-162.56" y1="83.82" x2="-162.56" y2="66.04" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC14"/>
<wire x1="-162.56" y1="66.04" x2="-129.54" y2="66.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="CON4" gate="-2" pin="S"/>
<wire x1="-177.8" y1="86.36" x2="-160.02" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-160.02" y1="86.36" x2="-160.02" y2="68.58" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC15"/>
<wire x1="-160.02" y1="68.58" x2="-129.54" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PC13"/>
<wire x1="-157.48" y1="63.5" x2="-129.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB10"/>
<wire x1="-83.82" y1="58.42" x2="-27.94" y2="58.42" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="58.42" x2="-27.94" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="SD"/>
<wire x1="-27.94" y1="71.12" x2="-27.94" y2="81.28" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="81.28" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB9"/>
<wire x1="-83.82" y1="55.88" x2="-25.4" y2="55.88" width="0.1524" layer="91"/>
<wire x1="-25.4" y1="55.88" x2="-25.4" y2="71.12" width="0.1524" layer="91"/>
<pinref part="IC8" gate="A" pin="EN"/>
<wire x1="-25.4" y1="71.12" x2="109.22" y2="71.12" width="0.1524" layer="91"/>
<wire x1="109.22" y1="71.12" x2="109.22" y2="48.26" width="0.1524" layer="91"/>
<pinref part="S1" gate="G$1" pin="V1"/>
<wire x1="223.52" y1="20.32" x2="223.52" y2="58.42" width="0.1524" layer="91"/>
<wire x1="223.52" y1="58.42" x2="223.52" y2="71.12" width="0.1524" layer="91"/>
<wire x1="223.52" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
<wire x1="114.3" y1="71.12" x2="109.22" y2="71.12" width="0.1524" layer="91"/>
<junction x="109.22" y="71.12"/>
<pinref part="JP1" gate="A" pin="3"/>
<wire x1="248.92" y1="58.42" x2="223.52" y2="58.42" width="0.1524" layer="91"/>
<junction x="223.52" y="58.42"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB8"/>
<wire x1="-83.82" y1="53.34" x2="-22.86" y2="53.34" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="53.34" x2="-22.86" y2="63.5" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="63.5" x2="81.28" y2="63.5" width="0.1524" layer="91"/>
<wire x1="81.28" y1="63.5" x2="81.28" y2="0" width="0.1524" layer="91"/>
<wire x1="81.28" y1="0" x2="93.98" y2="0" width="0.1524" layer="91"/>
<wire x1="93.98" y1="0" x2="93.98" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="EN"/>
<wire x1="93.98" y1="-30.48" x2="101.6" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="93.98" y1="0" x2="223.52" y2="0" width="0.1524" layer="91"/>
<junction x="93.98" y="0"/>
<pinref part="S1" gate="G$1" pin="V2"/>
<wire x1="223.52" y1="0" x2="223.52" y2="15.24" width="0.1524" layer="91"/>
<wire x1="223.52" y1="15.24" x2="226.06" y2="15.24" width="0.1524" layer="91"/>
<wire x1="226.06" y1="15.24" x2="226.06" y2="55.88" width="0.1524" layer="91"/>
<junction x="223.52" y="15.24"/>
<pinref part="JP1" gate="A" pin="4"/>
<wire x1="226.06" y1="55.88" x2="248.92" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="CON6" gate="_SIGNAL" pin="S"/>
<pinref part="IC4" gate="G$1" pin="OUT+"/>
<wire x1="132.08" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
<wire x1="121.92" y1="93.98" x2="121.92" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="OUT-"/>
<wire x1="121.92" y1="81.28" x2="124.46" y2="81.28" width="0.1524" layer="91"/>
<wire x1="124.46" y1="81.28" x2="124.46" y2="91.44" width="0.1524" layer="91"/>
<pinref part="CON6" gate="_GND" pin="S"/>
<wire x1="124.46" y1="91.44" x2="132.08" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="CON5" gate="_SIGNAL" pin="S"/>
<wire x1="132.08" y1="104.14" x2="-5.08" y2="104.14" width="0.1524" layer="91"/>
<pinref part="C48" gate="G$1" pin="1"/>
<wire x1="-5.08" y1="104.14" x2="-5.08" y2="101.6" width="0.1524" layer="91"/>
<pinref part="R14" gate="G$1" pin="2"/>
<wire x1="-10.16" y1="104.14" x2="-5.08" y2="104.14" width="0.1524" layer="91"/>
<junction x="-5.08" y="104.14"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="C48" gate="G$1" pin="2"/>
<pinref part="IC3" gate="G$1" pin="IN3_R"/>
<wire x1="-5.08" y1="93.98" x2="-5.08" y2="2.54" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="2.54" x2="10.16" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="MICBIAS"/>
<wire x1="-10.16" y1="93.98" x2="-10.16" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_1" class="0">
<segment>
<pinref part="C47" gate="G$1" pin="1"/>
<wire x1="-127" y1="121.92" x2="-127" y2="116.84" width="0.1524" layer="91"/>
<junction x="-127" y="121.92"/>
<pinref part="CON2" gate="_VCC" pin="S"/>
<wire x1="-142.24" y1="121.92" x2="-132.08" y2="121.92" width="0.1524" layer="91"/>
<pinref part="P+1" gate="G$1" pin="VCC_1"/>
<wire x1="-132.08" y1="121.92" x2="-127" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-127" y1="137.16" x2="-127" y2="121.92" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="IN"/>
<wire x1="-124.46" y1="121.92" x2="-127" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R20" gate="G$1" pin="2"/>
<wire x1="-132.08" y1="114.3" x2="-132.08" y2="121.92" width="0.1524" layer="91"/>
<junction x="-132.08" y="121.92"/>
</segment>
<segment>
<pinref part="JP1" gate="A" pin="1"/>
<pinref part="P+2" gate="G$1" pin="VCC_1"/>
<wire x1="248.92" y1="63.5" x2="241.3" y2="63.5" width="0.1524" layer="91"/>
<wire x1="241.3" y1="63.5" x2="241.3" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB1"/>
<wire x1="-83.82" y1="35.56" x2="-71.12" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="35.56" x2="-71.12" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-71.12" y1="45.72" x2="-20.32" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="45.72" x2="-20.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="68.58" x2="220.98" y2="68.58" width="0.1524" layer="91"/>
<wire x1="220.98" y1="68.58" x2="220.98" y2="53.34" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="5"/>
<wire x1="220.98" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PB0"/>
<wire x1="-83.82" y1="33.02" x2="-68.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="33.02" x2="-68.58" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="43.18" x2="-17.78" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="43.18" x2="-17.78" y2="66.04" width="0.1524" layer="91"/>
<wire x1="-17.78" y1="66.04" x2="218.44" y2="66.04" width="0.1524" layer="91"/>
<wire x1="218.44" y1="66.04" x2="218.44" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="6"/>
<wire x1="218.44" y1="50.8" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="LCD1" gate="G$1" pin="LED+"/>
<wire x1="-60.96" y1="-93.98" x2="-60.96" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="-60.96" y1="-96.52" x2="-58.42" y2="-96.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="A0"/>
<wire x1="-58.42" y1="-101.6" x2="-76.2" y2="-101.6" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="-101.6" x2="-76.2" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PA2"/>
<wire x1="-76.2" y1="-5.08" x2="-83.82" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="LCD1" gate="G$1" pin="CS"/>
<wire x1="-58.42" y1="-111.76" x2="-78.74" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="-78.74" y1="-111.76" x2="-78.74" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PA1"/>
<wire x1="-78.74" y1="-7.62" x2="-83.82" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA11"/>
<wire x1="-83.82" y1="17.78" x2="-99.06" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="17.78" x2="-99.06" y2="-91.44" width="0.1524" layer="91"/>
<wire x1="-99.06" y1="-91.44" x2="-134.62" y2="-91.44" width="0.1524" layer="91"/>
<pinref part="CON7" gate="G$1" pin="2"/>
<wire x1="-134.62" y1="-91.44" x2="-142.24" y2="-91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="IC1" gate="MCU" pin="PA12"/>
<wire x1="-83.82" y1="20.32" x2="-96.52" y2="20.32" width="0.1524" layer="91"/>
<wire x1="-96.52" y1="20.32" x2="-96.52" y2="-93.98" width="0.1524" layer="91"/>
<pinref part="CON7" gate="G$1" pin="3"/>
<wire x1="-96.52" y1="-93.98" x2="-142.24" y2="-93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="2"/>
<wire x1="27.94" y1="-71.12" x2="152.4" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-71.12" x2="152.4" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="IC7" gate="A" pin="VCTRL"/>
<wire x1="152.4" y1="-22.86" x2="147.32" y2="-22.86" width="0.1524" layer="91"/>
<pinref part="C51" gate="G$1" pin="1"/>
<wire x1="27.94" y1="-73.66" x2="27.94" y2="-71.12" width="0.1524" layer="91"/>
<junction x="27.94" y="-71.12"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="IC9" gate="G$1" pin="VOUT"/>
<wire x1="17.78" y1="-71.12" x2="7.62" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<pinref part="IC9" gate="G$1" pin="!CS"/>
<wire x1="-20.32" y1="-73.66" x2="-81.28" y2="-73.66" width="0.1524" layer="91"/>
<wire x1="-81.28" y1="-73.66" x2="-81.28" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PA0-WKUP"/>
<wire x1="-81.28" y1="-10.16" x2="-83.82" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="L2" gate="G$1" pin="1"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="12.7" y1="-43.18" x2="10.16" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-43.18" x2="10.16" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="VDDO"/>
<wire x1="5.08" y1="-43.18" x2="10.16" y2="-43.18" width="0.1524" layer="91"/>
<junction x="10.16" y="-43.18"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="CON8" gate="_SIGNAL" pin="S"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="241.3" y1="17.78" x2="238.76" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="IC3" gate="G$1" pin="AVDD"/>
<wire x1="10.16" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC_2" class="0">
<segment>
<pinref part="IC5" gate="G$1" pin="IN"/>
<pinref part="C46" gate="G$1" pin="1"/>
<wire x1="-73.66" y1="121.92" x2="-76.2" y2="121.92" width="0.1524" layer="91"/>
<wire x1="-76.2" y1="121.92" x2="-76.2" y2="119.38" width="0.1524" layer="91"/>
<pinref part="L1" gate="G$1" pin="2"/>
<wire x1="-81.28" y1="121.92" x2="-76.2" y2="121.92" width="0.1524" layer="91"/>
<junction x="-76.2" y="121.92"/>
<pinref part="P+3" gate="G$1" pin="VCC_2"/>
<wire x1="-76.2" y1="127" x2="-76.2" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC1" gate="MCU" pin="PA9"/>
<pinref part="CON1" gate="_TX" pin="S"/>
<wire x1="-83.82" y1="12.7" x2="-91.44" y2="12.7" width="0.1524" layer="91"/>
<wire x1="-91.44" y1="12.7" x2="-91.44" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-83.82" y="12.7"/>
<pinref part="CON7" gate="G$1" pin="1"/>
<wire x1="-91.44" y1="-88.9" x2="-132.08" y2="-88.9" width="0.1524" layer="91"/>
<pinref part="P+4" gate="G$1" pin="VCC_2"/>
<wire x1="-132.08" y1="-88.9" x2="-142.24" y2="-88.9" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="-83.82" x2="-132.08" y2="-88.9" width="0.1524" layer="91"/>
<junction x="-132.08" y="-88.9"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="CON9" gate="G$1" pin="P$2"/>
<wire x1="-147.32" y1="-12.7" x2="-144.78" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-144.78" y1="-12.7" x2="-139.7" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="-12.7" x2="-139.7" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-139.7" y1="-7.62" x2="-129.54" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="BOOT0"/>
<wire x1="-129.54" y1="-7.62" x2="-129.54" y2="-5.08" width="0.1524" layer="91"/>
<pinref part="R19" gate="G$1" pin="2"/>
<wire x1="-144.78" y1="-15.24" x2="-144.78" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-144.78" y="-12.7"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="-132.08" y1="104.14" x2="-132.08" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-132.08" y1="101.6" x2="-119.38" y2="101.6" width="0.1524" layer="91"/>
<wire x1="-119.38" y1="101.6" x2="-119.38" y2="35.56" width="0.1524" layer="91"/>
<pinref part="IC1" gate="MCU" pin="PC2"/>
<wire x1="-119.38" y1="35.56" x2="-129.54" y2="35.56" width="0.1524" layer="91"/>
<junction x="-132.08" y="101.6"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
